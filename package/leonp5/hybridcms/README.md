# hybridCMS

## Contents

-   [Goals](#-the-goals-of-this-package-are)
-   [Explanation](#-short-explanation)
-   [Setup](#-setup)
    -   [Environment Variables](#-.env-entries)
        -   [Database](#-database)
    -   [Templates](#-templates)
        -   [Folder Structure](#-folder-structure)
        -   [hcms tags](#-hcms-tags)
    -   [Assets](#-assets)

## The goals of this package are

-   Give the developer the possibility to use a wide range of templates (or self created HTML files) and have at least partial advantages of the Laravel Blade template engine.
-   (plugins & custom code would be nice, but at the moment (06.2021) i don't know, how to implement such a feature)
-   Creating a CMS, which gives a customer/client a simple CMS backend, where only the buttons and settings are, which he/she needs. **Everything else, should be done/maintained by the developer/a technical versed person who is familiar with Laravel, MySQL, Git**.
-   Improving my development skills ;-)

## Short explanation

A simple Laravel CMS package. (First) Intended for private usage.  
If you want to use it you should be familiar with PHP, Laravel and Blade!  
I develop this package because of the following experience: There are many CMS systems & different approaches to build a CMS. The "classical" ones like [Wordpress](https://wordpress.org/), [Drupal](https://www.drupal.de/) or [Typo3](https://typo3.org/). You have "smaller" ones like [Pico](https://picocms.org/), [Twill](https://twill.io/), [Strapi](https://strapi.io/), [Sulu](https://sulu.io/), [Kirby](https://getkirby.com/), [Silverstripe](https://www.silverstripe.org/) or [OctoberCMS](https://octobercms.com/). Or you have "Drag & Drop / WYSIWYG" solutions like [Wix](https://www.wix.com/) or [Jimdo](https://www.jimdo.com/) and many more ;-)  
In my experience all of them have at least one problem in common: **They are too complex for a "non technical versed" person**. So normally you need time and will to understand/learn the selected CMS or a person who already know how to use it.
With this problem in mind, i started to develop this package. I wanted to create a CMS which offers a simple user interface for a client (only editing content) and a more complex one for the developer/maintainer/technical versed person. So it is intended for at least a person, who is technical versed/interested or a developer who wants to use a CMS for him/herself or to set it up for a client/customer who doesn't want or can't do it by him/herself. From time to time it needs to be maintained/updated by the developer.
In the long term it would also be nice, if the developer could implement/write plugins but at the moment (06.2021) i don't know how to implement this feature.

## Planned features

-   Embedding plugins
-   Automatic backups
-   CMS package update from the backend interface
-   Inline editing with SunEditor

## Setup

### .env entries

---

You need to enter the environment variables for a database connection.

#### **Database**

`DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD`

#### **Other**

`FRONTEND_NAME` -> name of your project

`TEMPLATE_FOLDER` -> see [templates paragraph](#templates)

`FRONTEND_MULTILANG` -> true, if your project should be available in multiple languages

### Templates

---

Enter a valid path to the template folder in the .env and the filesystems.php confing in the disks section (Don't change the key "templates").

```
'templates' => [
            'driver' => 'local',
            'root' => base_path(env('TEMPLATE_FOLDER')),
        ],
```

When you run

```
php artisan vendor:publish --provider="Leonp5\Hybridcms\HybridCmsProvider" --tag="templates"
```

the path should be `resources/views/vendor/hybridcms` and the example templates are inside this folder.

#### **Folder structure**

You could implement any HTML you want. Create blade ([Laravel Docs](https://laravel.com/docs/8.x/blade)) files and insert the HTML of your template(s). The scan process of hybridCMS will scan blade files inside the folder named `scan` and searches for the hcms-tags ([see table below](#hcms-tags)). Files inside the `scan-exclude` folder won't be scanned.  
Inside the `scan` folder there are `layout`, `layout/partials` and `pages`.  
Inside `layout/partials` could be files you want to include in other template files.  
Inside `pages` are the page templates. This are the templates, you can select if you create a new site inside the hybridCMS backend.

#### **hcms tags**

The tags are set inside the template blade files before you start the template scan.

Simple example:

```
<p data-hcms-content-text="text_1">
      {{$text_1 ?? ''}}
    </p>
```

**Important**:

-   Variable name and the text of the data attribute must be identical!
-   Renaming this two (variable and text of the data attribute) and scan the templates again after you already created a page which uses this template **doesn't** work!  
    (Adding new fields is possible)

| Tag                            | Values       | Description                                                                                                                                  |
| ------------------------------ | ------------ | -------------------------------------------------------------------------------------------------------------------------------------------- |
| data-hcms-content-text         | <field_name> | simple text content field                                                                                                                    |
| data-hcms-content-editor       | <field_name> | content in this field will be editable with a JS editor. (Further the HTML element needs the css class `sun-editor-editable` -> testing)     |
| data-hcms-partial              | `true`       | declares a template file as a partial (files must be in the folder `layout/partials`)                                                        |
| data-hcms-single-use           | `true`       | if you set this tag to a template file, it could only used once (if you already used it, it will not appear in the "Create new page" dialog) |
| data-hcms-includes-single-uses | `true`       | a file, which includes files with `data-hcms-single-use` tag, won't be treated like a single use file                                        |
|                                |

### Assets

---

php artisan vendor:publish --provider="Leonp5\Hybridcms\HybridCmsProvider" --tag="assets"
