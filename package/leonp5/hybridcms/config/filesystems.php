<?php

return [

    'disks' => [

        'templates' => [
            'driver' => 'local',
            'root' => base_path(env('TEMPLATE_FOLDER')),
        ],

    ],
];
