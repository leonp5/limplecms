<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Migrations\Migration;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate\ContentTableCreateProcessor;

class CreateContentDeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ContentTableCreateProcessor::create(ContentTableInterface::CONTENT_TABLE_PREFIX . 'de');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_de');
    }
}
