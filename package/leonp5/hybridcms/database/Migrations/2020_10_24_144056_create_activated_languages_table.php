<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivatedLanguagesTable extends Migration
{
    /**
     * 
     * The activated languages for the frontend
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activated_languages', function (Blueprint $table) {
            $table->id();
            $table->string('alpha_2');
            $table->integer('lang_id');
            $table->boolean('main_language')->default(false);
            $table->boolean('disabled')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activated_languages');
    }
}
