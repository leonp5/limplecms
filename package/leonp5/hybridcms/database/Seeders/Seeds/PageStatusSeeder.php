<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;

use Leonp5\Hybridcms\App\Models\PageStatusDe;
use Leonp5\Hybridcms\App\Models\PageStatusGb;
use Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface;

class PageStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PageStatusDe::truncate();
        PageStatusGb::truncate();


        PageStatusDe::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_PUBLISHED,
            'status_label' => 'veröffentlicht'
        ]);

        PageStatusDe::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_DRAFT,
            'status_label' => 'Entwurf'
        ]);

        PageStatusDe::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_INACTIVE,
            'status_label' => 'inaktiv'
        ]);

        PageStatusGb::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_PUBLISHED,
            'status_label' => 'published'
        ]);

        PageStatusGb::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_DRAFT,
            'status_label' => 'Draft'
        ]);

        PageStatusGb::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_INACTIVE,
            'status_label' => 'inactive'
        ]);
    }
}
