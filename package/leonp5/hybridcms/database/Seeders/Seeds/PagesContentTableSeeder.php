<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

use Leonp5\Hybridcms\App\Models\Page;
use Leonp5\Hybridcms\App\Models\User;
use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\Page\Business\Status\PageStatusInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;

class PagesContentTableSeeder extends Seeder
{
    /**
     * @var array
     */
    private array $notFoundPageContentDe = [
        'heading' => '404',
        'text' => 'Die angefragte Seite ist nicht mehr hier oder existiert nicht.',
        'button' => 'Zur Startseite',
    ];

    /**
     * @var array
     */
    private array $notFoundPageContentEn = [
        'heading' => '404',
        'text' => 'The requested page is no longer here or does not exist.',
        'button' => 'To the home page',
    ];

    /**
     * @var Template
     */
    private object $startPageTemplate;

    /**
     * @var Collection
     */
    private object $startPageTemplateFields;

    /**
     * @var Template
     */
    private object $notFoundPageTemplate;

    /**
     * @var Collection
     */
    private object $notFoundPageTemplateFields;

    /**
     * @return void 
     */
    function __construct()
    {
        $this->startPageTemplate = Template::where('template_name', '=', 'Start')->with('templateField')->first();
        $this->startPageTemplateFields = $this->startPageTemplate->templateField;
        $this->notFoundPageTemplate = Template::where('template_name', '=', '404')->with('templateField')->first();
        $this->notFoundPageTemplateFields = $this->notFoundPageTemplate->templateField;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::find(1);

        $startTemplateId = $this->startPageTemplate->id;

        if (env('FRONTEND_MULTILANG') === false) {
            $availableLang = 'de';
        } else {
            $availableLang = 'de, gb';
        }

        $aboutPageId = 1;
        $contactPageId = 2;
        $disclaimerPageId = 3;
        $notFoundPageId = 4;

        $aboutPage = Page::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_DRAFT,
            'available_lang' => $availableLang,
            'template_id' => $startTemplateId
        ]);

        $contactPage = Page::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_PUBLISHED,
            'available_lang' => $availableLang,
            'template_id' => $startTemplateId
        ]);

        $disclaimerPage = Page::create([
            'page_status_id' => PageStatusInterface::PAGE_STATUS_INACTIVE,
            'available_lang' => $availableLang,
            'template_id' => $startTemplateId,
            'deletable' => false
        ]);

        $pageNotFoundTemplateId = Template::where('template_name', 404)->first()->id;

        $notFoundPage = Page::create(
            [
                'page_status_id' => PageStatusInterface::PAGE_STATUS_PUBLISHED,
                'available_lang' => $availableLang,
                'template_id' => $pageNotFoundTemplateId,
                'deletable' => false
            ]
        );

        $this->pageInserter(
            'de',
            $aboutPageId,
            'Über',
            '/ueber',
            'Das ist die Über Seite',
            function () use ($aboutPageId) {
                $this->contentFieldInserter(
                    'de',
                    'Hier steht toller super duper Text für ',
                    $aboutPageId,
                    $this->startPageTemplateFields
                );
            }
        );


        $this->pageInserter(
            'de',
            $disclaimerPageId,
            'Impressum',
            '/impressum',
            'Das ist das Impressum',
            function () use ($disclaimerPageId) {
                $this->contentFieldInserter(
                    'de',
                    'Hier steht toller super duper Text für ',
                    $disclaimerPageId,
                    $this->startPageTemplateFields
                );
            }
        );

        $this->pageInserter(
            'de',
            $notFoundPageId,
            '404',
            '/404',
            '404 Fehler Seite',
            function () use ($notFoundPageId) {
                $this->createNotFoundPageContent(
                    $this->notFoundPageContentDe,
                    $notFoundPageId,
                    'de'
                );
            }
        );

        $this->pageInserter(
            'de',
            $contactPageId,
            'Kontakt',
            '/kontakt',
            'Das ist die Kontaktseite',
            function () use ($contactPageId) {
                $this->contentFieldInserter(
                    'de',
                    'Hier steht toller super duper Text für ',
                    $contactPageId,
                    $this->startPageTemplateFields
                );
            }
        );

        if (env('FRONTEND_MULTILANG') === true) {

            $this->pageInserter(
                'gb',
                $aboutPageId,
                'About',
                '/about',
                'This is about us',
                function () use ($aboutPageId) {
                    $this->contentFieldInserter(
                        'gb',
                        'Here is text for the ',
                        $aboutPageId,
                        $this->startPageTemplateFields
                    );
                }
            );

            $this->pageInserter(
                'gb',
                $disclaimerPageId,
                'Disclaimer',
                '/disclaimer',
                'This is the disclaimer page',
                function () use ($disclaimerPageId) {
                    $this->contentFieldInserter(
                        'gb',
                        'Here is text for the ',
                        $disclaimerPageId,
                        $this->startPageTemplateFields
                    );
                }
            );


            $this->pageInserter(
                'gb',
                $contactPageId,
                'Contact',
                '/contact',
                'This is the contact page',
                function () use ($contactPageId) {
                    $this->contentFieldInserter(
                        'gb',
                        'Here is text for the ',
                        $contactPageId,
                        $this->startPageTemplateFields
                    );
                }
            );



            $this->pageInserter(
                'gb',
                $notFoundPageId,
                '404',
                '/404',
                '404 Error site',
                function () use ($notFoundPageId) {
                    $this->createNotFoundPageContent(
                        $this->notFoundPageContentEn,
                        $notFoundPageId,
                        'gb'
                    );
                }
            );
        }

        $admin->pages()->saveMany([
            $aboutPage,
            $contactPage,
            $disclaimerPage,
            $notFoundPage
        ]);
    }

    /**
     * @param string $langKey
     * @param int $pageId
     * @param string $title
     * @param string $url
     * @param string $description
     * @param Callable $pageContentInserter
     * 
     * @return void 
     */
    private function pageInserter(
        string $langKey,
        int $pageId,
        string $title,
        string $url,
        string $description,
        callable $pageContentInserter
    ) {
        DB::table(ContentTableInterface::CONTENT_TABLE_PREFIX . $langKey)->insert([
            'page_id' => $pageId,
            'title' => $title,
            'url' => $url,
            'description' => $description,
        ]);

        $pageContentInserter();
    }

    /**
     * @param string $langKey
     * @param string $textContent
     * @param int $pageId
     * @param Collection $templateFields
     * 
     * @return void 
     */
    private function contentFieldInserter(
        string $langKey,
        string $textContent,
        int $pageId,
        object $templateFields
    ) {
        foreach ($templateFields as $templateField) {
            DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)->insert(
                [
                    'page_id' => $pageId,
                    'template_field_id' => $templateField->id,
                    'content' => $textContent . $templateField->field_name,
                    'content_type_id' => ContentTableInterface::CONTENT_TYPE_TEXT,
                    'created_at' => Carbon::now()
                ]
            );
        }
    }

    /**
     * @param array $pageContent
     * @param int $pageId
     * @param string $langKey
     */
    private function createNotFoundPageContent(
        array $pageContent,
        int $pageId,
        string $langKey
    ) {
        foreach ($pageContent as $templateFieldName => $templateFieldContent) {
            $templateFieldId = $this->notFoundPageTemplateFields->where('field_name', '=', $templateFieldName)->first()->id;
            DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)->insert(
                [
                    'page_id' => $pageId,
                    'template_field_id' => $templateFieldId,
                    'content' => $templateFieldContent,
                    'content_type_id' => ContentTableInterface::CONTENT_TYPE_TEXT,
                    'created_at' => Carbon::now()
                ]
            );
        }
    }
}
