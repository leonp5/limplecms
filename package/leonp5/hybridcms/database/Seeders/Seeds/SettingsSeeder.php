<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsSeeder extends Seeder
{
    /**
     * 
     * @return void 
     */
    public function run(): void
    {

        DB::table('settings_password')
            ->insert([
                'min_chars' => 4,
                'number_required' => false,
                'capital_required' => true,
            ]);
    }
}
