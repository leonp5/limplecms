<?php

namespace Leonp5\Hybridcms\Database\Seeders\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanTemplateProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\UpdateTemplateTable\UpdateTemplateTableProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanUtils\TemplateScanUtilsProcessor;

class TemplateScanOptionSeeder extends Seeder
{
    /**
     * 
     * @return void 
     */
    public function run(): void
    {
        $templateScanUtilsProcessor = new TemplateScanUtilsProcessor();
        $templateScanProcessor = new ScanTemplateProcessor($templateScanUtilsProcessor);
        $templateTableUpdateProcessor = new UpdateTemplateTableProcessor();

        $templateScanResponseTransfer = $templateScanProcessor->scanPageTemplates();
        $templateTableUpdateProcessor->update($templateScanResponseTransfer->getTemplateTransfer());

        DB::table('template_scan_options')
            ->insert([
                'scan_partials' => false,
                'scan_components' => false,
            ]);
    }
}
