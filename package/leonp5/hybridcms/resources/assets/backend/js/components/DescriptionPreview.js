export default class DescriptionPreview {
    constructor() {
        this.descriptionPreviewElements = null
    }

    init(htmlEl) {
        this.descriptionPreviewElements = Array.from(
            document.querySelectorAll('[data-hcms-preview]')
        )
        this.attachEventlistener(
            this.descriptionPreviewElements,
            'data-hcms-preview'
        )
    }

    attachEventlistener(elements, dataAttribute) {
        elements.forEach(el => {
            const id = el.getAttribute(dataAttribute)
            const inputField = document.getElementById(id)
            inputField.addEventListener('keyup', e => {
                this.insertText(id, e)
            })

            // refill the preview in case of a validation error redirect back from the backend
            if (inputField.value.length > 0) {
                inputField.dispatchEvent(
                    new KeyboardEvent('keyup', { keyCode: 37 })
                )
            }
        })
    }

    insertText(elementId, e) {
        const previewEl = document.querySelector(
            `[data-hcms-preview="${elementId}"]`
        )

        if (elementId.includes('url') === true) {
            previewEl.innerHTML = window.location.origin + e.target.value
            return
        }

        if (elementId.includes('description') === true) {
            this.handleSignCount(elementId, e.target.value.length)
        }

        previewEl.innerHTML = e.target.value
    }

    handleSignCount(elementId, amountSigns) {
        const countEl = document.querySelector(
            `[data-hcms-preview-word-count="${elementId}"]`
        )
        countEl.innerHTML = amountSigns

        if (amountSigns >= 170) {
            countEl.classList.add('text-danger')
        }

        if (amountSigns < 170) {
            countEl.classList.remove('text-danger')
        }
    }
}
