export default class Fetch {
    constructor() {
        this.fetchOptions = null
        this.token = document
            .querySelector('meta[name="csrf-token"]')
            .getAttribute('content')
    }

    async doFetch(fetchOptions = this.fetchOptions) {
        const response = await fetch(fetchOptions.url, {
            method: fetchOptions.method,
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json, text-plain, */*',
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': fetchOptions.token
                    ? fetchOptions.token
                    : this.token,
            },
            body: fetchOptions.data,
        })
        return response
    }

    createFetchOptions(form, data = null) {
        this.fetchOptions = {
            method: form.method,
            url: form.action,
            data: data ? JSON.stringify(data) : null,
            token: this.token,
        }

        return this.fetchOptions
    }
}
