import HandleLanuageCheckboxes from '../HandleLanguageCheckboxes'
import TemplateStatusToggle from '../template/TemplateStatusToggle'
import Tooltip from '../Tooltip'
import TemplateSelectModal from '../template/TemplateSelectModal'
import TemplateScan from '../template/TemplateScan'
import DescriptionPreview from '../DescriptionPreview'
import PageDeletableStatusToggle from '../page/PageDeletableStatusToggle'
import PagePreview from '../page/PagePreview'

export const classes = {
    HandleLanuageCheckboxes: new HandleLanuageCheckboxes(),
    TemplateScan: new TemplateScan(),
    TemplateStatusToggle: new TemplateStatusToggle(),
    Tooltip: new Tooltip(),
    TemplateSelectModal: new TemplateSelectModal(),
    DescriptionPreview: new DescriptionPreview(),
    PageDeletableStatusToggle: new PageDeletableStatusToggle(),
    PagePreview: new PagePreview(),
}
