import Fetch from '../fetch/Fetch'
import AlertCreator from '../utils/AlertCreator'

export default class PageEdit {
    constructor(iFrameContainer = null) {
        this.headerHeight = document.getElementsByTagName(
            'header'
        )[0].offsetHeight
        this.iFrameContainer = iFrameContainer
        this.modalId = 'hcms-field-edit-modal'
        this.dataAttributeTextFields = 'data-hcms-content-text'
        this.dataAttributeEditorFields = 'data-hcms-content-editor'
        this.additionalPixels = 20
        this.fieldIdsContainer = document.getElementById('hcms-field-ids')
        this.langKey = document.documentElement.lang
        this.modal = null
        this.closeButtons = null
        this.jsonResponse = null
        this.fetch = null

        this.closeModal = () => {
            this.modal.classList.remove('show')
            this.modal.style.display = ''
            document.body.className = ''
            this.background.remove()
        }
    }

    init() {
        const textFields = this.searchTextFields(this.dataAttributeTextFields)
        textFields.forEach((textField) => {
            textField.addEventListener('mouseover', (e) => {
                this.addHoverAnimation(
                    e,
                    textField.getAttribute(this.dataAttributeTextFields)
                )
            })
        })

        const editorFields = this.searchTextFields(
            this.dataAttributeEditorFields
        )
        editorFields.forEach((editorField) => {
            editorField.addEventListener('mouseover', (e) => {
                this.addHoverAnimation(
                    e,
                    editorField.getAttribute(this.dataAttributeEditorFields)
                )
            })
        })

        this.closeButtons = Array.from(
            document.querySelectorAll('[data-hcms-field-edit-modal-dismiss]')
        )
    }

    searchTextFields(dataAttribute) {
        return Array.from(
            this.iFrameContainer.contentWindow.document.body.querySelectorAll(
                `[${dataAttribute}]`
            )
        )
    }

    getFieldId(key) {
        return this.fieldIdsContainer.querySelector(`input[name=${key}`).value
    }

    addHoverAnimation(e, dataAttribute) {
        const coleredBackgroundElement = document.createElement('div')
        coleredBackgroundElement.id = dataAttribute

        coleredBackgroundElement.addEventListener('mouseleave', () => {
            this.removeHoverAnimation(coleredBackgroundElement)
        })

        coleredBackgroundElement.addEventListener('click', () => {
            this.doEditRequest(coleredBackgroundElement)
        })

        const css = {
            position: 'absolute',
            background: '#e55353',
            display: 'flex',
            borderRadius: '3px',
            border: '1px solid red',
            opacity: 0,
            transition: 'all 0.1s ease-in-out',
            minHeight: e.target.offsetHeight + this.additionalPixels + 'px',
            minWidth: e.target.offsetWidth + this.additionalPixels + 'px',
        }

        const position = this.setPosition(e.target)

        Object.assign(coleredBackgroundElement.style, css, position)

        document.body.append(coleredBackgroundElement)
        document.body.style.cursor = 'pointer'

        // Giving the browser time to process makes animation work
        setTimeout(() => {
            Object.assign(coleredBackgroundElement.style, {
                opacity: 0.8,
            })
        }, 5)
    }

    setPosition(element) {
        const top =
            this.headerHeight +
            element.getBoundingClientRect().top -
            this.additionalPixels / 2 +
            'px'

        let leftNumber

        if (this.iFrameContainer.getBoundingClientRect().left > 0) {
            leftNumber =
                this.iFrameContainer.getBoundingClientRect().left +
                element.getBoundingClientRect().left -
                this.additionalPixels / 2
        } else {
            leftNumber =
                element.getBoundingClientRect().left - this.additionalPixels / 2
        }

        const left = leftNumber + 'px'

        const position = {
            top: top,
            left: left,
        }

        return position
    }

    removeHoverAnimation(coleredBackgroundElement) {
        this.iFrameContainer.contentWindow.document.body.style.cursor = 'auto'
        coleredBackgroundElement.remove()
    }

    async doEditRequest(coleredBackgroundElement) {
        const fieldId = this.getFieldId(coleredBackgroundElement.id)

        const url =
            '/cms/fields/' +
            this.iFrameContainer.id +
            '/' +
            coleredBackgroundElement.id +
            '/' +
            fieldId +
            '/' +
            this.langKey
        const form = document.createElement('form')
        form.setAttribute('action', url)
        form.setAttribute('method', 'GET')

        this.fetch = new Fetch()

        const fetchOptions = this.fetch.createFetchOptions(form)
        const response = await this.fetch.doFetch(fetchOptions)

        this.handleEditResponse(response)
    }

    async handleEditResponse(response) {
        this.jsonResponse = await response.json()

        if (response.status !== 200) {
            this.handleResponseError(this.jsonResponse)
        }

        if (this.jsonResponse.hasOwnProperty('redirect') === true) {
            window.location = this.jsonResponse.redirect.url
        }

        if (this.jsonResponse.hasOwnProperty('data') === true) {
            this.openModal()
        }
    }

    openModal() {
        this.modal = document.getElementById(this.modalId)

        this.addDataToModal()

        this.background = document.createElement('div')
        this.background.className = 'modal-backdrop fade show'
        document.body.append(this.background)

        this.modal.classList.add('show')
        this.modal.style.display = 'block'
        document.body.className = 'modal-open'

        this.closeButtons.forEach((buttonEl) => {
            buttonEl.addEventListener('click', this.closeModal)
        })

        window.addEventListener('click', (e) => {
            if (e.target.getAttribute('id') === this.modalId) {
                this.closeModal(e)
            }
        })
    }

    addDataToModal() {
        const formGroup = this.modal.getElementsByClassName('form-group')[0]

        formGroup.getElementsByTagName(
            'label'
        )[0].innerHTML = `${this.jsonResponse.data.field_name}`

        const input = formGroup.getElementsByTagName('input')[0]

        input.value = this.jsonResponse.data.content
        input.name = this.jsonResponse.data.field_name

        const submitButton = this.modal.querySelector('button[type="submit"]')
        submitButton.addEventListener('click', (e) => {
            this.fetchData(
                e,
                input,
                this.jsonResponse.data.field_id,
                this.jsonResponse.data.page_id,
                this.jsonResponse.data.field_name,
                this.langKey,
                submitButton.form
            )
        })
    }

    async fetchData(e, inputEl, fieldId, pageId, fieldName, langKey, form) {
        e.preventDefault()
        const fetchOptions = this.fetch.createFetchOptions(form, {
            fieldId: fieldId,
            pageId: pageId,
            content: inputEl.value,
            fieldName: fieldName,
            langKey: langKey,
        })

        const response = await this.fetch.doFetch(fetchOptions)

        if (response.status !== 200) {
            this.handleResponseError(await response.json())
        }

        if (response.status === 200) {
            this.closeModal()
            const jsonResponse = await response.json()
            new AlertCreator(jsonResponse.success).createAlertElement()

            setTimeout(() => {
                location.reload()
            }, 1000)
        }
    }

    handleResponseError(response) {
        new AlertCreator(response.error, 'error').createAlertElement()
    }
}
