import PageEdit from './PageEdit'

export default class PagePreview {
    constructor() {
        this.iFrame = null
        this.mainContainer = null
    }

    init(iFrame) {
        this.iFrame = iFrame

        this.mainContainer = document.querySelector('main[class="c-main"]')
        this.mainContainer.style.paddingTop = '0'

        this.setFrameHeight()

        window.addEventListener('resize', () => {
            this.setFrameHeight()
        })

        new PageEdit(iFrame).init()
    }

    setFrameHeight() {
        this.iFrame.height = 0
        this.iFrame.height = this.mainContainer.clientHeight - 5
    }
}
