import AlertCreator from '../utils/AlertCreator'
import Fetch from '../fetch/Fetch'

export default class TemplateSelectModal {
    constructor() {
        this.alreadyInitialized = false
        this.optionsCreated = false
        this.openButtons = null
        this.closeButtons = null
        this.modalId = null
        this.modal = null
        this.background = null
        this.select = null
        this.linkElement = null
        this.previewLink = null
        this.pageCreateForm = null
        this.pageCreateLink = null
        this.createButton = null
        this.closeModal = () => {
            this.modal.classList.remove('show')
            this.modal.style.display = ''
            document.body.className = ''
            this.background.remove()
        }
    }

    init() {
        if (this.alreadyInitialized === true) {
            return
        }

        this.openButtons = Array.from(
            document.querySelectorAll('[data-hcms-create-page-modal-open]')
        )
        this.closeButtons = Array.from(
            document.querySelectorAll('[data-hcms-create-page-modal-dismiss]')
        )
        this.openButtons.forEach((buttonEl) => {
            buttonEl.addEventListener('click', (e) => {
                this.showModal(e)
            })
        })

        this.select = document.getElementById('hcms-template-select')

        this.alreadyInitialized = true
    }

    showModal(e) {
        if (this.optionsCreated === false) {
            this.createSelectOptions()
        }
        this.background = document.createElement('div')
        this.background.className = 'modal-backdrop fade show'
        document.body.append(this.background)
        this.modalId = e.target.getAttribute('data-hcms-target')
        this.modal = document.getElementById(this.modalId)
        this.modal.classList.add('show')
        this.modal.style.display = 'block'
        document.body.className = 'modal-open'

        this.closeButtons.forEach((buttonEl) => {
            buttonEl.addEventListener('click', this.closeModal)
        })

        window.addEventListener('click', (e) => {
            if (e.target.getAttribute('id') === this.modalId) {
                this.closeModal(e)
            }
        })
    }

    async createSelectOptions() {
        const form = document.getElementById('hcms-template-select-get')
        const fetch = new Fetch()
        fetch.createFetchOptions(form)
        const response = await fetch.doFetch()
        if (response.status !== 200) {
            new AlertCreator(response.status, 'error').createAlertElement()
            return
        }
        const templates = await response.json()
        templates.templates.forEach((template) => {
            const option = document.createElement('option')
            option.innerHTML = template.template_name
            option.value = template.id
            option.setAttribute('name', template.template_name)
            this.select.append(option)
        })

        const previewLinkElement = document.getElementById(
            'hcms-template-preview-link'
        )
        this.linkElement = previewLinkElement.getElementsByTagName('a')[0]

        this.previewLink = this.linkElement.getAttribute('href')

        this.pageCreateForm = document.getElementById('hcms-page-create')

        this.createButton = this.pageCreateForm.querySelector(
            'button[type="submit"]'
        )

        this.pageCreateLink = this.pageCreateForm.getAttribute('action')

        Array.from(this.select.options).forEach((optionEl) => {
            optionEl.addEventListener('click', (e) => {
                this.handleClick(e)
            })
        })

        this.optionsCreated = true
    }

    handleClick(e) {
        if (e.target.getAttribute('value')) {
            const previewUrl = this.previewLink + '/' + e.target.value
            const pageCreateUrl = this.pageCreateLink + '/' + e.target.value
            this.pageCreateForm.setAttribute('action', pageCreateUrl)
            this.linkElement.setAttribute('href', previewUrl)
            this.linkElement.classList.remove('d-none')
            this.createButton.classList.remove('hcms-cursor-forbidden')
            this.createButton.removeAttribute('disabled')
        } else {
            this.linkElement.className = 'd-none'
            this.pageCreateForm.setAttribute('action', this.pageCreateLink)
            this.linkElement.setAttribute('href', this.previewLink)
            this.createButton.classList.add('hcms-cursor-forbidden')
            this.createButton.setAttribute('disabled', '')
        }
    }
}
