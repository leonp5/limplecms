import AlertCreator from '../utils/AlertCreator'
import Fetch from '../fetch/Fetch'

export default class TemplateStatusToggle {
    constructor() {
        this.switch = null
    }

    init(htmlElement) {
        this.switch = htmlElement

        this.switch.addEventListener('change', e => {
            this.updateStatus(e)
        })
    }

    async updateStatus(e) {
        const templateId = e.target.getAttribute('data-hcms-template-id')
        const fetch = new Fetch()
        fetch.createFetchOptions(this.switch.form, { template_id: templateId })
        const response = await fetch.doFetch()

        if (response.status !== 200) {
            this.handleResponseError(await response.json())
        }
    }

    handleResponseError(response) {
        new AlertCreator(response.error, 'error').createAlertElement()
    }
}
