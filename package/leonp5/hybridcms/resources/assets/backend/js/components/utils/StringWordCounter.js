export default class StringWordCounter {
    constructor(string) {
        this.string = string
        this.prepareString()
    }

    hasMax(maxNumber) {
        const stringLength = this.string.split(' ').length
        if (maxNumber >= stringLength) {
            return true
        }

        if (maxNumber < stringLength) {
            return false
        }
    }

    hasMin(minNumber) {
        const stringLength = this.string.split(' ').length
        if (stringLength >= minNumber) {
            return true
        }
        if (minNumber > stringLength) {
            return false
        }
    }

    prepareString() {
        this.string = this.string.replace(/(^\s*)|(\s*$)/gi, '')
        this.string = this.string.replace(/[ ]{2,}/gi, ' ')
        this.string = this.string.replace(/\n /, '\n')
    }
}
