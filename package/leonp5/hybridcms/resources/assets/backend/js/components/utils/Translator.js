export default class Translator {
    constructor() {
        this.translations = null
    }

    translate(key) {
        return this.translations[key]
    }

    // not used now

    async loadTranslation(translationUrl) {
        const script = document.createElement('script')
        script.src = translationUrl
        document.body.appendChild(script)

        script.onload = () => {
            // console.log(globalThis.tinyMCECustomTranslations)
        }

        // return await this.isLoaded()
    }
}
