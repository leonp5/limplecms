import RemoveAlert from './components/utils/RemoveAlert'
import Initializer from './components/initializer/Initializer'

document.RemoveAlert = RemoveAlert

const initializer = new Initializer()
initializer.init()
