import SunEditorBase from './SunEditorBase'
import LocaleSetter from '../../components/utils/LocaleSetter'

export default class JsEditor {
    constructor() {
        this.locale = new LocaleSetter().get()
        this.contentEditor = null
    }

    init(htmlElement) {
        this.contentEditor = SunEditorBase.create(htmlElement, {
            height: '50vh' || 'auto',
            buttonList: [
                ['undo', 'redo'],
                ['font', 'fontSize', 'formatBlock'],
                ['paragraphStyle', 'blockquote'],
                [
                    'bold',
                    'underline',
                    'italic',
                    'strike',
                    'subscript',
                    'superscript',
                ],
                ['fontColor', 'hiliteColor', 'textStyle'],
                ['outdent', 'indent'],
                ['align', 'horizontalRule', 'list', 'lineHeight'],
                ['table', 'link', 'video' /** ,'math' */], // You must add the 'katex' library at options to use the 'math' plugin. // You must add the "imageGalleryUrl".
                /** ['imageGallery'] */ [
                    'fullScreen',
                    'preview',
                    'showBlocks',
                    'codeView',
                ],
                ['removeFormat'],
            ],
        })

        this.contentEditor.hide()

        document
            .getElementById('hcms-content-update-submit')
            .addEventListener('click', (e) => {
                this.handleSubmit(e, htmlElement.parentNode)
            })

        this.revealEditor(htmlElement)
    }

    handleSubmit(e, formElement) {
        e.preventDefault()
        this.contentEditor.save()
        formElement.submit()
    }

    revealEditor(textAreaElement) {
        const delay = 300
        textAreaElement.parentNode.classList.add('show')
        const spinner = document.getElementById('hcms-editor-spinner')
        setTimeout(() => {
            spinner.classList.add('fade')
        }, delay)
        setTimeout(() => {
            spinner.remove()
            this.contentEditor.show()
        }, delay * 2.2)
    }
}
