import tinymce from 'tinymce/tinymce'
import 'tinymce/icons/default'
import 'tinymce/themes/silver'
// Plugins
import 'tinymce/plugins/paste'
import 'tinymce/plugins/link'
import 'tinymce/plugins/fullscreen'
import 'tinymce/plugins/quickbars'
import 'tinymce/plugins/image'
import 'tinymce/plugins/imagetools'
import 'tinymce/plugins/table'
import 'tinymce/plugins/preview'
import 'tinymce/plugins/contextmenu'
import 'tinymce/plugins/code'
import 'tinymce/plugins/searchreplace'
import 'tinymce/plugins/help'
import 'tinymce/plugins/visualblocks'
import 'tinymce/plugins/emoticons'
import 'tinymce/plugins/charmap'
import 'tinymce/plugins/hr'
import 'tinymce/plugins/lists'

import LocaleSetter from '../../components/utils/LocaleSetter'

// Important: Not used. SunEditor is used at the moment

export default class JsEditor {
    constructor() {
        this.elementId = null
        this.editorHeight = '72vh'
        this.skinUrl =
            window.location.protocol +
            '//' +
            window.location.host +
            '/assets/backend/tiny/ui/oxide'
        this.emoticonsUrl =
            window.location.protocol +
            '//' +
            window.location.host +
            '/assets/backend/tiny/emojis.min.js'
        this.locale = new LocaleSetter().get()
    }

    init(htmlElement) {
        htmlElement.style.height = this.editorHeight
        window.addEventListener('load', (event) => {
            this.revealEditor(htmlElement)
        })
        this.elementId = htmlElement.id
        tinymce.init({
            selector: '#' + this.elementId,
            language: this.locale,
            language_url: this.setLanguageUrl(),
            plugins: [
                'paste',
                'link',
                'fullscreen',
                'quickbars',
                'image',
                'imagetools',
                'table',
                'preview',
                'contextmenu',
                'code',
                'searchreplace',
                'help',
                'visualblocks',
                'emoticons',
                'charmap',
                'hr',
                'lists',
            ],
            height: this.editorHeight,
            width: '100%',
            skin_url: this.skinUrl,
            emoticons_database_url: this.emoticonsUrl,
            resize: false,
            statusbar: true,
            branding: false,
            style_formats_autohide: true,
            toolbar_mode: 'floating',
            toolbar_sticky: true,
            quickbars_insert_toolbar: false,
            // The image, table etc. context menu section is empty unless the selected element is an image
            contextmenu: 'link image table emoticons',
            menu: this.menu(),
            quickbars_selection_toolbar: this.quickSelectionToolbar(),
            toolbar: this.toolbar(),
            toolbar_groups: this.toolbarGroups(),
            image_title: true,
            // file picker callback enables the button to choose a file from the user disk
            file_picker_types: 'image',
            file_picker_callback: this.filePicker,
            setup: this.handleTabPress,
        })
    }

    menu() {
        return {
            file: {
                title: 'File',
                items: 'newdocument | print ',
            },
            edit: {
                title: 'Edit',
                items: 'undo redo | cut copy paste | selectall | searchreplace',
            },
            view: {
                title: 'View',
                items: 'visualaid visualblocks',
            },
            insert: {
                title: 'Insert',
                items: 'image link emoticons | charmap hr',
            },
            format: {
                title: 'Format',
                items:
                    'bold italic underline strikethrough superscript subscript codeformat | formats blockformats fontformats fontsizes align lineheight | forecolor backcolor | removeformat',
            },
            tools: {
                title: 'Tools',
                items: 'preview fullscreen code',
            },
            table: {
                title: 'Table',
                items: 'inserttable | cell row column | tableprops deletetable',
            },
            help: { title: 'Help', items: 'help' },
        }
    }

    toolbar() {
        return 'fullscreen | undo redo | formatgroup | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | image'
    }

    toolbarGroups() {
        return {
            formatgroup: {
                icon: 'format',
                tooltip: 'Formatting',
                items:
                    'styleselect | fontsizeselect | bold italic underline | forecolor backcolor | superscript subscript',
            },
        }
    }

    quickSelectionToolbar() {
        return 'bold italic underline | styleselect | quicklink blockquote'
    }

    setLanguageUrl() {
        return (
            window.location.protocol +
            '//' +
            window.location.host +
            `/assets/backend/translations/TinyMCE/${this.locale}.js`
        )
    }

    revealEditor(textAreaElement) {
        textAreaElement.parentNode.classList.add('show')
        const spinner = document.getElementById('hcms-editor-spinner')
        spinner.classList.add('fade')
        setTimeout(() => {
            spinner.remove()
        }, 250)
    }

    filePicker(cb, value, meta) {
        // content of this method is from TinyMCE Website
        // https://www.tiny.cloud/docs/demo/file-picker/
        const input = document.createElement('input')
        input.setAttribute('type', 'file')
        input.setAttribute('accept', 'image/*')

        input.onchange = function () {
            const file = this.files[0]

            const reader = new FileReader()
            reader.onload = function () {
                const id = 'blobid' + new Date().getTime()
                const blobCache = tinymce.activeEditor.editorUpload.blobCache
                const base64 = reader.result.split(',')[1]
                const blobInfo = blobCache.create(id, file, base64)
                blobCache.add(blobInfo)

                cb(blobInfo.blobUri(), { title: file.name })
            }
            reader.readAsDataURL(file)
        }

        input.click()
    }

    handleTabPress(editor) {
        editor.on('keyDown', (e) => {
            if (e.keyCode === 9) {
                // tab pressed
                editor.execCommand('mceInsertContent', false, '&emsp;&emsp;') // inserts tab
                e.preventDefault()
            }
        })
    }
}
