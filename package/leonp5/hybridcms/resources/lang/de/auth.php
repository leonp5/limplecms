<?php

return [

  /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

  'failed' => 'Keine passenden Benutzerdaten gefunden!',
  'throttle' => 'Zu viele Login Versuche. Bitte versuche es in :seconds Sekunden erneut.',

  'register.username' => 'Benutzername',
  'register.email' => 'E-Mail Adresse',
  'register.pw' => 'Passwort',
  'register.cpw' => 'Passwort bestätigen',
  'register' => 'Registrieren',

  'login.login' => 'Login',
  'login.username' => 'Benutzername',
  'login.password' => 'Passwort',
  'login.remember' => 'Merken',
  'login.missing.password' => 'Bitte gib ein Passwort ein',
  'login.missing.identity' => 'Bitte gib einen Benutzernamen ein',

  'logout.successful' => 'Erfolgreich ausgeloggt!'
];
