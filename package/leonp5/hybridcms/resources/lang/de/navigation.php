<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Navigation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | navigation.
    |
    */

    'pages' => 'Seiten',
    'pages.overview' => 'Seitenübersicht',
    'pages.create.new' => 'Neue Seite anlegen',

    'user.management' => 'Benutzerverwaltung',
    'users.overview' => 'Alle Benutzer',
    'users.create' => 'Benutzer anlegen',

    'languages' => 'Sprachauswahl',
    'languages.settings' => 'Spracheinstellungen',

    'templates.settings' => 'Template Einstellungen',

    'user.dropdown.settings' => 'Einstellungen',
    'user.dropdown.account' => 'Account',
    'user.dropdown.user' => 'Benutzer',

    'breadcrumb.dashboard' => 'Dashboard',
    'breadcrumb.pages.overview' => 'Seitenübersicht',
    'breadcrumb.pages.new' => 'Neue Seite',
    'breadcrumb.pages.edit' => 'Seite bearbeiten',

    'breadcrumb.users.overview' => 'Benutzerübersicht',
    'breadcrumb.users.new' => 'Neuer Benutzer',
    'breadcrumb.users.edit' => 'Benutzer bearbeiten',

    'breadcrumb.languages' => 'Spracheinstellungen',

    'breadcrumb.templates' => 'Template Einstellungen',
    'breadcrumb.templates.fields' => 'Template Felder',
];
