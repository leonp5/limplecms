<?php

return [

    // Pages
    'title.pages.index' => 'Seitenübersicht',
    'title.page.edit' => 'Seite bearbeiten',
    'title.page.create' => 'Seite erstellen',
    'title.preview' => '":templateName" Vorschau',
    'title.page.preview' => '":pageTitle" Vorschau',
    'title.field.edit' => '":fieldName" bearbeiten',
    'description.preview' => 'Hier steht die Beschreibung, die u.a. für SEO Optimierung wichtig ist.',
    'heading.page.create' => 'Seite erstellen',
    'template.no-preview.error' => 'Für ":templateName" ist momentan keine Vorschau verfügbar!',
    'template.preview.generation.error' => 'Es ist ein Fehler beim Erstellen der Vorschau aufgetreten!',
    'page.create.error' => 'Es ist ein Fehler aufgetreten!',
    'edit.status' => 'Status',
    'edit.title' => 'Seitentitel',
    'edit.url' => 'URL',
    'edit.description' => 'Beschreibung',
    'edit.description.preview' => 'Vorschau',
    'edit.page' => 'Seite bearbeiten',
    'edit.submit' => 'Speichern',
    'general.page.content' => 'Allgemeine Seiteninhalte',
    'page.content' => 'Seiteninhalte',
    'chosen.template' => 'Gewähltes Template',
    'table.heading.deletable' => 'Löschbar',
    'table.heading.available.lang' => 'Verfügbare Sprachen',
    'table.heading.page.title' => 'Seitentitel',
    'delete.error' => 'Seite konnte nicht gelöscht werden!',
    'page.create.necessary' => 'Für die Bearbeitung dieses Feldes die Seite zuerst anlegen.',

    // Editor
    'content.field.open.editor' => 'Im Editor öffnen',
    'content.field.editor.abort' => 'Abbrechen',
    'content.field.editor.submit' => 'Speichern',
    'content.field.editor.field.required' => 'Das Editor Feld darf nicht leer sein!',
    'content.field.editor.missing.language' => 'Das Sprachattribut fehlt.',
    'content.field.editor.missing.field-id' => 'Die Feld ID fehlt.',
    'content.field.editor.update-error' => 'Es ist ein Fehler aufgetreten.',
    'content.field.editor.update-success' => 'Inhalte erfolgreich gespeichert',

    'create.submit' => 'Seite anlegen',
    'edit.abort' => 'Abbrechen',
    'no.template.fields' => 'Das Template hat keine Template Felder oder sie wurden bisher nicht gescannt.',
    'content.field.get.error' => 'Beim Abrufen der Daten ist ein Fehler aufgetreten!',
    'content.field.edit' => 'Inhalt bearbeiten',
    'content.field.edit.abort' => 'Abbrechen',
    'content.field.update' => 'Änderungen speichern',
    'content.field.update.success' => 'Änderungen für ":fieldName" gespeichert!',
    'content.field.update.error' => 'Update fehlgeschlagen!',
    'content.field.placeholder' => 'Platz für deine Inhalte',
    'js.editor.attribution' => 'Editor powered by :editorName',

    //Content field types
    'content.field.type.text' => 'Textfeld',
    'content.field.type.editor' => 'Mit Editor',

    // Users
    'title.users.index' => 'Benutzerübersicht',
    'title.users.edit' => '":username" bearbeiten',
    'title.users.add' => 'Benutzer hinzufügen',

    // Languages
    'title.languages.index' => 'Spracheinstellungen',

    // Templates
    'title.templates.index' => 'Template Einstellungen',
    'title.templates.fields' => 'Template Felder'
];
