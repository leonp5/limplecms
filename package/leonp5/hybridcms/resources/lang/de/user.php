<?php

return [
    // user roles
    'role.admin' => 'Administrator',
    'role.editor' => 'Editor',
    'role.author' => 'Autor',

    'admin.add.user' => 'Neuen Benutzer anlegen',

    // user table
    'admin.table.users.no-users' => 'Es gibt noch keine Benutzer',
    'admin.table.user.name' => 'Benutzername',
    'admin.table.user.role' => 'Rolle',
    'admin.table.user.last-online' => 'Zuletzt online',

    // user create, edit & delete
    'admin.delete.user.modal.text' => 'Den Benutzer ":user" löschen?',
    'admin.delete.user.confirm' => 'Benutzer löschen',
    'admin.delete.user.success' => '":username" gelöscht!',
    'admin.edit.user' => '":username" bearbeiten',
    'admin.edit.user.role' => 'Rolle(n)',
    'admin.edit.user.username' => 'Benutzername',
    'admin.edit.user.basic.data' => 'Basisdaten',
    'admin.edit.user.new.pw' => 'Neues Passwort',
    'admin.edit.user.new.pw.confirm' => 'Passwort bestätigen',
    'admin.save.user.admin.role.error' => 'Du kannst deine Adminrechte nicht löschen!',
    'admin.save.user.missing.role.error' => 'Ein Benutzer braucht mindestens eine Rolle!',
    'admin.save.user.short.name.error' => 'Es muss ein Benutzername mit mindestes :minLength Buchstaben vergeben werden!',
    'admin.save.user.name.exists.error' => 'Der Benutzername ":name" existiert bereits',
    'admin.save.user.pw.empty.error' => 'Beide Passwort Felder müssen ausgefüllt werden!',
    'admin.save.user.pw.not.identic.error' => 'Die eingegeben Passwörter stimmen nicht überein!',
    'admin.save.user.short.pw.error' => 'Das Passwort muss mindestens :minLength Zeichen lang sein!',
    'admin.save.user.capital.pw.error' => 'Das Passwort muss mindestens einen Großbuchstaben enthalten!',
    'admin.save.user.number.pw.error' => 'Das Passwort muss mindestens eine Zahl enthalten!',
    'admin.save.user.success' => 'Benutzer erfolgreich angelegt!',
    'admin.create.user' => 'Neuen Benutzer anlegen',
    'admin.create.user.submit' => 'Benutzer anlegen',
    'admin.create.user.username' => 'Benutzername',
    'admin.create.user.new.pw' => 'Neues Passwort',
    'admin.create.user.new.pw.confirm' => 'Passwort bestätigen',
    'admin.create.user.role' => 'Rolle(n)',
];
