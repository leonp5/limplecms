<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to show everything concerning the
    | admin dashboard and actions.
    |
    */

    // General
    'update.data.success' => 'Changes successful saved!',

    // Pages table
    'create.new.page' => 'Create new page',
    'page.create' => 'Create page',
    'page.select.template' => 'Select template',
    'page.select.template.abort' => 'Cancel',
    'page.select.template.preview' => 'Template preview',
    'page.select.template.preview.unavailable' => 'Template preview isn\'t available',
    'page.status.draft' => 'Draft',
    'page.status.inactive' => 'Inactive',
    'page.status.published' => 'Published',
    'page.no-pages' => 'There are no pages',
    'page.title.missing' => 'Missing title',
    'page.url.missing' => 'Missing url',

    // Page create, edit & delete

    'delete.modal.title' => 'Confirm deletion',
    'delete.modal.text' => 'Permanently delete this page?',
    'delete.abort' => 'Cancel',
    'delete.page.confirm' => 'Delete page',

    'page.update.success' => 'Page update saved!',
    'page.save.success' => 'Page saved!',

    'page.delete.success' => 'Page deleted!',

    // url error messages
    'invalid.url' => 'Invalid URL',
    'url.already.exists' => 'URL already exists for the language ":language"',
    'min.one.url' => 'The page needs at least in one language a valid url',
    'page.need.different.urls' => 'It is not allowed, that the url is identical with one url of this page in another language',

    'page.missing.title.min' => 'The page needs at least in one language a title',

    // Languages
    'languages.multilang.disabled' => 'Multilanguage disabled',
    'languages.active.heading' => 'Active languages',
    'languages.all.heading' => 'All languages',
    'languages.settings' => 'Language settings',
    'languages.table.heading.language' => 'Language',
    'languages.table.heading.main-language' => 'Main language',
    'languages.table.heading.disable' => 'Disable',
    'languages.table.heading.delete' => 'Delete',
    'languages.table.button.update' => 'Save',
    'languages.update.one.main.language.error' => 'Exactly one language must be set as the main language!',
    'languages.update.language.all.deleted.error' => 'Not all languages may be deleted!',
    'languages.update.language.all.deactivated.error' => 'Not all languages may be deactivated!',
    'languages.update.success' => 'Language settings successfully updated!',
    'languages.update.error' => 'Language settings update failed!',
    'languages.delete.warning' => 'Deletes all website content of the selected language. Deactivating the language is recommended!',

    // Templates
    'templates.settings.heading' => 'Template settings',
    'templates.scan' => 'Read template files',
    'templates.field.already.exist.error' => '":fieldName" already exists in the template!',
    'templates.scan.update.success' => 'Template scan & update successful!',
    'templates.data.attribute.missing.error' => 'In ":template" the attribute ":attribute" is missing!',
    'templates.overview.tab.all-templates' => 'All templates',
    'templates.overview.tab.single-use' => 'Single use',
    'templates.overview.tab.components' => 'Components',
    'templates.overview.tab.partials' => 'Partials',
    'templates.table.heading.name' => 'Name',
    'templates.table.heading.partial' => 'Partial Template',
    'templates.table.heading.single-use' => 'Single use',
    'templates.table.heading.component' => 'Component',
    'templates.table.heading.disabled' => 'Disabled',
    'templates.scan.not-happened' => 'The template files have not yet been read in',
    'templates.status.update.error' => 'Status update failed!',
    'template.fields.table.heading.name' => 'Field name',
    'template.fields.table.heading.template-id' => 'Template Id',
    'template.fields.table.heading.type' => 'Type',
    'template.fields.table.type-info' => 'Plain text field or with editor.',
    'template.fields.no-fields' => 'No template fields found!',
    'template.delete.modal.title' => 'Unused templates',
    'template.delete.modal.confirm' => 'Delete templates',
    'template.delete.modal.keep' => 'Keep templates',
    'template.delete.success' => 'Templates removed from the database & deleted HTML/Blade files.',
    'template.delete.templates.missing.error' => 'No templates have been selected for deletion!',
    'template.scan.settings.unused' => 'Unused',
    'template.scan.settings.partials' => 'Scan partials',
    'template.scan.settings.components' => 'Scan components',
    'template.scan.unused.tooltip' => 'Searches for unused templates and displays them after the scan.',
];
