<?php

return [

    // Pages
    'title.pages.index' => 'Pages overview',
    'title.page.edit' => 'Edit page',
    'title.page.create' => 'Create page',
    'title.preview' => '":templateName" preview',
    'title.page.preview' => '":pageTitle" preview',
    'title.field.edit' => 'Edit ":fieldName"',
    'description.preview' => 'Here is the description, which is important for SEO optimisation, among other things.',
    'heading.page.create' => 'Create page',
    'template.no-preview.error' => 'There is no preview for ":templateName"!',
    'template.preview.generation.error' => 'An error occured during the preview generation!',
    'page.create.error' => 'An Error occured!',
    'edit.status' => 'Status',
    'edit.title' => 'Page title',
    'edit.url' => 'URL',
    'edit.page' => 'Edit page',
    'edit.description' => 'Description',
    'edit.description.preview' => 'Preview',
    'edit.submit' => 'Save changes',
    'general.page.content' => 'General page content',
    'page.content' => 'Page content',
    'chosen.template' => 'Chosen template',
    'table.heading.deletable' => 'Deletable',
    'table.heading.available.lang' => 'Available Languages',
    'table.heading.page.title' => 'Page title',
    'delete.error' => 'Page could not be deleted!',
    'page.create.necessary' => 'To edit this field, create the page first.',
    'content.field.editor.missing.language' => 'The language attribute is missing.',
    'content.field.editor.missing.field-id' => 'The field id is missing.',

    // Editor
    'content.field.open.editor' => 'Open in editor',
    'content.field.editor.abort' => 'Abort',
    'content.field.editor.submit' => 'Save',
    'content.field.editor.field.required' => 'The editor field must not be empty!',
    'content.field.editor.update-error' => 'An error occurred.',
    'content.field.editor.update-success' => 'Content successfully saved',

    'create.submit' => 'Create page',
    'edit.abort' => 'Cancel',
    'no.template.fields' => 'The template has no template fields or they haven\'t been scanned yet.',
    'content.field.get.error' => 'An error occurred while retrieving the data!',
    'content.field.edit' => 'Edit content',
    'content.field.edit.abort' => 'Abort',
    'content.field.update' => 'Save changes',
    'content.field.update.success' => 'Changes for ":fieldName" saved!',
    'content.field.update.error' => 'Update failed!',
    'content.field.placeholder' => 'Space for your content',
    'js.editor.attribution' => 'Editor powered by :editorName',

    //Content field types
    'content.field.type.text' => 'Text field',
    'content.field.type.editor' => 'With editor',

    // Users
    'title.users.index' => 'User overview',
    'title.users.edit' => 'Edit ":username"',
    'title.users.add' => 'Add user',

    // Languages
    'title.languages.index' => 'Language settings',

    // Templates
    'title.templates.index' => 'Template settings',
    'title.templates.fields' => 'Template fields'
];
