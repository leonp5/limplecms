<?php

return [
    // user roles
    'role.admin' => 'Admin',
    'role.editor' => 'Editor',
    'role.author' => 'Author',

    'admin.add.user' => 'Add new user',

    // user table
    'admin.table.users.no-users' => 'There are no users',
    'admin.table.user.name' => 'Username',
    'admin.table.user.role' => 'Role',
    'admin.table.user.last-online' => 'Last online',

    // user create, edit & delete
    'admin.delete.user.modal.text' => 'Delete the user ":user"?',
    'admin.delete.user.confirm' => 'Delete user',
    'admin.delete.user.success' => '":username" deleted!',
    'admin.edit.user' => 'Edit ":username"',
    'admin.edit.user.role' => 'Role(s)',
    'admin.edit.user.username' => 'Username',
    'admin.edit.user.basic.data' => 'Basic data',
    'admin.edit.user.new.pw' => 'New password',
    'admin.edit.user.new.pw.confirm' => 'Confirm password',
    'admin.save.user.admin.role.error' => 'You can\'t delete your own admin rights!',
    'admin.save.user.missing.role.error' => 'A user needs at least one role!',
    'admin.save.user.short.name.error' => 'A username needs at least :minLength characters!',
    'admin.save.user.name.exists.error' => 'The username ":name" already exists!',
    'admin.save.user.pw.empty.error' => 'Both password fields are required!',
    'admin.save.user.pw.not.identic.error' => 'The entered Passwords aren\'t identical!',
    'admin.save.user.short.pw.error' => 'The password must have at least :minLength characters!',
    'admin.save.user.capital.pw.error' => 'The password must contain at least one capital letter!',
    'admin.save.user.number.pw.error' => 'The password must contain at least one number!',
    'admin.save.user.success' => 'User successfully created!',
    'admin.create.user' => 'Create new user',
    'admin.create.user.submit' => 'Create user',
    'admin.create.user.username' => 'Username',
    'admin.create.user.new.pw' => 'New password',
    'admin.create.user.new.pw.confirm' => 'Confirm password',
    'admin.create.user.role' => 'Role(s)',
];
