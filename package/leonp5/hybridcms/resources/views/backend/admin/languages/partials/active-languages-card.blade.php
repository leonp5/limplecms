<form class="card" id="hcms-language-checkboxes" action="{{route('languages.updateLanguages')}}"
                method="POST" data-hcms-function="HandleLanuageCheckboxes">
                @csrf
                <h5 class="card-header">{{__('hybridcms::admin.languages.active.heading')}}
                    <span class="badge badge-pill badge-primary float-right"
                        id="hcms_active_lang_count">{{count($activeLanguages)}}</span>
                </h5>
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead class="table-light">
                            <tr>
                                <th scope="col"></th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.language')}}</th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.main-language')}}</th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.disable')}}</th>
                                <th scope="col">{{__('hybridcms::admin.languages.table.heading.delete')}}
                                    <a hred="#" class="badge bg-danger text-light"
                                    data-hcms-function="Tooltip"
                                     data-hcms-tooltip="{{__('hybridcms::admin.languages.delete.warning')}}"
                                    data-tooltip-position="top">i</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($activeLanguages as $language)
                            <tr class="{{$language->disabled ? "bg-light text-dark" : ""}}"
                                data-hcms-language-id="{{$language->id}}"
                                data-hcms-language-alpha_2="{{$language->alpha_2}}">
                                <input type="hidden" name="languages[{{$language->lang_id}}][alpha_2]"
                                    value="{{$language->alpha_2}}" />
                                <td>
                                    <span class="flag-icon flag-icon-{{$language->alpha_2}}"></span>
                                </td>
                                <td>
                                    <p id="lang_name_{{$language->alpha_2}}">{{$language->langName}}</p>
                                </td>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input"
                                            id="main_{{$language->lang_id}}"
                                            {{$language->main_language ? "checked" : "disabled"}}
                                            value="{{$language->main_language}}"
                                            name="languages[{{$language->lang_id}}][main_language]"
                                            data-hcms-main-language="">
                                        <label class="custom-control-label" for="main_{{$language->lang_id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input"
                                            id="disabled_{{$language->lang_id}}"
                                            {{$language->main_language ? "disabled" : ""}}
                                            {{$language->disabled ? "checked" : ""}} value="{{$language->disabled}}"
                                            name="languages[{{$language->lang_id}}][disabled]"
                                            data-hcms-lang-disabled="">
                                        <label class="custom-control-label"
                                            for="disabled_{{$language->lang_id}}"></label>
                                    </div>
                                </td>
                                <td>
                                    <x-hybridcms-svg svg="trash" fill="none" stroke="currentColor"
                                        class="{{$language->main_language ? 'svg-delete hcms-cursor-forbidden' : 'svg-delete'}}"
                                        name="delete" />
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">
                    <div class="d-flex justify-content-end">
                        <button class="btn btn-success hcms-cursor-forbidden" type="submit" disabled
                            id="hmcs-update-languages">
                            {{__('hybridcms::admin.languages.table.button.update')}}</button>
                    </div>
                </div>
            </form>