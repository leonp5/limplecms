<div class="dropdown">
    <button class="btn shadow-none" type="button" id="hcms-scan-options" data-toggle="dropdown" aria-expanded="false">
        <x-hybridcms-svg svg="settings" class="c-sidebar-nav-icon" viewBox="512 512" />
    </button>
    <div class="dropdown-menu" aria-labelledby="hcms-scan-options">
        <form class="p-3" id="hcms-scan-options-form" method="POST"
            action="{{route('templates.toggle.scan-options')}}">

            <div class="custom-control custom-switch hcms-switch-info">
                <input id="check_unused" type="checkbox" class="custom-control-input" {{$scanOptions->check_unused ? "checked" : ""}}
                    value="{{$scanOptions->check_unused}}" name="check_unused" form="hcms-scan-options-form">
                <label class="custom-control-label" for="check_unused">
                    {{__('hybridcms::admin.template.scan.settings.unused')}}
                    <a hred="#" class="badge bg-danger text-light"
                                    data-hcms-function="Tooltip"
                                     data-hcms-tooltip="{{__('hybridcms::admin.template.scan.unused.tooltip')}}"
                                    data-tooltip-position="left">i</a>
                </label>
            </div>

            <div class="dropdown-divider"></div>

            <div class="custom-control custom-switch hcms-switch-info">
                <input id="scan_partials" type="checkbox" class="custom-control-input" {{$scanOptions->scan_partials ? "checked" : ""}}
                    value="{{$scanOptions->scan_partials}}" name="scan_partials" form="hcms-scan-options-form">
                <label class="custom-control-label" for="scan_partials">
                    {{__('hybridcms::admin.template.scan.settings.partials')}}
                </label>
            </div>

            <div class="dropdown-divider"></div>

            <div class="custom-control custom-switch hcms-switch-info">
                <input id="scan_components" type="checkbox" class="custom-control-input" {{$scanOptions->scan_components ? "checked" : ""}}
                    value="{{$scanOptions->scan_components}}" name="scan_components" form="hcms-scan-options-form">
                <label class="custom-control-label" for="scan_components">
                    {{__('hybridcms::admin.template.scan.settings.components')}}
                </label>
            </div>
        </form>
    </div>
</div>