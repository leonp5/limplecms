@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.templates.fields'))

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['url' => route('templates.index'), 'name' => __('hybridcms::navigation.breadcrumb.templates')],
        ['name' => __('hybridcms::navigation.breadcrumb.templates.fields')]
        ]])
@endsection

@section('content')
<div class="container-fluid">
    <h3 class="mb-3">{{__('hybridcms::pages.title.templates.fields')}} </h3>
    <div class="card">
        <div class="card-header">
            <div class="d-flex">
                <p class="h4 mr-2">Template:</p>
                <p class="h4 text-info"> {{$template->template_name}}</p>

            </div>
        </div>
        <div class="card-body">
            @if($templateFields->count() > 0)
            <table class="table table-responsive-sm">
                <thead class="table-light">
                    <tr>
                        <th scope="col">{{__('hybridcms::admin.template.fields.table.heading.name')}}</th>
                        <th scope="col">{{__('hybridcms::admin.template.fields.table.heading.template-id')}}</th>
                        <th scope="col">{{__('hybridcms::admin.template.fields.table.heading.type')}}
                            <a hred="#" class="badge bg-success text-light" data-hcms-function="Tooltip"
                                data-hcms-tooltip="{{__('hybridcms::admin.template.fields.table.type-info')}}"
                                data-tooltip-position="top">i</a></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($templateFields as $field)
                    <tr>
                        <td>
                            <p>{{$field->field_name}}</p>
                        </td>
                        <td>
                            <p>{{$field->template_id}}</p>
                        </td>
                        <td>
                           <p>{{__($field->label)}}</p>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
            <p class="h5 text-center text-muted">{{__('hybridcms::admin.template.fields.no-fields')}}</p>
            @endif
        </div>
    </div>
</div>
@endsection