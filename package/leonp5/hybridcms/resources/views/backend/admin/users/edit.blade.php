@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.users.edit', ['username' => $user->getUserName()]))

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['url' => route('users.index'), 'name' => __('hybridcms::navigation.breadcrumb.users.overview')],
        ['name' => __('hybridcms::navigation.breadcrumb.users.edit')]
        ]])
@endsection

@section('content')

<div class="container">
    <h3>{{__('hybridcms::user.admin.edit.user', ['username' => $user->getUserName()])}}</h3>

    <form action="{{route('users.update', ['user' => $user->getUserUuid()])}}" method="POST">
        @method('PUT')

        @include('hybridcms::backend.admin.users.partials.fields')
        
        <div class="form-group d-flex justify-content-between">
            <a href="{{ url()->previous() }}" class="btn btn-secondary">{{__('hybridcms::app.abort')}}</a>
            <button type="submit" class="btn btn-primary">{{__('hybridcms::app.save')}}</button>
        </div>
    </form>

</div>

@endsection