<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if(View::hasSection('title'))
        @yield('title')
        @else
        hybridCms dev app
        @endif
    </title>

    <link href="{{ asset('assets/backend/hcms.css') }}" rel="stylesheet" type="text/css">

    @yield('styles')
    
</head>

<body>
    @auth
    @include('hybridcms::backend.layout.partials.sidebar')  
    @endauth
    <div class="c-wrapper c-fixed-components">
        @include('hybridcms::backend.layout.partials.nav')
        @yield('breadcrumb')
        <div class="c-body">
            <main class="c-main">
                @include('hybridcms::backend.layout.partials.alerts')
                @yield('content')
                @include('hybridcms::backend.layout.partials.template-select-modal')
            </main>
        </div>
    </div>
    @yield('scripts')
    <script src="{{ asset('assets/backend/hcms.js') }}" defer></script>
    <script src="{{ asset('assets/backend/hcms-vendor.js')}}"></script>
</body>

</html>