@if(count($errors) > 0)
<div class="warning-modal" onclick="RemoveAlert()">
  @foreach($errors->all() as $error)
  {{$error}} <br>
  @endforeach
</div>
@endif

@if(session("success"))
<div class="alert-modal" onclick="RemoveAlert()">
  {{session("success")}}
</div>
@endif

@if(session("error"))
<div class="warning-modal" onclick="RemoveAlert()">
  {{session("error")}}
</div>
@endif
