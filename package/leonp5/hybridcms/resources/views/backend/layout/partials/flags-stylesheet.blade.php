@if (env('FRONTEND_MULTILANG') === true)
@section('styles')
    <link href="{{ asset('assets/backend/flags.css') }}" rel="stylesheet" type="text/css">
@endsection
@endif