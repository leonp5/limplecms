<header class="c-header c-header-light c-header-fixed bg-light px-3">
    @auth
    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
        data-class="c-sidebar-lg-show" responsive="true">
        <span class="c-header-toggler-icon"></span>
    </button>
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
        data-class="c-sidebar-show">
        <span class="c-header-toggler-icon"></span>
    </button>
    <!-- Left Side Of Navbar -->
    <ul class="c-header-nav d-md-down-none">
        <li class="c-header-nav-item px-3">
            <a class="c-header-nav-link" href="{{ route('dashboard') }}">Dashboard</a>
        </li>
    </ul>
    @endauth

    <!-- Right Side Of Navbar -->
    <ul class="c-header-nav ml-auto mr-4">
        @guest
        <li class="c-header-nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
        <li class="c-header-nav-item">
            <a class="c-header-nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="" role="button" aria-haspopup="true" aria-expanded="true">
                <div class="c-avatar">
                    <x-hybridcms-svg svg="user-circle" viewBox="496 512"></x-hybridcms-svg>
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
                <div class="dropdown-header bg-light py-2">
                    <strong>{{__('hybridcms::navigation.languages')}}</strong>
                </div>
                <a class="dropdown-item" href="{{ route('localization.backend', ['locale' => 'gb']) }}">
                    <x-hybridcms-svg svg="flag_gb" class="c-icon mr-2" width="301" height="181" viewBox="301 181" />
                    GB</a>
                <a class="dropdown-item" href="{{ route('localization.backend', ['locale' => 'de']) }}">
                    <x-hybridcms-svg svg="flag_de" class="c-icon mr-2" width="301" height="181" viewBox="301 181" />
                    DE</a>
                <div class="dropdown-header bg-light py-2">
                    <strong>  {{ Auth::user()->username }}</strong>
                </div>
                <a class="dropdown-item" href="#">
                    <x-hybridcms-svg svg="account" class="c-icon mr-2" viewBox="512 512" />
                    {{__('hybridcms::navigation.user.dropdown.account')}}</a>
                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <x-hybridcms-svg svg="logout" class="c-icon mr-2" viewBox="512 512" />
                    {{ __('Logout') }}</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </li>
        @endguest
    </ul>
</header>