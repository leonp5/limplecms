<div class="modal fade" id="hcms-page-create-modal" tabindex="-1" aria-labelledby="hcms-page-create-modal-heading"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hcms-page-create-modal-heading">
                    {{__('hybridcms::admin.page.select.template')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    data-hcms-create-page-modal-dismiss>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="GET" action="{{route('templates.get')}}" id="hcms-template-select-get"></form>
                <select class="custom-select custom-select-sm" id="hcms-template-select"
                    form="hcms-template-select-get">
                    <option selected>{{__('hybridcms::admin.page.select.template')}}</option>
                </select>
                <div class="container-fluid mt-3">
                    <div class="row" id="hcms-template-preview-link">
                        <a class="d-none"
                            href="{{route('pages.demo.preview')}}">{{__('hybridcms::admin.page.select.template.preview')}}</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer mt-4">
                <a type="button" class="btn btn-secondary mr-auto" data-dismiss="modal"
                    data-hcms-create-page-modal-dismiss="">{{__('hybridcms::admin.page.select.template.abort')}}</a>
                <form method="GET" action="{{route('pages.create')}}" id="hcms-page-create">
                    <button type="submit" class="btn btn-success m-2 hcms-cursor-forbidden" form="hcms-page-create"
                        disabled>{{__('hybridcms::admin.page.create')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>