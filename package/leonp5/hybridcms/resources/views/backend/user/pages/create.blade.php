@extends('hybridcms::backend.layout.app')

@section('title', __('hybridcms::pages.title.page.create'))

@include('hybridcms::backend.layout.partials.flags-stylesheet')

@section('breadcrumb')
    @include('hybridcms::backend.layout.partials.breadcrumb', 
        ['items' => [
        ['url' => route('dashboard'), 'name' => __('hybridcms::navigation.breadcrumb.dashboard')],
        ['url' => route('pages.index'), 'name' => __('hybridcms::navigation.breadcrumb.pages.overview')],
        ['name' => __('hybridcms::navigation.breadcrumb.pages.new')]]
        ])
@endsection

@section('content')

<div class="container" data-hcms-function="DescriptionPreview">
    <h3>{{__('hybridcms::pages.heading.page.create')}}</h3>

    <form action="{{route('pages.store')}}" method="POST">

        @include('hybridcms::backend.user.pages.partials.fields')
        <div class="form-group d-flex justify-content-between">
            <a href="{{ url()->previous() }}" class="btn btn-secondary">{{__('hybridcms::pages.edit.abort')}}</a>
            <button type="submit" class="btn btn-primary">{{__('hybridcms::pages.create.submit')}}</button>
        </div>
    </form>

</div>

@endsection