<div class="modal fade" id="hcms-field-edit-modal" tabindex="-1" aria-labelledby="hcms-field-edit-modal-heading"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="hcms-field-edit-modal-heading">
                    {{__('hybridcms::pages.content.field.edit')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    data-hcms-field-edit-modal-dismiss="">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="content_field"></label>
                    <input id="content_field" class="form-control">
                </div>
                
            </div>
            <div class="modal-footer mt-4">
                <a type="button" class="btn btn-secondary mr-auto" data-dismiss="modal"
                    data-hcms-field-edit-modal-dismiss="">{{__('hybridcms::pages.content.field.edit.abort')}}</a>
                <form method="POST" id="hcms-update-content-field" action="{{route('field.update')}}">
                    <button type="submit" class="btn btn-success m-2" form="hcms-update-content-field"
                        >{{__('hybridcms::pages.content.field.update')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>