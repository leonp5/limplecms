<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="{{$meta['description'] ?? ''}}">

    <title>
        @if(View::hasSection('title'))
        @yield('title')
        @else
        {{env('FRONTEND_NAME')}}
        @endif
    </title>

    <link href="{{ asset('assets/frontend/' . env('FRONTEND_NAME') . '.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/backend/suneditor/suneditor-contents.css') }}" rel="stylesheet" type="text/css">


</head>

<body>
        @include('hybridcms::frontend.scan.layout.partials.nav')
        @include('hybridcms::frontend.scan.layout.partials.header')
        <div class="w3-sand w3-grayscale w3-large">
            @include('hybridcms::frontend.scan.layout.partials.alerts')
            @yield('content')
            @include('hybridcms::frontend.scan.layout.partials.footer')
        </div>
    <script src="{{ asset('assets/frontend/'. env('FRONTEND_NAME') .'.js') }}" defer></script>
    <script src="{{ asset('assets/frontend/'. env('FRONTEND_NAME') . '-vendor.js')}}"></script>
</body>

</html>