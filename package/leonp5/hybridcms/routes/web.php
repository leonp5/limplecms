<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

use Leonp5\Hybridcms\Auth\Routes\AuthRoutes;
use Leonp5\Hybridcms\User\Routes\UserRoutes;
use Leonp5\Hybridcms\Page\Routes\PageRoutes;
use Leonp5\Hybridcms\Account\Routes\AccountRoutes;
use Leonp5\Hybridcms\Frontend\Routes\FrontendRoutes;
use Leonp5\Hybridcms\Language\Routes\LanguageRoutes;
use Leonp5\Hybridcms\Template\Routes\TemplateRoutes;
use Leonp5\Hybridcms\Dashboard\Routes\DashboardRoutes;
use Leonp5\Hybridcms\ContentField\Routes\ContentFieldRoutes;
use Leonp5\Hybridcms\ContentEditor\Routes\ContentEditorRoutes;

$cmsPrefix = config('hybridcms.cms_prefix');
$cmsUrl = '/' . $cmsPrefix;

// Frontend

Route::group([], function () use ($cmsPrefix) {
    (new FrontendRoutes($cmsPrefix))->register();
});


// Backend (Cms)

// prefix all possible routes with /cms
Route::group(
    [
        'prefix' => $cmsUrl,
        'middleware' => 'lastonline'
    ],
    function () {

        // Set language
        Route::get('/locale/{locale}', function ($locale) {
            Session::put('locale', $locale);
            return redirect()->back();
        })->name('localization.backend');

        // Account
        Route::group([], function () {
            (new AccountRoutes)->register();
        });

        // Dashboard
        Route::group([], function () {
            (new DashboardRoutes)->register();
        });

        // Auth
        Route::group([], function () {
            (new AuthRoutes)->register();
        });

        // Page
        Route::group([], function () {
            (new PageRoutes)->register();
        });

        /**
         * Edit content fields of a page
         */
        Route::group([], function () {
            (new ContentFieldRoutes)->register();
        });

        Route::group([], function () {
            (new ContentEditorRoutes)->register();
        });



        Route::group([], function () {
            (new UserRoutes())->register();
        });

        Route::group([], function () {
            (new LanguageRoutes)->register();
        });

        Route::group([], function () {
            (new TemplateRoutes)->register();
        });
    }
);
