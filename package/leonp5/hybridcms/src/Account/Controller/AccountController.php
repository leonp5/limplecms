<?php

namespace Leonp5\Hybridcms\Account\Controller;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Controller\Controller;

class AccountController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Renderable
     * @throws BindingResolutionException
     */
    public function welcome(): Renderable
    {
        return view('hybridcms::backend.welcome');
    }
}
