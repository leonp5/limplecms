<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\App\Interfaces;

use Illuminate\Contracts\Foundation\Application;

interface HcmsDependencyProviderInterface
{

    /**
     * @return void
     */
    public function provide(Application $app): void;
}
