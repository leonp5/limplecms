<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\App\Interfaces;

interface HcmsRouteInterface
{

    /**
     * @return void
     */
    public function register(): void;
}
