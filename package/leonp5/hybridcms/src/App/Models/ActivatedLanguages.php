<?php

namespace Leonp5\Hybridcms\App\Models;

use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ActivatedLanguages extends Model
{
    use HasFactory;

    protected $guarded = [];

    /**
     * @return array 
     * @throws InvalidArgumentException 
     */
    public static function withDisabled(): array
    {
        // getting the language name in the correct language
        $langTable = 'language_' . App::getLocale();

        $activeLanguages = [];

        $activatedLanguages = DB::table('activated_languages')
            ->select(['activated_languages.lang_id', 'activated_languages.main_language', 'activated_languages.disabled'])
            ->get();

        foreach ($activatedLanguages as $language) {

            $activeLanguage = DB::table($langTable)
                ->select([$langTable . '.name as langName', $langTable . '.id', $langTable . '.alpha_2'])
                ->where($langTable . '.id', '=', $language->lang_id)
                ->first();

            $activeLanguages[] = (object) array_merge((array)$activeLanguage, (array)$language);
        }

        return $activeLanguages;
    }
}
