<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Content extends Model
{
    use HasFactory;
}
