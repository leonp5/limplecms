<?php

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Page extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo('Leonp5\Hybridcms\App\Models\User');
    }

    /**
     * 
     * @return BelongsToMany 
     */
    public function status(): BelongsToMany
    {
        return $this->belongsToMany('Leonp5\Hybridcms\App\Models\PageStatus');
    }
}
