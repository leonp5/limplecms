<?php

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TemplateField extends Model
{
    use HasFactory;

    protected $table = 'template_field';

    protected $fillable = ['template_id', 'field_name', 'content_type_id'];

    /**
     * @return BelongsTo 
     */
    public function template(): BelongsTo
    {
        return $this->belongsTo(Template::class);
    }
}
