<?php

namespace Leonp5\Hybridcms\App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        "last_online_at" => "datetime"
    ];

    /**
     * 
     * @return HasMany 
     */
    public function pages(): HasMany
    {
        return $this->hasMany('Leonp5\Hybridcms\App\Models\Page');
    }

    /**
     * 
     * @return BelongsToMany 
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany('Leonp5\Hybridcms\App\Models\Role');
    }

    /**
     * 
     * @param mixed $roles 
     * @return bool 
     */
    public function hasAnyRole($roles): bool
    {
        return null !== $this->roles()
            ->whereIn('role', $roles)->first();
    }

    /**
     * 
     * @param mixed $role 
     * @return bool 
     */
    public function hasRole($role): bool
    {
        return null !== $this->roles()
            ->where('role', $role)->first();
    }

    /**
     *
     * @return string 
     */
    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
