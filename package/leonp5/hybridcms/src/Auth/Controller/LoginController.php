<?php

namespace Leonp5\Hybridcms\Auth\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
// use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Controller\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:cms')->except('logout');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    // commenting out this line and add a specific redirectTo function for a custom redirect

    // protected $redirectTo = RouteServiceProvider::HOME;

    // needs to bring in Auth Facade

    /**
     * 
     * @return mixed 
     * @throws BindingResolutionException 
     */
    public function redirectTo()
    {
        $user = Auth::user();
        if ($user->hasRole("admin")) {
            $this->redirectTo = route('pages.index');
        } else {
            $this->redirectTo = route('dashboard');
        }

        return $this->redirectTo;
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        $login = request()->input('identity');

        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$field => $login]);

        return $field;
    }

    public function show()
    {
        return view('hybridcms::backend.auth.login');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $messages = [
            'identity.required' => __('hybridcms::auth.login.missing.identity'),
            'password.required' => __('hybridcms::auth.login.missing.password'),
        ];

        $request->validate([
            'identity' => 'required|string',
            'password' => 'required|string',
            // 'email' => 'string|exists:users',
            // 'username' => 'string|exists:users',
        ], $messages);
    }

    /**
     * 
     * @param Request $request 
     * @return RedirectResponse|JsonResponse 
     * @throws BindingResolutionException 
     */
    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route('login')->with('success', __('hybridcms::auth.logout.successful'));
    }
}
