<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Auth\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\Auth\Controller\LoginController;
use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;

class AuthRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::get('/login', [LoginController::class, 'show'])->name('login');
        Route::post('/login', [LoginController::class, 'login']);
        Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
    }
}
