<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Content;

interface ContentTableInterface
{
    public const CONTENT_TABLE_PREFIX = 'content_';
    public const CONTENT_FIELD_TABLE_PREFIX = 'template_content_field_';


    public const CONTENT_TEXT_TAG = 'data-hcms-content-text';
    public const CONTENT_TYPE_TEXT = 1;

    public const CONTENT_EDITOR_TAG = 'data-hcms-content-editor';
    public const CONTENT_TYPE_EDITOR = 2;
}
