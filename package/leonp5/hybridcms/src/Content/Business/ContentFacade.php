<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business;

use Illuminate\Contracts\Container\BindingResolutionException;
use Leonp5\Hybridcms\Content\Transfer\ContentAddRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentAddResponseTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateResponseTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateResponseTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataResponseTransfer;

final class ContentFacade implements ContentFacadeInterface
{
    /**
     * @var ContentFacadeBusinessFactory
     */
    private ContentFacadeBusinessFactory $factory;

    /**
     * @param ContentFacadeBusinessFactory $factory
     * 
     * @return void
     */
    public function __construct(ContentFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param string $tableName
     * 
     * @return void
     * @throws BindingResolutionException
     */
    public function createTable(string $tableName): void
    {
        $this->factory
            ->getContentTableCreateProcessor()
            ->create($tableName);
    }

    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     */
    public function validateContentTitle(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer {
        return $this->factory
            ->getContentValidateProcessor()
            ->validateTitle($validateContentRequestTransfer, $validateContentResponseTransfer);
    }

    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     */
    public function validateContentUrl(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer {
        return $this->factory
            ->getContentValidateProcessor()
            ->validateUrl($validateContentRequestTransfer, $validateContentResponseTransfer);
    }

    /**
     * @param ContentAddRequestTransfer $contentAddRequestTransfer
     * 
     * @return ContentAddResponseTransfer 
     */
    public function addContent(
        ContentAddRequestTransfer $contentAddRequestTransfer
    ): ContentAddResponseTransfer {
        return $this->factory
            ->getContentAddProcessor()
            ->add($contentAddRequestTransfer);
    }

    /**
     * @param ContentUpdateRequestTransfer $contentUpdateRequestTransfer
     * 
     * @return ContentUpdateResponseTransfer
     * @throws BindingResolutionException
     */
    public function updateContent(
        ContentUpdateRequestTransfer $contentUpdateRequestTransfer
    ): ContentUpdateResponseTransfer {
        return $this->factory
            ->getContentUpdateProcessor()
            ->updateContent($contentUpdateRequestTransfer);
    }

    /**
     * @param ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
     * 
     * @return ContentFieldDataResponseTransfer
     */
    public function getContentFieldData(
        ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
    ): ContentFieldDataResponseTransfer {
        return $this->factory
            ->getContentFieldDataProcessor()
            ->getContentFieldData($contentFieldDataRequestTransfer);
    }
}
