<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;
use Leonp5\Hybridcms\Content\Business\Processor\Add\ContentAddProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Update\ContentUpdateProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Validation\ContentValidateProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate\ContentTableCreateProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Get\GetContentFieldDataProcessorInterface;

class ContentFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return ContentTableCreateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getContentTableCreateProcessor(): ContentTableCreateProcessorInterface
    {
        return $this->app->make(ContentTableCreateProcessorInterface::class);
    }

    /**
     * @return ContentValidateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getContentValidateProcessor(): ContentValidateProcessorInterface
    {
        return $this->app->make(ContentValidateProcessorInterface::class);
    }

    /**
     * @return ContentAddProcessorInterface
     * @throws BindingResolutionException
     */
    public function getContentAddProcessor(): ContentAddProcessorInterface
    {
        return $this->app->make(ContentAddProcessorInterface::class);
    }

    /**
     * @return ContentUpdateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getContentUpdateProcessor(): ContentUpdateProcessorInterface
    {
        return $this->app->make(ContentUpdateProcessorInterface::class);
    }

    /**
     * @return GetContentFieldDataProcessorInterface 
     * @throws BindingResolutionException 
     */
    public function getContentFieldDataProcessor(): GetContentFieldDataProcessorInterface
    {
        return $this->app->make(GetContentFieldDataProcessorInterface::class);
    }
}
