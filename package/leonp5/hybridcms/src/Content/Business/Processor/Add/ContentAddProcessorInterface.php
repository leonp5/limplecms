<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Add;

use Leonp5\Hybridcms\Content\Transfer\ContentAddRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentAddResponseTransfer;

interface ContentAddProcessorInterface
{
    /**
     * @param ContentAddRequestTransfer $contentAddRequestTransfer
     * 
     * @return ContentAddResponseTransfer
     * @throws InvalidArgumentException
     */
    public function add(
        ContentAddRequestTransfer $contentAddRequestTransfer,
    ): ContentAddResponseTransfer;
}
