<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

final class ContentTableCreateProcessor implements ContentTableCreateProcessorInterface
{
    /**
     * @param string $tableName
     * 
     * @return void
     */
    public static function create(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('page_id');
            $table->string('title')->nullable();
            $table->string('description', 200)->nullable();
            $table->string('url')->nullable();
            $table->softDeletes();
        });
    }
}
