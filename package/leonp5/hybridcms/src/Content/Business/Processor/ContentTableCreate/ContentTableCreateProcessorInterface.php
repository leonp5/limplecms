<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate;

interface ContentTableCreateProcessorInterface
{
    /**
     * @param string $tableName
     * 
     * @return void 
     */
    public static function create(string $tableName): void;
}
