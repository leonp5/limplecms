<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Get;

use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Utility\Traits\LocaleChangerTrait;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataResponseTransfer;

final class GetContentFieldDataProcessor implements GetContentFieldDataProcessorInterface
{
    use LocaleChangerTrait;

    /**
     * @var LanguageFacadeInterface
     */
    private LanguageFacadeInterface $languageFacade;

    /**
     * @var string|null
     */
    private ?string $locale = null;

    /**
     * @param LanguageFacadeInterface $languageFacade
     * 
     * @return void 
     */
    public function __construct(
        LanguageFacadeInterface $languageFacade
    ) {
        $this->languageFacade = $languageFacade;
    }

    /**
     * @param ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
     * 
     * @return ContentFieldDataResponseTransfer
     * @throws InvalidArgumentException
     * @throws BindingResolutionException
     */
    public function getContentFieldData(
        ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
    ): ContentFieldDataResponseTransfer {
        $contentFieldTable = ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $contentFieldDataRequestTransfer->getLangKey();

        $contentFieldData =  DB::table($contentFieldTable)
            ->join('template_field', 'template_field.id', '=', $contentFieldTable . '.template_field_id')
            ->where([
                $contentFieldTable . '.id' => $contentFieldDataRequestTransfer->getContentFieldId()
            ])
            ->select([
                'template_field' . '.field_name',
                $contentFieldTable . '.content',
                $contentFieldTable . '.id',
                $contentFieldTable . '.page_id',
            ])
            ->first();

        $contentFieldDataResponseTransfer = new ContentFieldDataResponseTransfer();

        if ($contentFieldData === null) {
            $contentFieldDataResponseTransfer->setSuccess(false);
            return $contentFieldDataResponseTransfer;
        }

        if ($contentFieldData->content === null) {
            $contentData = __('hybridcms::pages.content.field.placeholder');
        } else {
            $contentData = $contentFieldData->content;
        }

        $contentFieldDataResponseTransfer->setContentData($contentData);
        $contentFieldDataResponseTransfer->setFieldName($contentFieldData->field_name);
        $contentFieldDataResponseTransfer->setFieldId($contentFieldData->id);
        $contentFieldDataResponseTransfer->setPageId($contentFieldData->page_id);

        return $contentFieldDataResponseTransfer;
    }
}
