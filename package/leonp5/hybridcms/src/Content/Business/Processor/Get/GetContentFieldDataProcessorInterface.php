<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Get;

use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataResponseTransfer;

interface GetContentFieldDataProcessorInterface
{
    /**
     * @param ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
     * 
     * @return ContentFieldDataResponseTransfer
     */
    public function getContentFieldData(
        ContentFieldDataRequestTransfer $contentFieldDataRequestTransfer
    ): ContentFieldDataResponseTransfer;
}
