<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Update;

use Leonp5\Hybridcms\Content\Transfer\ContentUpdateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateResponseTransfer;

interface ContentUpdateProcessorInterface
{
    /**
     * @param ContentUpdateRequestTransfer $contentUpdateRequestTransfer
     * 
     * @return ContentUpdateResponseTransfer
     */
    public function updateContent(
        ContentUpdateRequestTransfer $contentUpdateRequestTransfer
    ): ContentUpdateResponseTransfer;
}
