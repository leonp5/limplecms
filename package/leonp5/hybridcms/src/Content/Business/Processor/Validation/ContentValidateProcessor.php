<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Validation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateResponseTransfer;

class ContentValidateProcessor implements ContentValidateProcessorInterface
{
    /**
     * @var LanguageFacadeInterface
     */
    private LanguageFacadeInterface $languageFacade;

    /**
     * @param LanguageFacadeInterface $languageFacade
     */
    public function __construct(LanguageFacadeInterface $languageFacade)
    {
        $this->languageFacade = $languageFacade;
    }

    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     * @throws BindingResolutionException
     */
    public function validateTitle(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer,
    ): ContentValidateResponseTransfer {
        $existAtLeastOneTitle = false;

        foreach ($validateContentRequestTransfer->getGeneralContent() as $content) {
            $title = $content['title'];
            if (null !== $title) {
                $existAtLeastOneTitle = true;
            }
        }

        if (false === $existAtLeastOneTitle) {
            $validateContentResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::admin.page.missing.title.min');
            $validateContentResponseTransfer->setMessage($msg);

            return $validateContentResponseTransfer;
        }

        return $validateContentResponseTransfer;
    }

    /**
     * @param Request $request
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer 
     */
    public function validateUrl(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer {
        $pageId = $validateContentRequestTransfer->getPageId();
        $atLeastOneValidUrl = false;

        $urlsInsidePageDiffer = $this->validateDifferenceOfUrlsInsideThePage(
            $validateContentResponseTransfer,
            $validateContentRequestTransfer
        );

        if (true === $urlsInsidePageDiffer) {

            foreach ($validateContentRequestTransfer->getGeneralContent() as $content) {
                $url = $content['url'];

                if (null !== $url) {
                    $validatedUrl = $this->checkForSlashesAndWhiteSpaces($url);

                    if (false === $validatedUrl) {
                        $validateContentResponseTransfer->setSuccess(false);
                        $msg = __('hybridcms::admin.invalid.url');
                        $validateContentResponseTransfer->setMessage($msg);

                        return $validateContentResponseTransfer;
                    }

                    $activatedLanguages = $this->languageFacade->getActivatedLanguages();

                    // check if URL is unique by looking in every content table
                    foreach ($activatedLanguages as $activeLanguage) {
                        $languageCode = $activeLanguage->alpha_2;

                        $contentTable = $this->createContentTableString($languageCode);

                        if ($pageId === null) {
                            if (DB::table($contentTable)->where($contentTable . '.url', '=', $url)->exists()) {
                                $validateContentResponseTransfer->setSuccess(false);
                                $msg = trans('hybridcms::admin.url.already.exists', ['language' => $languageCode]);
                                $validateContentResponseTransfer->setMessage($msg);

                                return $validateContentResponseTransfer;
                            }
                        } else {
                            $pageWithUrl = DB::table($contentTable)->where($contentTable . '.url', '=', $url)->first();

                            // if the url exists and it is not the one in the page which will be updated throw the error
                            if (null !== $pageWithUrl && $pageWithUrl->page_id !== $pageId) {
                                $validateContentResponseTransfer->setSuccess(false);
                                $msg = trans('hybridcms::admin.url.already.exists', ['language' => $languageCode]);
                                $validateContentResponseTransfer->setMessage($msg);

                                return $validateContentResponseTransfer;
                            }
                        }
                    }

                    $atLeastOneValidUrl = true;
                }
            }

            if (false === $atLeastOneValidUrl) {
                $validateContentResponseTransfer->setSuccess(false);
                $msg = __('hybridcms::admin.min.one.url');
                $validateContentResponseTransfer->setMessage($msg);

                return $validateContentResponseTransfer;
            }
        }

        return $validateContentResponseTransfer;
    }

    /**
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * 
     * @return bool
     * @throws BindingResolutionException
     */
    private function validateDifferenceOfUrlsInsideThePage(
        ContentValidateResponseTransfer $validateContentResponseTransfer,
        ContentValidateRequestTransfer $validateContentRequestTransfer,
    ): bool {
        $urls = [];

        foreach ($validateContentRequestTransfer->getGeneralContent() as $content) {
            $url = $content['url'];

            if (null !== $url) {

                if ($url === '/') {
                    break;
                }

                if ($url === '/404') {
                    break;
                }

                $urls[] = $url;
            }
        }

        $unique = array_unique($urls);
        $duplicates = array_diff_key($urls, $unique);
        if ([] !== $duplicates) {
            $validateContentResponseTransfer->setSuccess(false);
            $msg = trans('hybridcms::admin.page.need.different.urls');
            $validateContentResponseTransfer->setMessage($msg);

            return false;
        }

        return true;
    }

    /**
     * @param string $url
     * 
     * @return bool
     *
     * @throws BindingResolutionException
     */
    private function checkForSlashesAndWhiteSpaces(string $url): bool
    {
        $currentUrl = url()->current();
        $urlToCheck = $currentUrl . $url;

        $urlStartsWithSlash = '/' === substr($url, 0, strlen('/'));
        $urlHasOnlyOneSlash = '/' === substr($url, 1, strlen('/'));
        $urlHasWhiteSpace = preg_match('/\s/', $url);
        $validUrl = parse_url($urlToCheck, PHP_URL_SCHEME) && parse_url($urlToCheck, PHP_URL_HOST);

        if (
            true === $urlStartsWithSlash
            && false === $urlHasOnlyOneSlash
            && 0 === $urlHasWhiteSpace
            && true === $validUrl
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param string $language
     * 
     * @return string
     */
    private function createContentTableString(string $language): string
    {
        return ContentTableInterface::CONTENT_TABLE_PREFIX . $language;
    }
}
