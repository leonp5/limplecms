<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Business\Processor\Validation;

use Leonp5\Hybridcms\Content\Transfer\ContentValidateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateResponseTransfer;

interface ContentValidateProcessorInterface
{
    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     */
    public function validateTitle(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer;

    /**
     * @param ContentValidateRequestTransfer $validateContentRequestTransfer
     * @param ContentValidateResponseTransfer $validateContentResponseTransfer
     * 
     * @return ContentValidateResponseTransfer
     */
    public function validateUrl(
        ContentValidateRequestTransfer $validateContentRequestTransfer,
        ContentValidateResponseTransfer $validateContentResponseTransfer
    ): ContentValidateResponseTransfer;
}
