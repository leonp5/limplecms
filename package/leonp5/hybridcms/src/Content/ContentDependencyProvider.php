<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Content\Business\ContentFacade;
use Leonp5\Hybridcms\Content\Business\ContentFacadeInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Content\Business\ContentFacadeBusinessFactory;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Add\ContentAddProcessor;
use Leonp5\Hybridcms\Content\Business\Processor\Update\ContentUpdateProcessor;
use Leonp5\Hybridcms\Content\Business\Processor\Add\ContentAddProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Get\GetContentFieldDataProcessor;
use Leonp5\Hybridcms\Content\Business\Processor\Validation\ContentValidateProcessor;
use Leonp5\Hybridcms\Content\Business\Processor\Update\ContentUpdateProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Get\GetContentFieldDataProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\Validation\ContentValidateProcessorInterface;
use Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate\ContentTableCreateProcessor;
use Leonp5\Hybridcms\Content\Business\Processor\ContentTableCreate\ContentTableCreateProcessorInterface;

final class ContentDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @param Application $app
     * 
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(ContentFacadeBusinessFactory::class);

        $app->bind(ContentFacadeInterface::class, function ($app) {
            return new ContentFacade(
                $app->make(ContentFacadeBusinessFactory::class)
            );
        });

        $app->bind(
            ContentTableCreateProcessorInterface::class,
            ContentTableCreateProcessor::class
        );

        $app->bind(
            ContentAddProcessorInterface::class,
            ContentAddProcessor::class
        );

        $app->bind(
            ContentUpdateProcessorInterface::class,
            ContentUpdateProcessor::class
        );

        $app->bind(
            GetContentFieldDataProcessorInterface::class,
            GetContentFieldDataProcessor::class
        );

        $app->bind(
            ContentValidateProcessorInterface::class,
            function ($app) {
                return new ContentValidateProcessor(
                    $app->make(LanguageFacadeInterface::class)
                );
            }
        );
    }
}
