<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Transfer;

class ContentAddRequestTransfer
{
    /**
     * @var mixed[]
     */
    private array $generalContent;

    /**
     * @var null|mixed[]
     */
    private ?array $pageContent = null;

    /**
     * @var int
     */
    private int $templateId;

    /**
     * @var int
     */
    private int $pageStatusId;

    /**
     * @return mixed[]
     */
    public function getGeneralContent(): array
    {
        return $this->generalContent;
    }

    /**
     * @param mixed[] $generalContent
     *
     * @return self
     */
    public function setGeneralContent(array $generalContent): self
    {
        $this->generalContent = $generalContent;

        return $this;
    }

    /**
     * @return @var null|mixed[]
     */
    public function getPageContent(): ?array
    {
        return $this->pageContent;
    }

    /**
     * @param mixed[] $pageContent
     *
     * @return self
     */
    public function setPageContent(array $pageContent): self
    {
        $this->pageContent = $pageContent;

        return $this;
    }

    /**
     * @return int
     */
    public function getTemplateId(): int
    {
        return $this->templateId;
    }

    /**
     * @param int $templateId
     *
     * @return self
     */
    public function setTemplateId(int $templateId): self
    {
        $this->templateId = $templateId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPageStatusId(): int
    {
        return $this->pageStatusId;
    }

    /**
     * @param int $pageStatusId
     *
     * @return self
     */
    public function setPageStatusId(int $pageStatusId): self
    {
        $this->pageStatusId = $pageStatusId;

        return $this;
    }
}
