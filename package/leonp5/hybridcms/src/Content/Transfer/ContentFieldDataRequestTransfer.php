<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Content\Transfer;

class ContentFieldDataRequestTransfer
{
    /**
     * @var int
     */
    private int $contentFieldId;

    /**
     * @var string
     */
    private string $langKey;



    /**
     * @return string
     */
    public function getLangKey(): string
    {
        return $this->langKey;
    }

    /**
     * @param string $langKey 
     *
     * @return self
     */
    public function setLangKey(string $langKey): self
    {
        $this->langKey = $langKey;

        return $this;
    }

    /**
     * @return int
     */
    public function getContentFieldId(): int
    {
        return $this->contentFieldId;
    }

    /**
     * @param int $contentFieldId 
     *
     * @return self
     */
    public function setContentFieldId(int $contentFieldId): self
    {
        $this->contentFieldId = $contentFieldId;

        return $this;
    }
}
