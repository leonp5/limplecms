<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Business;

use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentRequestTransfer;
use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentResponseTransfer;

class ContentEditorFacade implements ContentEditorFacadeInterface
{
    /**
     * @var ContentEditorFacadeBusinessFactory
     */
    private ContentEditorFacadeBusinessFactory $factory;

    public function __construct(ContentEditorFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param UpdateFieldContentRequestTransfer $updateFieldContentRequestTransfer
     * 
     * @return UpdateFieldContentResponseTransfer
     * @throws BindingResolutionException
     */
    public function updateFieldContent(
        UpdateFieldContentRequestTransfer $updateFieldContentRequestTransfer
    ): UpdateFieldContentResponseTransfer {
        return $this->factory
            ->getContentEditorWriteRepository()
            ->updateFieldContent($updateFieldContentRequestTransfer);
    }
}
