<?php

namespace Leonp5\Hybridcms\ContentEditor\Business\Validation;

use Illuminate\Foundation\Http\FormRequest;

class StoreContentEditorFieldRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [

            'field_content' => ['required', 'bail', 'string'],
            'language' => ['required', 'bail', 'string'],
            'field_id' => ['required', 'bail', 'string'],
        ];
    }

    /**
     * @return string[]
     */
    public function messages(): array
    {
        return [
            'field_content.required' => __('hybridcms::pages.content.field.editor.field.required'),
            'language.required'  => __('hybridcms::pages.content.field.editor.missing.language'),
            'field_id.required' => __('hybridcms::pages.content.field.editor.missing.field-id')
        ];
    }
}
