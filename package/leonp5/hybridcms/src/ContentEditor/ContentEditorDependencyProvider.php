<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\ContentEditor\Business\ContentEditorFacade;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\ContentEditor\Business\ContentEditorFacadeInterface;
use Leonp5\Hybridcms\ContentEditor\Persistence\ContentEditorWriteRepository;
use Leonp5\Hybridcms\ContentEditor\Business\ContentEditorFacadeBusinessFactory;
use Leonp5\Hybridcms\ContentEditor\Persistence\ContentEditorWriteRepositoryInterface;

final class ContentEditorDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(ContentEditorFacadeBusinessFactory::class);

        $app->bind(ContentEditorFacadeInterface::class, function ($app) {
            return new ContentEditorFacade(
                $app->make(ContentEditorFacadeBusinessFactory::class)
            );
        });

        $app->bind(
            ContentEditorWriteRepositoryInterface::class,
            ContentEditorWriteRepository::class
        );
    }
}
