<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Persistence;

use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Utility\Traits\BoolIntCasterTrait;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentRequestTransfer;
use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentResponseTransfer;

final class ContentEditorWriteRepository implements ContentEditorWriteRepositoryInterface
{
    use BoolIntCasterTrait;

    /**
     * @var DB
     */
    private DB $db;

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    /**
     * @param UpdateFieldContentRequestTransfer $updateFieldContentRequestTransfer
     * 
     * @return UpdateFieldContentResponseTransfer
     */
    public function updateFieldContent(
        UpdateFieldContentRequestTransfer $updateFieldContentRequestTransfer
    ): UpdateFieldContentResponseTransfer {
        $updateFieldContentResponse = new UpdateFieldContentResponseTransfer();

        $contentFieldTable =
            ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $updateFieldContentRequestTransfer->getLangKey();

        $success = $this->db::table($contentFieldTable)
            ->where($contentFieldTable . '.id', '=', $updateFieldContentRequestTransfer->getFieldId())
            ->update(['content' => $updateFieldContentRequestTransfer->getFieldContent()]);

        $updateFieldContentResponse->setSuccess($this->toBool($success));

        if ($updateFieldContentResponse->hasSuccess() === false) {
            $updateFieldContentResponse->setMessage(__('hybridcms::pages.content.field.editor.update-error'));
        }

        return $updateFieldContentResponse;
    }
}
