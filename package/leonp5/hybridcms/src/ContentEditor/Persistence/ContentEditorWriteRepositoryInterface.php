<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Persistence;

use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentRequestTransfer;
use Leonp5\Hybridcms\ContentEditor\Transfer\UpdateFieldContentResponseTransfer;

interface ContentEditorWriteRepositoryInterface
{
    /**
     * @param UpdateFieldContentRequestTransfer $updateFieldContentRequestTransfer
     *
     * @return UpdateFieldContentResponseTransfer
     */
    public function updateFieldContent(
        UpdateFieldContentRequestTransfer $updateFieldContentRequestTransfer
    ): UpdateFieldContentResponseTransfer;
}
