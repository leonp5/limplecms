<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Transfer;

class UpdateFieldContentRequestTransfer
{
    /**
     * @var string
     */
    private string $langKey;

    /**
     * @var string
     */
    private string $fieldContent;

    /**
     * @var int
     */
    private int $fieldId;

    /**
     * @return string
     */
    public function getLangKey(): string
    {
        return $this->langKey;
    }

    /**
     * @param string $langKey 
     *
     * @return self
     */
    public function setLangKey(string $langKey): self
    {
        $this->langKey = $langKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldContent(): string
    {
        return $this->fieldContent;
    }

    /**
     * @param string $fieldContent 
     *
     * @return self
     */
    public function setFieldContent(string $fieldContent): self
    {
        $this->fieldContent = $fieldContent;

        return $this;
    }

    /**
     * @return int
     */
    public function getFieldId(): int
    {
        return $this->fieldId;
    }

    /**
     * @param int $fieldId 
     *
     * @return self
     */
    public function setFieldId(int $fieldId): self
    {
        $this->fieldId = $fieldId;

        return $this;
    }
}
