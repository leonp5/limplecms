<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentEditor\Transfer;

class UpdateFieldContentResponseTransfer
{
    /**
     * @var bool
     */
    private bool $success = true;

    /**
     * @var string
     */
    private string $message;

    public function __construct()
    {
        $this->message = __('hybridcms::pages.content.field.editor.update-success');
    }

    /**
     * @return bool
     */
    public function hasSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success 
     *
     * @return self
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return self
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
