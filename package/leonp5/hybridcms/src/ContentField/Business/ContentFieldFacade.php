<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business;

use Illuminate\Contracts\Container\BindingResolutionException;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateRequestTransfer;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateResponseTransfer;

final class ContentFieldFacade implements ContentFieldFacadeInterface
{
    /**
     * @var ContentFacadeBusinessFactory
     */
    private ContentFieldFacadeBusinessFactory $factory;

    /**
     * @param ContentFieldFacadeBusinessFactory $factory
     * 
     * @return void
     */
    public function __construct(ContentFieldFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param string $tableName
     * 
     * @return void
     * @throws BindingResolutionException 
     */
    public function createContentFieldTable(string $tableName): void
    {
        $this->factory
            ->getContentFieldTableCreateProcessor()
            ->createTable($tableName);
    }

    /**
     * @param ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
     * 
     * @return ContentFieldUpdateResponseTransfer
     * @throws BindingResolutionException
     */
    public function updateContentField(
        ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
    ): ContentFieldUpdateResponseTransfer {
        return $this->factory
            ->getContentFieldUpdateProcessor()
            ->updateContentField($contentFieldUpdateRequestTransfer);
    }
}
