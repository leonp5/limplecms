<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

final class ContentFieldTableCreateProcessor implements ContentFieldTableCreateProcessorInterface
{
    /**
     * @param string $tableName
     * 
     * @return void
     */
    public static function createTable(string $tableName): void
    {
        Schema::create($tableName, function (Blueprint $table) {
            $table->id();
            $table->bigInteger('page_id');
            $table->bigInteger('template_field_id');
            $table->text('content')->nullable();
            $table->integer('content_type_id');
            $table->timestamps();
        });
    }
}
