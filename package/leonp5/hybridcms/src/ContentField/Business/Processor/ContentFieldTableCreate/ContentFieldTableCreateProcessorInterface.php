<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate;

interface ContentFieldTableCreateProcessorInterface
{
    /**
     * @param string $tableName
     * 
     * @return void 
     */
    public static function createTable(string $tableName): void;
}
