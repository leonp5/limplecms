<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Business\Processor\Update;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateRequestTransfer;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateResponseTransfer;

final class ContentFieldUpdateProcessor implements ContentFieldUpdateProcessorInterface
{
    /**
     * @param ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer
     * 
     * @return ContentFieldUpdateResponseTransfer 
     */
    public function updateContentField(
        ContentFieldUpdateRequestTransfer $contentFieldUpdateRequestTransfer,
    ): ContentFieldUpdateResponseTransfer {

        $contentFieldUpdateResponseTransfer = new ContentFieldUpdateResponseTransfer();

        $langKey = $contentFieldUpdateRequestTransfer->getLangKey();

        $updateResponse = DB::table(ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langKey)
            ->where(
                [
                    'page_id' => $contentFieldUpdateRequestTransfer->getPageId(),
                    'id' => $contentFieldUpdateRequestTransfer->getFieldId()
                ]
            )
            ->update(
                [
                    'content' => $contentFieldUpdateRequestTransfer->getContent(),
                    'updated_at' => Carbon::now()
                ]
            );

        if ($updateResponse !== 1) {
            $contentFieldUpdateResponseTransfer->setSuccess(false);
            $contentFieldUpdateResponseTransfer->setMessage(
                __('hybridcms::pages.content.field.update.error')
            );
            return $contentFieldUpdateResponseTransfer;
        }
        $contentFieldUpdateResponseTransfer->setMessage(
            __('hybridcms::pages.content.field.update.success', ['fieldName' =>
            $contentFieldUpdateRequestTransfer->getFieldName()])
        );

        return $contentFieldUpdateResponseTransfer;
    }
}
