<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\ContentField\Business\ContentFieldFacade;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\ContentField\Business\ContentFieldFacadeInterface;
use Leonp5\Hybridcms\ContentField\Business\ContentFieldFacadeBusinessFactory;
use Leonp5\Hybridcms\ContentField\Business\Processor\Update\ContentFieldUpdateProcessor;
use Leonp5\Hybridcms\ContentField\Business\Processor\Update\ContentFieldUpdateProcessorInterface;
use Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate\ContentFieldTableCreateProcessor;
use Leonp5\Hybridcms\ContentField\Business\Processor\ContentFieldTableCreate\ContentFieldTableCreateProcessorInterface;

final class ContentFieldDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @param Application $app
     * 
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(ContentFieldFacadeBusinessFactory::class);

        $app->bind(ContentFieldFacadeInterface::class, function ($app) {
            return new ContentFieldFacade(
                $app->make(ContentFieldFacadeBusinessFactory::class)
            );
        });

        $app->bind(
            ContentFieldTableCreateProcessorInterface::class,
            ContentFieldTableCreateProcessor::class
        );

        $app->bind(
            ContentFieldUpdateProcessorInterface::class,
            ContentFieldUpdateProcessor::class
        );
    }
}
