<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Controller;

use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Models\Page;
use Leonp5\Hybridcms\App\Models\TemplateField;
use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\Utility\Traits\LocaleChangerTrait;
use Leonp5\Hybridcms\Utility\Business\UtilityFacadeInterface;
use Leonp5\Hybridcms\Content\Business\ContentFacadeInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentFieldDataRequestTransfer;
use Leonp5\Hybridcms\ContentField\Business\ContentFieldFacadeInterface;
use Leonp5\Hybridcms\ContentField\Transfer\ContentFieldUpdateRequestTransfer;

class ContentFieldController extends Controller
{

    use LocaleChangerTrait;

    /**
     * @param Page $page
     * @param string $contentField
     * @param int $fieldId
     * @param string $langKey
     * @param UtilityFacadeInterface $contentFacade
     * 
     * @return JsonResponse
     * @throws BindingResolutionException
     */
    public function edit(
        Page $page,
        string $contentField,
        int $fieldId,
        string $langKey,
        ContentFacadeInterface $contentFacade,
    ): JsonResponse {
        $contentFielDataRequest = (new ContentFieldDataRequestTransfer())
            ->setContentFieldId($fieldId)
            ->setLangKey($langKey);

        $content_type_id = TemplateField::where([
            'template_id' => $page->template_id,
            'field_name' => $contentField
        ])->first(['content_type_id'])->content_type_id;

        if ($content_type_id === ContentTableInterface::CONTENT_TYPE_TEXT) {

            $contentFieldDataResponseTransfer = $contentFacade->getContentFieldData(
                $contentFielDataRequest
            );

            if ($contentFieldDataResponseTransfer->isSuccessful() === false) {
                return response()->json(['error' => __('hybridcms::pages.content.field.get.error')], 500);
            }

            return response()->json([
                'data' => [
                    'page_id' => $page->id,
                    'field_name' => $contentFieldDataResponseTransfer->getFieldName(),
                    'content' => $contentFieldDataResponseTransfer->getContentData(),
                    'field_id' => $contentFieldDataResponseTransfer->getFieldId()
                ]
            ]);
        }

        if ($content_type_id === ContentTableInterface::CONTENT_TYPE_EDITOR) {

            $url = route('field.edit.editor', [
                'contentFieldId' => $fieldId,
                'langKey' => $contentFielDataRequest->getLangKey()
            ]);

            if (config('app.env') === 'local') {
                $baseUrl = url('/');
                $devBaseUrl = config('app.url');
                $url = str_replace($baseUrl, $devBaseUrl, $url);
            }

            return response()->json([
                'redirect' => ['url' => $url]
            ]);
        }

        return response()->json(['error' => __('hybridcms::pages.content.field.get.error')], 500);
    }

    /**
     * @param Request $request
     * @param ContentFieldFacadeInterface $contentFieldFacade
     * 
     * @return JsonResponse
     * @throws InvalidArgumentException
     * @throws BindingResolutionException
     */
    public function update(
        Request $request,
        ContentFieldFacadeInterface $contentFieldFacade
    ): JsonResponse {
        $request->all();
        $contentFieldUpdateRequestTransfer = new ContentFieldUpdateRequestTransfer(
            $request->input('content'),
            (int) $request->input('fieldId'),
            (int) $request->input('pageId'),
            $request->input('fieldName'),
            $request->input('langKey'),
        );

        $contentFieldUpdateResponseTransfer = $contentFieldFacade
            ->updateContentField($contentFieldUpdateRequestTransfer);

        if ($contentFieldUpdateResponseTransfer->isSuccessful() === false) {
            return response()->json(['error' => $contentFieldUpdateResponseTransfer->getMessage()], 500);
        }

        return response()->json(['success' => $contentFieldUpdateResponseTransfer->getMessage()], 200);
    }
}
