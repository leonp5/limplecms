<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Transfer;

class ContentFieldUpdateRequestTransfer
{
    /**
     * @var string
     */
    private string $content;

    /**
     * @var int
     */
    private int $fieldId;

    /**
     * @var string
     */
    private string $fieldName;

    /**
     * @var int
     */
    private int $pageId;

    /**
     * @var string
     */
    private string $langKey;

    /**
     * @param string $content
     * @param int $fieldId
     * @param int $pageId
     * @param string $langKey
     * 
     * @return void 
     */
    public function __construct(
        string $content,
        int $fieldId,
        int $pageId,
        string $fieldName,
        string $langKey
    ) {
        $this->content = $content;
        $this->fieldId = $fieldId;
        $this->pageId = $pageId;
        $this->fieldName = $fieldName;
        $this->langKey = $langKey;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return int
     */
    public function getPageId(): int
    {
        return $this->pageId;
    }

    /**
     * @return int
     */
    public function getFieldId(): int
    {
        return $this->fieldId;
    }

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @return string
     */
    public function getLangKey(): string
    {
        return $this->langKey;
    }
}
