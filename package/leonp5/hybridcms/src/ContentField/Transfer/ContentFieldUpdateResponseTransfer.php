<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\ContentField\Transfer;

class ContentFieldUpdateResponseTransfer
{
    /**
     * @var bool
     */
    private bool $success = true;

    /**
     * @var string
     */
    private string $message;

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success 
     *
     * @return ContentFieldUpdateResponseTransfer
     */
    public function setSuccess(bool $success): ContentFieldUpdateResponseTransfer
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message 
     *
     * @return ContentFieldUpdateResponseTransfer
     */
    public function setMessage(string $message): ContentFieldUpdateResponseTransfer
    {
        $this->message = $message;

        return $this;
    }
}
