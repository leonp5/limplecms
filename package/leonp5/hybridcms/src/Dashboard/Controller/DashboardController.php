<?php

namespace Leonp5\Hybridcms\Dashboard\Controller;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Controller\Controller;

class DashboardController extends Controller
{
    /**
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Renderable
     * @throws BindingResolutionException
     */
    public function dashboard(): Renderable
    {
        return view('hybridcms::backend.user.index');
    }
}
