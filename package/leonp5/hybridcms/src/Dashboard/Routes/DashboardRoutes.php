<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Dashboard\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\Dashboard\Controller\DashboardController;

class DashboardRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
    }
}
