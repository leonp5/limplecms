<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Business;

use Illuminate\Contracts\Container\BindingResolutionException;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleRequestTransfer;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleResponseTransfer;

class FrontendFacade implements FrontendFacadeInterface
{

    /**
     * @var FrontendFacadeBusinessFactory
     */
    private FrontendFacadeBusinessFactory $factory;

    public function __construct(FrontendFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param PageAssembleRequestTransfer $pageAssembleRequestTransfer
     * 
     * @return PageAssembleResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function assemblePage(
        PageAssembleRequestTransfer $pageAssembleRequestTransfer
    ): PageAssembleResponseTransfer {
        return $this->factory
            ->getPageAssembleProcessor()
            ->assemble($pageAssembleRequestTransfer);
    }
}
