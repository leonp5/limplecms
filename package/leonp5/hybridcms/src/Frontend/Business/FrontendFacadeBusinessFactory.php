<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Frontend\Business\Processor\PageAssemble\PageAssembleProcessorInterface;

final class FrontendFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return PageAssembleProcessorInterface 
     * @throws BindingResolutionException 
     */
    public function getPageAssembleProcessor(): PageAssembleProcessorInterface
    {
        return $this->app->make(PageAssembleProcessorInterface::class);
    }
}
