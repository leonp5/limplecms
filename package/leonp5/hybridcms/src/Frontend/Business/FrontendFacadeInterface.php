<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Business;

use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleRequestTransfer;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleResponseTransfer;

interface FrontendFacadeInterface
{
    /**
     * @param PageAssembleRequestTransfer $pageAssembleRequestTransfer
     * 
     * @return PageAssembleResponseTransfer
     */
    public function assemblePage(
        PageAssembleRequestTransfer $pageAssembleRequestTransfer
    ): PageAssembleResponseTransfer;
}
