<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Business\Processor\Localization;

use InvalidArgumentException;

use Leonp5\Hybridcms\App\Models\ActivatedLanguages;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Frontend\Transfer\LocalizationProcessorResponseTransfer;

use function trim;
use function preg_match;

class LocalizationProcessor implements LocalizationProcessorInterface
{
    /**
     * @var LanguageFacadeInterface
     */
    private LanguageFacadeInterface $languageFacade;

    public function __construct(LanguageFacadeInterface $languageFacade)
    {
        $this->languageFacade = $languageFacade;
    }

    /**
     * @param LocalizationProcessorResponseTransfer $localizationProcessorResponseTransfer
     * 
     * @return LocalizationProcessorResponseTransfer
     * @throws InvalidArgumentException
     */
    public function validate(
        LocalizationProcessorResponseTransfer $localizationProcessorResponseTransfer
    ): LocalizationProcessorResponseTransfer {

        $activatedLanguages = $this->languageFacade->getActivatedLanguages();

        foreach ($activatedLanguages as $language) {
            if (
                $language->alpha_2 === $localizationProcessorResponseTransfer->getLocale()
            ) {
                return $localizationProcessorResponseTransfer;
            }
        }

        $localizationProcessorResponseTransfer->setLocale(null);

        return $localizationProcessorResponseTransfer;
    }

    /**
     * @param string|null $url 
     * 
     * @return LocalizationProcessorResponseTransfer 
     */
    public function hasLocale(?string $url): LocalizationProcessorResponseTransfer
    {
        $localizationProcessorResponseTransfer = new LocalizationProcessorResponseTransfer();

        if ($url === null) {
            $url = '/';
            $localizationProcessorResponseTransfer->setHasLocale(true);
            if (session()->has('locale') === false) {
                session()->put('locale', $this->languageFacade->getMainLanguage()->alpha_2);
            }
            $localizationProcessorResponseTransfer->setLocale(session()->get('locale'));
            $localizationProcessorResponseTransfer->setUrl($url);

            return $localizationProcessorResponseTransfer;
        }

        $regEx = '`^([a-zA-Z]{2})[/]`';
        preg_match($regEx, $url, $matches);
        if (!empty($matches)) {
            $localizationProcessorResponseTransfer->setHasLocale(true);

            if (trim($matches[0], '/') === 'en') {
                $localizationProcessorResponseTransfer->setLocale('gb');
            } else {
                $localizationProcessorResponseTransfer->setLocale(trim($matches[0], '/'));
            }
            $localizationProcessorResponseTransfer->setUrl(
                '/' .  substr($url, strpos($url, "/") + 1)
            );
        }
        return $localizationProcessorResponseTransfer;
    }
}
