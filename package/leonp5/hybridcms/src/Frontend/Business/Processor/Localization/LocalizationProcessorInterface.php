<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Business\Processor\Localization;

use Leonp5\Hybridcms\Frontend\Transfer\LocalizationProcessorResponseTransfer;

interface LocalizationProcessorInterface
{
    /**
     * @param LocalizationProcessorResponseTransfer $localizationProcessorResponseTransfer
     * 
     * @return LocalizationProcessorResponseTransfer 
     */
    public function validate(
        LocalizationProcessorResponseTransfer $localizationProcessorResponseTransfer
    ): LocalizationProcessorResponseTransfer;

    /**
     * @param string|null $url
     * 
     * @return LocalizationProcessorResponseTransfer 
     */
    public function hasLocale(?string $url): LocalizationProcessorResponseTransfer;
}
