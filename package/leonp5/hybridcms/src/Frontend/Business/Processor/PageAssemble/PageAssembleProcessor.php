<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Business\Processor\PageAssemble;

use Leonp5\Hybridcms\Template\Business\Template\TemplateInterface;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleRequestTransfer;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleResponseTransfer;

class PageAssembleProcessor implements PageAssembleProcessorInterface
{
    /**
     * @param PageAssembleRequestTransfer $pageAssembleRequestTransfer
     * 
     * @return PageAssembleResponseTransfer
     */
    public function assemble(
        PageAssembleRequestTransfer $pageAssembleRequestTransfer
    ): PageAssembleResponseTransfer {

        $pageAssembleResponseTransfer = new PageAssembleResponseTransfer();

        if ($pageAssembleRequestTransfer->redirectNecessary() === true) {
            $pageAssembleResponseTransfer->setPage(
                redirect(
                    $pageAssembleRequestTransfer->getUrl()
                )
            );
            return $pageAssembleResponseTransfer;
        };

        $pageAssembleResponseTransfer->setPage(
            response()
                ->view(
                    TemplateInterface::VIEW_PREFIX . $pageAssembleRequestTransfer->getViewString(),
                    $pageAssembleRequestTransfer->getPageDataTransfer()->getPageData(),
                    $pageAssembleRequestTransfer->getStatusCode()
                )
        );

        return $pageAssembleResponseTransfer;
    }
}
