<?php

namespace Leonp5\Hybridcms\Frontend\Business\Processor\PageAssemble;

use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleRequestTransfer;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleResponseTransfer;

interface PageAssembleProcessorInterface
{
    /**
     * @param PageAssembleRequestTransfer $pageAssembleRequestTransfer
     * 
     * @return PageAssembleResponseTransfer
     */
    public function assemble(
        PageAssembleRequestTransfer $pageAssembleRequestTransfer
    ): PageAssembleResponseTransfer;
}
