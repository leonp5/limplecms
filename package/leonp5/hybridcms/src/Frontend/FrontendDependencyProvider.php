<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Frontend\Business\FrontendFacade;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Frontend\Business\FrontendFacadeInterface;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\Frontend\Business\FrontendFacadeBusinessFactory;
use Leonp5\Hybridcms\Frontend\Business\Processor\Localization\LocalizationProcessor;
use Leonp5\Hybridcms\Frontend\Business\Processor\PageAssemble\PageAssembleProcessor;
use Leonp5\Hybridcms\Frontend\Business\Processor\PageAssemble\PageAssembleProcessorInterface;
use Leonp5\Hybridcms\Frontend\Business\Processor\Localization\LocalizationProcessorInterface;

final class FrontendDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(FrontendFacadeBusinessFactory::class);

        $app->bind(FrontendFacadeInterface::class, function ($app) {
            return new FrontendFacade(
                $app->make(FrontendFacadeBusinessFactory::class)
            );
        });

        $app->bind(
            LocalizationProcessorInterface::class,
            function ($app) {
                return new LocalizationProcessor(
                    $app->make(LanguageFacadeInterface::class)
                );
            }

        );
        $app->bind(
            PageAssembleProcessorInterface::class,
            PageAssembleProcessor::class
        );
    }
}
