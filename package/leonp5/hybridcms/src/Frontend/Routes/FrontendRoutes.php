<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Frontend\Routes;

use Illuminate\Support\Facades\Route;
use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\Frontend\Controller\FrontendController;

class FrontendRoutes implements HcmsRouteInterface
{
    /**
     * @var string
     */
    private string $cmsPrefix;

    /**
     * @param string $cmsPrefix
     * 
     * @return void 
     */
    public function __construct(string $cmsPrefix)
    {
        $this->cmsPrefix = $cmsPrefix;
    }

    /**
     * @return void 
     */
    public function register(): void
    {
        /**
         * The regex in the where clause excludes ONLY the 'cms' prefix string.
         * 
         *  */

        Route::any('{url}', [FrontendController::class, 'handleRequest'])
            ->name('frontend')
            ->where('url', "^((?!(?<!\w)$this->cmsPrefix(?!\w)).)*?");
    }
}
