<?php

namespace Leonp5\Hybridcms\Frontend\Transfer;

use Illuminate\Http\Request;

class FrontendRequestTransfer
{

    /**
     * @var string
     */
    private string $url;

    /**
     * @var string|null
     */
    private ?string $locale = null;

    /**
     * @var int
     */
    private int $pageStatus;

    /**
     * @var Request
     */
    private Request $request;

    /**
     * @param Request $request
     * 
     * @return void 
     */
    public function __construct(
        Request $request,
    ) {
        $this->request = $request;
    }

    /**
     * @param string|null $locale 
     * 
     * @return FrontendRequestTransfer 
     */
    public function setLocale(?string $locale): FrontendRequestTransfer
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $url
     * 
     * @return FrontendRequestTransfer
     */
    public function setUrl(string $url): FrontendRequestTransfer
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return  string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return  Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return int
     */
    public function getPageStatus(): int
    {
        return $this->pageStatus;
    }

    /**
     * @param int $pageStatus 
     *
     * @return self
     */
    public function setPageStatus(int $pageStatus): self
    {
        $this->pageStatus = $pageStatus;

        return $this;
    }
}
