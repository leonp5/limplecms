<?php

namespace Leonp5\Hybridcms\Frontend\Transfer;

class LocalizationProcessorResponseTransfer
{

    /**
     * @var bool
     */
    private bool $success = true;

    /**
     * @var bool
     */
    private bool $hasLocale = false;

    /**
     * @var string|null
     */
    private ?string $locale;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var string
     */
    private string $message;

    /**
     * @return  bool
     */
    public function getSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param  bool  $success
     *
     * @return  LocalizationProcessorResponseTransfer
     */
    public function setSuccess(bool $success): LocalizationProcessorResponseTransfer
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return  string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param  string  $message
     *
     * @return  LocalizationProcessorResponseTransfer
     */
    public function setMessage(string $message): LocalizationProcessorResponseTransfer
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return  bool
     */
    public function hasLocale(): bool
    {
        return $this->hasLocale;
    }

    /**
     * @param  bool  $hasLocale
     *
     * @return  LocalizationProcessorResponseTransfer
     */
    public function setHasLocale(bool $hasLocale): LocalizationProcessorResponseTransfer
    {
        $this->hasLocale = $hasLocale;

        return $this;
    }

    /**
     * @return  string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param  string|null  $locale
     *
     * @return  LocalizationProcessorResponseTransfer
     */
    public function setLocale(?string $locale): LocalizationProcessorResponseTransfer
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * 
     * @return LocalizationProcessorResponseTransfer
     */
    public function setUrl(string $url): LocalizationProcessorResponseTransfer
    {
        $this->url = $url;

        return $this;
    }
}
