<?php

namespace Leonp5\Hybridcms\Frontend\Transfer;

use Leonp5\Hybridcms\Utility\Transfer\PageDataTransfer;

class PageAssembleRequestTransfer
{
    /**
     * @var bool
     */
    private bool $redirectNecessary = false;

    /**
     * @var PageDataTransfer
     */
    private PageDataTransfer $pageDataTransfer;

    /**
     * @var string
     */
    private string $viewString;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var int
     */
    private int $statusCode = 200;

    /**
     * @return  bool
     */
    public function redirectNecessary(): bool
    {
        return $this->redirectNecessary;
    }

    /**
     * @param  bool  $redirectNecessary
     *
     * @return  PageAssembleRequestTransfer
     */
    public function setRedirectNecessary(bool $redirectNecessary): PageAssembleRequestTransfer
    {
        $this->redirectNecessary = $redirectNecessary;

        return $this;
    }

    /**
     * @return string
     */
    public function getViewString(): string
    {
        return $this->viewString;
    }

    /**
     * @param string $viewString 
     *
     * @return PageAssembleRequestTransfer
     */
    public function setViewString(string $viewString): PageAssembleRequestTransfer
    {
        $this->viewString = $viewString;

        return $this;
    }

    /**
     * @return PageDataTransfer
     */
    public function getPageDataTransfer(): PageDataTransfer
    {
        return $this->pageDataTransfer;
    }

    /**
     * @param PageDataTransfer $pageDataTransfer
     *
     * @return PageAssembleRequestTransfer
     */
    public function setPageDataTransfer(PageDataTransfer $pageDataTransfer): PageAssembleRequestTransfer
    {
        $this->pageDataTransfer = $pageDataTransfer;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url 
     *
     * @return PageAssembleRequestTransfer
     */
    public function setUrl(string $url): PageAssembleRequestTransfer
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode 
     *
     * @return PageAssembleRequestTransfer
     */
    public function setStatusCode(int $statusCode): PageAssembleRequestTransfer
    {
        $this->statusCode = $statusCode;

        return $this;
    }
}
