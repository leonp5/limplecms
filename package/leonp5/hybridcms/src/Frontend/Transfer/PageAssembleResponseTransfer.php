<?php

namespace Leonp5\Hybridcms\Frontend\Transfer;

class PageAssembleResponseTransfer
{
    /**
     * @var mixed
     */
    private mixed $page;

    /**
     * @return  mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param  mixed  $page
     *
     * @return  PageAssembleResponseTransfer
     */
    public function setPage($page): PageAssembleResponseTransfer
    {
        $this->page = $page;

        return $this;
    }
}
