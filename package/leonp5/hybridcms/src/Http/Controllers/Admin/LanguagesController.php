<?php

namespace Leonp5\Hybridcms\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Leonp5\Hybridcms\Models\Languages;
use Leonp5\Hybridcms\Models\ActivatedLanguages;
use Leonp5\Hybridcms\Http\Controllers\Controller;
use Leonp5\Hybridcms\Http\Controllers\Admin\Transfer\ResponseTransfer;

class LanguagesController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (env('FRONTEND_MULTILANG') === true) {
            $activeLanguages = ActivatedLanguages::withDisabled();
            $allLanguages = Languages::findAll();
            // dd($activeLanguages);

            foreach ($activeLanguages as $activeLang) {
                $lang = $allLanguages->where('id', $activeLang->lang_id)->first();
                $lang->enabled = true;
            }

            // dd($allLanguages);

            return view(
                'hybridcms::backend.admin.languages.index',
                [
                    'activeLanguages' => $activeLanguages,
                    'allLanguages' => $allLanguages
                ]
            );
        }

        $activeLanguages = [];
        return view('hybridcms::backend.admin.languages.index', ['activeLanguages' => $activeLanguages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function destroy($id)
    // {
    //     //
    // }
}
