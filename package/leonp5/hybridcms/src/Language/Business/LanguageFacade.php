<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business;

use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Language\Transfer\FindLanguageRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\FindLanguageResponseTransfer;

final class LanguageFacade implements LanguageFacadeInterface
{

    /**
     * @var LanguageFacadeBusinessFactory
     */
    private LanguageFacadeBusinessFactory $factory;

    public function __construct(LanguageFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @return null|object 
     * @throws BindingResolutionException 
     */
    public function getMainLanguage(): ?object
    {
        return $this->factory
            ->getMainLanguageProcessor()
            ->get();
    }

    /**
     * @return mixed[]
     */
    public function getActivatedLanguages(): array
    {
        return $this->factory
            ->getActivatedLanguageProcessor()
            ->get();
    }

    /**
     * @param FindLanguageRequestTransfer $languageRequestTransfer
     * 
     * @return FindLanguageResponseTransfer
     */
    public function findLanguage(FindLanguageRequestTransfer $languageRequestTransfer): FindLanguageResponseTransfer
    {
        return $this->factory
            ->getLanguageReadRepository()
            ->findLanguage($languageRequestTransfer);
    }
}
