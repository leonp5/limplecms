<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Language\Persistence\Language\LanguageReadRepositoryInterface;
use Leonp5\Hybridcms\Language\Business\Processor\MainLanguage\MainLanguageProcessorInterface;
use Leonp5\Hybridcms\Language\Business\Processor\ActivatedLanguage\ActivatedLanguageProcessorInterface;

class LanguageFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return MainLanguageProcessorInterface
     */
    public function getMainLanguageProcessor(): MainLanguageProcessorInterface
    {
        return $this->app->make(MainLanguageProcessorInterface::class);
    }

    /**
     * @return ActivatedLanguageProcessorInterface
     */
    public function getActivatedLanguageProcessor(): ActivatedLanguageProcessorInterface
    {
        return $this->app->make(ActivatedLanguageProcessorInterface::class);
    }

    /**
     * @return LanguageReadRepositoryInterface
     * @throws BindingResolutionException
     */
    public function getLanguageReadRepository(): LanguageReadRepositoryInterface
    {
        return $this->app->make(LanguageReadRepositoryInterface::class);
    }
}
