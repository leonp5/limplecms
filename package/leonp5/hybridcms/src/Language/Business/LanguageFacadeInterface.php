<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business;

use Leonp5\Hybridcms\Language\Transfer\FindLanguageRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\FindLanguageResponseTransfer;

interface LanguageFacadeInterface
{
    /**
     * @return object|null 
     */
    public function getMainLanguage(): object | null;

    /**
     * @return mixed[]
     */
    public function getActivatedLanguages(): array;

    /**
     * @param FindLanguageRequestTransfer $languageRequestTransfer
     * 
     * @return FindLanguageResponseTransfer
     */
    public function findLanguage(FindLanguageRequestTransfer $languageRequestTransfer): FindLanguageResponseTransfer;
}
