<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\ActivatedLanguage;

interface ActivatedLanguageProcessorInterface
{
    /**
     * @return mixed[]
     */
    public function get(): array;
}
