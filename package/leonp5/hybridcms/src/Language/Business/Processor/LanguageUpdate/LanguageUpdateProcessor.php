<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdate;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Collection;

use Leonp5\Hybridcms\App\Models\ActivatedLanguages;
use Leonp5\Hybridcms\Content\Business\ContentFacadeInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateResponseTransfer;
use Leonp5\Hybridcms\ContentField\Business\ContentFieldFacadeInterface;

class LanguageUpdateProcessor implements LanguageUpdateProcessorInterface
{

    /**
     * @var ContentFacadeInterface
     */
    private ContentFacadeInterface $contentFacade;

    /**
     * @var ContentFieldFacadeInterface
     */
    private ContentFieldFacadeInterface $contentFieldFacade;

    /**
     * @param ContentFacadeInterface $contentFacade
     * @param ContentFieldFacadeInterface $contentFieldFacade
     * 
     * @return void
     */
    public function __construct(
        ContentFacadeInterface $contentFacade,
        ContentFieldFacadeInterface $contentFieldFacade
    ) {
        $this->contentFacade = $contentFacade;
        $this->contentFieldFacade = $contentFieldFacade;
    }

    /**
     * @param LanguageUpdateRequestTransfer $languageUpdateRequestTransfer
     * 
     * @return LanguageUpdateResponseTransfer 
     */
    public function update(LanguageUpdateRequestTransfer $languageUpdateRequestTransfer): LanguageUpdateResponseTransfer
    {
        $languageUpdateResponseTransfer = new LanguageUpdateResponseTransfer();

        $langIds = [];
        foreach ($languageUpdateRequestTransfer->getLanguages() as $langId => $language) {
            $langIds[] = $langId;
            ActivatedLanguages::updateOrCreate(
                ['alpha_2' => $language['alpha_2'], 'lang_id' => $langId],
                [
                    'disabled' => isset($language['disabled']),
                    'main_language' => isset($language['main_language'])
                ]
            );
            $contentTable = ContentTableInterface::CONTENT_TABLE_PREFIX . $language['alpha_2'];
            $templateContentFieldTable = ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $language['alpha_2'];

            if (!Schema::hasTable($contentTable)) {
                $this->contentFacade->createTable($contentTable);
            };
            if (!Schema::hasTable($templateContentFieldTable)) {
                $this->contentFieldFacade->createContentFieldTable($templateContentFieldTable);
            };
        }

        // If a language is deleted from the frontend table, it will not be send with the request and therefore should
        // be deleted from the DB table. So all languages which are not in the request, but exist in the DB table
        // will be deleted

        $langTablesToDelete = ActivatedLanguages::whereNotIn('lang_id', $langIds)->get();
        $this->deleteTables($langTablesToDelete);

        ActivatedLanguages::whereNotIn('lang_id', $langIds)->delete();

        $languageUpdateResponseTransfer->setSuccessful(true);
        return $languageUpdateResponseTransfer;
    }

    /**
     * @param Collection $tablesToDelete
     * 
     * @return void 
     */
    private function deleteTables(Collection $langTablesToDelete): void
    {
        foreach ($langTablesToDelete as $langTableToDelete) {
            $contentTable = ContentTableInterface::CONTENT_TABLE_PREFIX . $langTableToDelete->alpha_2;
            $contentFieldTable = ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $langTableToDelete->alpha_2;
            Schema::dropIfExists($contentTable);
            Schema::dropIfExists($contentFieldTable);
        }
    }
}
