<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdate;

use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateResponseTransfer;

interface LanguageUpdateProcessorInterface
{
    /**
     * @return LanguageUpdateResponseTransfer 
     */
    public function update(
        LanguageUpdateRequestTransfer $languageUpdateRequestTransfer
    ): LanguageUpdateResponseTransfer;
}
