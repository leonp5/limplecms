<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\LanguageUpdateValidation;

use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateValidationRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\LanguageUpdateValidationResponseTransfer;

class LanguageUpdateValidationProcessor implements LanguageUpdateValidationProcessorInterface
{
    /**
     * @param LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer 
     * 
     * @return LanguageUpdateValidationResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function validate(
        LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer
    ): LanguageUpdateValidationResponseTransfer {

        $languageUpdateValidationResponseTransfer = $this->hasLanguages($languageUpdateRequestTransfer);
        if ($languageUpdateValidationResponseTransfer->hasSuccess() === false) {
            return $languageUpdateValidationResponseTransfer;
        }

        $languageUpdateValidationResponseTransfer = $this->checkForExistingMainLanguage(
            $languageUpdateRequestTransfer,
            $languageUpdateValidationResponseTransfer
        );
        if ($languageUpdateValidationResponseTransfer->hasSuccess() === false) {
            return $languageUpdateValidationResponseTransfer;
        }

        $languageUpdateValidationResponseTransfer = $this->checkDisabledLanguages(
            $languageUpdateRequestTransfer,
            $languageUpdateValidationResponseTransfer
        );
        if ($languageUpdateValidationResponseTransfer->hasSuccess() === false) {
            return $languageUpdateValidationResponseTransfer;
        }

        return $languageUpdateValidationResponseTransfer;
    }

    /**
     * @param LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer 
     * @param LanguageUpdateValidationResponseTransfer $languageUpdateValidationResponseTransfer 
     * 
     * @return LanguageUpdateValidationResponseTransfer 
     */
    private function checkDisabledLanguages(
        LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer,
        LanguageUpdateValidationResponseTransfer $languageUpdateValidationResponseTransfer
    ): LanguageUpdateValidationResponseTransfer {

        $disabledLangCount = 0;
        $availableLanguages = count($languageUpdateRequestTransfer->getLanguages());

        foreach ($languageUpdateRequestTransfer->getLanguages() as $language) {
            if (array_key_exists('disabled', $language) === true) {
                $disabledLangCount++;
            }
        }

        if ($disabledLangCount === $availableLanguages) {
            $languageUpdateValidationResponseTransfer->setSuccessful(false);
            $languageUpdateValidationResponseTransfer
                ->setMessage(__('hybridcms::admin.languages.update.language.all.deactivated.error'));
            return $languageUpdateValidationResponseTransfer;
        }

        return $languageUpdateValidationResponseTransfer;
    }

    /**
     * @param LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer 
     * @param LanguageUpdateValidationResponseTransfer $languageUpdateValidationResponseTransfer 
     * 
     * @return LanguageUpdateValidationResponseTransfer 
     * @throws BindingResolutionException 
     */
    private function checkForExistingMainLanguage(
        LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer,
        LanguageUpdateValidationResponseTransfer $languageUpdateValidationResponseTransfer
    ): LanguageUpdateValidationResponseTransfer {

        $activeLanguageCount = 0;

        foreach ($languageUpdateRequestTransfer->getLanguages() as $language) {
            if (array_key_exists('main_language', $language) === true) {
                $activeLanguageCount++;
            }
        }
        if ($activeLanguageCount === 1) {
            return $languageUpdateValidationResponseTransfer;
        }

        $languageUpdateValidationResponseTransfer->setSuccessful(false);
        $languageUpdateValidationResponseTransfer
            ->setMessage(__('hybridcms::admin.languages.update.one.main.language.error'));
        return $languageUpdateValidationResponseTransfer;
    }

    /**
     * @param LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer 
     * 
     * @return LanguageUpdateValidationResponseTransfer 
     */
    private function hasLanguages(
        LanguageUpdateValidationRequestTransfer $languageUpdateRequestTransfer
    ): LanguageUpdateValidationResponseTransfer {
        $languageUpdateValidationResponseTransfer = new LanguageUpdateValidationResponseTransfer();
        if ($languageUpdateRequestTransfer->getLanguages() === null) {
            $languageUpdateValidationResponseTransfer->setSuccessful(false);
            $languageUpdateValidationResponseTransfer
                ->setMessage(__('hybridcms::admin.languages.update.language.all.deleted.error'));
            return $languageUpdateValidationResponseTransfer;
        }
        return $languageUpdateValidationResponseTransfer;
    }
}
