<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\MainLanguage;

use Illuminate\Support\Facades\DB;

class MainLanguageProcessor implements MainLanguageProcessorInterface
{

    /**
     * @return null|object
     */
    public function get(): ?object
    {
        return DB::table('activated_languages')
            ->select(['activated_languages.alpha_2'])
            ->where('activated_languages.main_language', '=', 1)
            ->first();
    }
}
