<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Business\Processor\MainLanguage;

interface MainLanguageProcessorInterface
{
    /**
     * @return object|null
     */
    public function get(): object | null;
}
