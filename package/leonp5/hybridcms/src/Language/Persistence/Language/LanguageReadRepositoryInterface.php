<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Persistence\Language;

use Leonp5\Hybridcms\Language\Transfer\FindLanguageRequestTransfer;
use Leonp5\Hybridcms\Language\Transfer\FindLanguageResponseTransfer;

interface LanguageReadRepositoryInterface
{
    /**
     * @param FindLanguageRequestTransfer $findLanguageRequestTransfer
     * 
     * @return FindLanguageResponseTransfer
     */
    public function findLanguage(
        FindLanguageRequestTransfer $findLanguageRequestTransfer
    ): FindLanguageResponseTransfer;
}
