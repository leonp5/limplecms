<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Language\Routes;

use Illuminate\Support\Facades\Route;
use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\Language\Controller\AdminLanguagesController;

final class LanguageRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        /**
         * Add, disable, delete languages & change main language
         */
        Route::prefix('languages')->middleware('can:admin')->group(function () {
            Route::get('/', [AdminLanguagesController::class, 'index'])
                ->name('languages.index');
            Route::post('/update-languages', [AdminLanguagesController::class, 'updateLanguages'])
                ->name('languages.updateLanguages');
        });
    }
}
