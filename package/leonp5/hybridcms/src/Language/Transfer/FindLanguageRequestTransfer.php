<?php

namespace Leonp5\Hybridcms\Language\Transfer;

class FindLanguageRequestTransfer
{
    /**
     * @var string
     */
    private string $langKey;

    /**
     * @var string
     */
    private string $wantedLangAlpha_2;

    /**
     * @return string
     */
    public function getLangKey(): string
    {
        return $this->langKey;
    }

    /**
     * @param string $langKey 
     *
     * @return self
     */
    public function setLangKey(string $langKey): self
    {
        $this->langKey = $langKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getWantedLangAlpha_2(): string
    {
        return $this->wantedLangAlpha_2;
    }

    /**
     * @param string $wantedLangAlpha_2 
     *
     * @return self
     */
    public function setWantedLangAlpha_2(string $wantedLangAlpha_2): self
    {
        $this->wantedLangAlpha_2 = $wantedLangAlpha_2;

        return $this;
    }
}
