<?php

namespace Leonp5\Hybridcms\Language\Transfer;

class LanguageUpdateRequestTransfer
{
    /**
     * @var array
     */
    private array $languages;

    /**
     * @param array $languages 
     */
    public function __construct(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param array  $languages
     *
     * @return  LanguageUpdateRequestTransfer
     */
    public function setLanguages(array $languages): LanguageUpdateRequestTransfer
    {
        $this->languages = $languages;

        return $this;
    }
}
