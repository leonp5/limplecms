<?php

namespace Leonp5\Hybridcms\Language\Transfer;

class LanguageUpdateValidationResponseTransfer
{
    /**
     * @var bool
     */
    private bool $successful = true;

    /**
     * @var string
     */
    private string $message;

    /**
     * Get the value of message
     *
     * @return  string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param  string  $message
     *
     * @return  LanguageUpdateValidationResponseTransfer
     */
    public function setMessage(string $message): LanguageUpdateValidationResponseTransfer
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return  bool
     */
    public function hasSuccess()
    {
        return $this->successful;
    }

    /**
     * @param  bool  $successful
     *
     * @return  LanguageUpdateValidationResponseTransfer
     */
    public function setSuccessful(bool $successful): LanguageUpdateValidationResponseTransfer
    {
        $this->successful = $successful;

        return $this;
    }
}
