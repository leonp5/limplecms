<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Business;

final class PageFacade implements PageFacadeInterface
{

    /**
     * @var PageFacadeBusinessFactory
     */
    private PageFacadeBusinessFactory $factory;

    public function __construct(PageFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }
}
