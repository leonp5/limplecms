<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Business\Processor\Create;

interface PageCreateProcessorInterface
{
    /**
     * @param int $templateId
     * 
     * @return object 
     * @throws InvalidArgumentException 
     */
    public function createNewPage(int $templateId): object;

    /**
     * @param int $pageId
     * 
     * @return object 
     * @throws InvalidArgumentException 
     */
    public function createExistingPage(int $pageId): object;
}
