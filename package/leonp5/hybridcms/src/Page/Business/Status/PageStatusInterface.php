<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Business\Status;

interface PageStatusInterface
{
    public const PAGE_STATUS_PUBLISHED = 1;
    public const PAGE_STATUS_DRAFT = 2;
    public const PAGE_STATUS_INACTIVE = 3;
}
