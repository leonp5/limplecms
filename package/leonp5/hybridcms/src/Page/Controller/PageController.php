<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Controller;

use RuntimeException;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\Container\BindingResolutionException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Leonp5\Hybridcms\App\Models\Page;
use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\Utility\Business\UtilityFacadeInterface;
use Leonp5\Hybridcms\Content\Business\ContentFacadeInterface;
use Leonp5\Hybridcms\Frontend\Business\FrontendFacadeInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentAddRequestTransfer;
use Leonp5\Hybridcms\Template\Business\Template\TemplateInterface;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataResponseTransfer;
use Leonp5\Hybridcms\Frontend\Transfer\PageAssembleRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentUpdateRequestTransfer;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateRequestTransfer;
use Leonp5\Hybridcms\Content\Transfer\ContentValidateResponseTransfer;
use Leonp5\Hybridcms\Page\Business\Processor\Create\PageCreateProcessorInterface;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(LanguageFacadeInterface $languageFacade)
    {
        $allPages = [];

        $mainLanguage = $languageFacade->getMainLanguage();

        $lang = $mainLanguage->alpha_2;

        $contentTable = 'content_' . $lang;
        $pageStatusLang = 'page_statuses_' . $lang;

        $allPages = DB::table('pages')
            ->join($pageStatusLang, 'pages.page_status_id', '=', $pageStatusLang . '.page_status_id')
            ->leftJoin($contentTable, 'pages.id', '=', $contentTable . '.page_id')
            ->select([
                'pages.id', 'pages.user_id', 'pages.available_lang', 'pages.deletable',
                $contentTable . '.title', $contentTable . '.url',
                $pageStatusLang . '.status_label', $pageStatusLang . '.page_status_id'
            ])
            ->paginate(15);

        return view('hybridcms::backend.user.pages.index', ['allPages' => $allPages]);
    }

    /**
     * @param Request $request
     * 
     * @return JsonResponse 
     * @throws InvalidArgumentException 
     * @throws BindingResolutionException 
     */
    public function toggleDeletableStatus(Request $request): JsonResponse
    {
        $pageId = (int) $request->input('page_id');
        $pageDeletableStatus = DB::table('pages')
            ->where('id', $pageId)
            ->first('deletable')->deletable;

        $response = DB::table('pages')
            ->where('id', $pageId)
            ->update(['deletable' => !$pageDeletableStatus]);

        if ($response === 0) {
            return response()->json(['error' => __('hybridcms::admin.templates.status.update.error')], 500);
        }
        return response()->json();
    }

    /**
     * @param string|null $templateId
     * @param PageCreateProcessorInterface $pageCreator
     * 
     * @return View|Factory|RedirectResponse
     * @throws BindingResolutionException
     */
    public function create(
        string $templateId = null,
        PageCreateProcessorInterface $pageCreator
    ) {
        if ($templateId === null) {
            return redirect()
                ->back()
                ->with(['error' => __('hybridcms::pages.page.create.error')]);
        }

        $page = $pageCreator->createNewPage((int) $templateId);

        return view(
            'hybridcms::backend.user.pages.create',
            ['page' => $page]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(
        Request $request,
        ContentFacadeInterface $contentFacade
    ) {
        $contentValidateRequestTransfer = new ContentValidateRequestTransfer();
        $contentValidateRequestTransfer->setGeneralContent($request->general_content);

        $contentValidateResponseTransfer = new ContentValidateResponseTransfer();

        // check if exists at least one title
        $contentFacade->validateContentTitle($contentValidateRequestTransfer, $contentValidateResponseTransfer);
        // check if url is valid and unique
        $contentFacade->validateContentUrl($contentValidateRequestTransfer, $contentValidateResponseTransfer);

        if (false === $contentValidateResponseTransfer->getSuccess()) {

            return back()->withInput()->with('error', $contentValidateResponseTransfer->getMessage());
        }

        $contentAddRequestTransfer = new ContentAddRequestTransfer();
        $contentAddRequestTransfer->setGeneralContent($request->general_content);
        $contentAddRequestTransfer->setPageContent($request->page_content);
        $contentAddRequestTransfer->setTemplateId((int) $request->input('template_id'));
        $contentAddRequestTransfer->setPageStatusId((int) $request->input('page_status'));

        $contentFacade->addContent($contentAddRequestTransfer);

        if (true !== $contentValidateResponseTransfer->getSuccess()) {

            return back()->with('error', $contentValidateResponseTransfer->getMessage());
        }
        return redirect()->route('pages.index')->with('success', __('hybridcms::admin.page.save.success'));
    }

    /**
     * @param Page $page
     * @param string $langKey
     * @param UtilityFacadeInterface $utilityFacade
     * 
     * @return View|Factory
     * @throws BindingResolutionException
     */
    public function pagePreview(
        Page $page,
        string $langKey,
        UtilityFacadeInterface $utilityFacade
    ) {
        $pageDataResponseTransfer = $utilityFacade
            ->requestPageDataByPageId($page->id, $langKey);
        $htmlFile = view(
            TemplateInterface::VIEW_PREFIX . $pageDataResponseTransfer->getViewString(),
            $pageDataResponseTransfer->getPageDataTransfer()->getPageData()
        )->render();

        $pageTitle = $pageDataResponseTransfer
            ->getPageDataTransfer()->getPageData()['meta']['title'];

        $fieldIds = $pageDataResponseTransfer
            ->getPageDataTransfer()->getPageData()['meta']['field_ids'];

        return view(
            'hybridcms::backend.user.pages.page-preview',
            [
                'pagePreview' => $htmlFile,
                'pageTitle' => $pageTitle,
                'pageId' => $page->id,
                'fieldIds' => $fieldIds
            ]
        );
    }

    /**
     * @param Page $page
     * @param PageCreateProcessorInterface $pageCreator
     * 
     * @return View|Factory
     * @throws BindingResolutionException
     */
    public function edit(
        Page $page,
        PageCreateProcessorInterface $pageCreator
    ) {
        $page = $pageCreator->createExistingPage($page->id);

        return view(
            'hybridcms::backend.user.pages.edit',
            ['page' => $page]
        );
    }

    /**
     * @param string|null $templateId
     * @param UtilityFacadeInterface $utilityFacade
     * @param FrontendFacadeInterface $frontendFacade
     * 
     * @return mixed 
     * @throws BindingResolutionException 
     * @throws InvalidArgumentException 
     */
    public function demoPreview(
        string $templateId = null,
        UtilityFacadeInterface $utilityFacade,
        FrontendFacadeInterface $frontendFacade
    ) {
        if (!$templateId) {
            return redirect()
                ->back()
                ->with(['error' => __('hybridcms::pages.template.preview.generation.error')]);
        }

        $template = Template::with('templateField')
            ->where('id', $templateId)
            ->get(['template_name', 'id', 'view_string'])
            ->first();

        if (!$template->templateField->first()) {
            return redirect()
                ->back()
                ->with(['error' => __(
                    'hybridcms::pages.template.no-preview.error',
                    ['templateName' => $template->template_name]
                )]);
        }

        $pageDataGetterResponseTransfer = $utilityFacade->createPreviewData($template);
        $pageAssemblerResponseTranser = $frontendFacade->assemblePage(
            $this->createPageAssembleRequestTransfer($pageDataGetterResponseTransfer)
        );

        return $pageAssemblerResponseTranser->getPage();
    }

    /**
     * @param Request $request
     * @param int $pageId
     * @param ContentFacadeInterface $contentFacade
     * 
     * @return RedirectResponse
     * @throws BindingResolutionException
     * @throws InvalidArgumentException
     * @throws RuntimeException
     * @throws RouteNotFoundException
     */
    public function update(
        Request $request,
        int $pageId,
        ContentFacadeInterface $contentFacade
    ) {
        $validateContentRequestTransfer = new ContentValidateRequestTransfer();
        $validateContentRequestTransfer->setGeneralContent($request->general_content);
        $validateContentRequestTransfer->setPageId($pageId);
        $validateContentResponseTransfer = new ContentValidateResponseTransfer();

        // check if exists at least one title
        $contentFacade->validateContentTitle($validateContentRequestTransfer, $validateContentResponseTransfer);
        // check if url is valid and unique
        $contentFacade->validateContentUrl($validateContentRequestTransfer, $validateContentResponseTransfer);

        if (true !== $validateContentResponseTransfer->getSuccess()) {

            return back()->withInput()->with('error', $validateContentResponseTransfer->getMessage());
        }

        $contentUpdateRequestTransfer = new ContentUpdateRequestTransfer();
        $contentUpdateRequestTransfer->setPageId($pageId);
        $contentUpdateRequestTransfer->setGeneralContent($request->general_content);
        $contentUpdateRequestTransfer->setPageContent($request->page_content);
        $contentUpdateRequestTransfer->setPageStatusId((int) $request->input('page_status'));


        $contentUpdateResponse = $contentFacade->updateContent($contentUpdateRequestTransfer);

        if ($contentUpdateResponse->hasSuccess() === false) {
            return back()->with('error', $contentUpdateResponse->getMessage());
        }

        return redirect()->route('pages.index')->with('success', __('hybridcms::admin.page.update.success'));
    }

    /**
     * @param Page $page
     * @param LanguageFacadeInterface $languageFacade
     * 
     * @return RedirectResponse
     * @throws InvalidArgumentException
     * @throws BindingResolutionException
     * @throws RouteNotFoundException
     */
    public function destroy(
        Page $page,
        LanguageFacadeInterface $languageFacade
    ) {
        $success = DB::table('pages')->where([
            'id' =>  $page->id,
            'deletable' => true
        ])->delete();

        if ($success === 0) {
            return redirect()->back()->with('error', __('hybridcms::pages.delete.error'));
        }

        $activeLanguages = $languageFacade->getActivatedLanguages();

        foreach ($activeLanguages as $activeLanguage) {

            $contentTable = ContentTableInterface::CONTENT_TABLE_PREFIX . $activeLanguage->alpha_2;
            $contentFieldTable = ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $activeLanguage->alpha_2;

            DB::table($contentTable)->where('page_id', $page->id)->delete();
            DB::table($contentFieldTable)->where('page_id', $page->id)->delete();
        }

        return redirect()->route('pages.index')->with('success', __('hybridcms::admin.page.delete.success'));
    }


    /**
     * @param GetPageDataResponseTransfer $pageDataGetterResponseTransfer
     * 
     * @return PageAssembleRequestTransfer
     */
    private function createPageAssembleRequestTransfer(
        GetPageDataResponseTransfer $pageDataGetterResponseTransfer
    ): PageAssembleRequestTransfer {

        $pageAssembleRequestTransfer = new PageAssembleRequestTransfer();

        $pageAssembleRequestTransfer->getStatusCode($pageDataGetterResponseTransfer->getStatusCode());
        $pageAssembleRequestTransfer->setViewString($pageDataGetterResponseTransfer->getViewString());
        $pageAssembleRequestTransfer->setPageDataTransfer($pageDataGetterResponseTransfer->getPageDataTransfer());

        return $pageAssembleRequestTransfer;
    }
}
