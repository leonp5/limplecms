<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Page\Business\PageFacade;
use Leonp5\Hybridcms\Page\Business\PageFacadeInterface;
use Leonp5\Hybridcms\Page\Business\PageFacadeBusinessFactory;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\Page\Business\Processor\Create\PageCreateProcessor;
use Leonp5\Hybridcms\Page\Business\Processor\Create\PageCreateProcessorInterface;

final class PageDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(PageFacadeBusinessFactory::class);

        $app->bind(PageFacadeInterface::class, function ($app) {
            return new PageFacade(
                $app->make(PageFacadeBusinessFactory::class)
            );
        });

        $app->bind(PageCreateProcessorInterface::class, function ($app) {
            return new PageCreateProcessor(
                $app->make(LanguageFacadeInterface::class)
            );
        });
    }
}
