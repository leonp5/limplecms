<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Page\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\Page\Controller\PageController;
use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;

final class PageRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        /*
        * IMPORTANT: The url is used in the url validation of page creation and update process (depecrated hint?)
        * 
        * Add, edit, move & delete pages
        */
        Route::prefix('pages')->middleware('auth')->group(function () {
            Route::get('/preview/{templateId?}', [PageController::class, 'demoPreview'])
                ->name('pages.demo.preview');
            Route::get('/create/{templateId?}', [PageController::class, 'create'])
                ->name('pages.create');
            Route::put('/update/{pageId}', [PageController::class, 'update'])
                ->name('pages.update');
            Route::post('/deletable', [PageController::class, 'toggleDeletableStatus'])
                ->name('pages.toggleDeletableStatus');
            Route::get('/page/{page}/{langKey}', [PageController::class, 'pagePreview'])
                ->name('pages.page.preview');
        });

        Route::resource(
            'pages',
            PageController::class,
            ['except' => ['show', 'update']]
        )->middleware('auth');
    }
}
