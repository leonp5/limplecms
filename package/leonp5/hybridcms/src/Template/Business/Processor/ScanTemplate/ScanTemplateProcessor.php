<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate;

use Illuminate\Support\Facades\Storage;

use Leonp5\Hybridcms\Template\Transfer\TemplateTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateScanResponseTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateFileScanRequestTransfer;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanUtils\TemplateScanUtilsProcessorInterface;

final class ScanTemplateProcessor implements ScanTemplateProcessorInterface
{
    /**
     * @var string
     */
    private string $excludeFromScanFolderPattern;

    /**
     * @var TemplateScanUtilsProcessorInterface
     */
    private TemplateScanUtilsProcessorInterface $scanUtils;

    /**
     * @return void 
     */
    public function __construct(TemplateScanUtilsProcessorInterface $scanUtils)
    {
        $this->scanUtils = $scanUtils;
        $this->excludeFromScanFolderPattern = $this->scanUtils->createSearchPattern('scan-exclude');
    }

    /**
     * @return TemplateScanResponseTransfer
     */
    public function scanPageTemplates(): TemplateScanResponseTransfer
    {
        $templateTransfer = new TemplateTransfer();
        $templateScanResponseTransfer = new TemplateScanResponseTransfer();

        $pattern = $this->scanUtils->createSearchPattern('partials');

        $files = Storage::disk('templates')->allFiles();

        $filesWithoutPartials = preg_grep($pattern, $files, PREG_GREP_INVERT);
        $filesToScan = preg_grep($this->excludeFromScanFolderPattern, $filesWithoutPartials, PREG_GREP_INVERT);

        foreach ($filesToScan as $filePath) {
            $templateFileScanRequestTransfer = new TemplateFileScanRequestTransfer();
            $templateFileScanRequestTransfer->setFilePath($filePath);
            $templateFileScanRequestTransfer->setSearchSingleUseTag(true);
            $templateFileScanResponseTransfer = $this->scanUtils->scanFile($templateFileScanRequestTransfer);

            if ($templateFileScanResponseTransfer->hasSuccess() === false) {
                $templateScanResponseTransfer->setMessage(
                    $templateFileScanResponseTransfer->getMessage()
                );
                $templateScanResponseTransfer->setSuccessful(false);
                return $templateScanResponseTransfer;
            }

            $templateTransfer->addTemplateFileTransfer(
                $templateFileScanResponseTransfer->getTemplateFileTransfer()
            );
        }

        $templateScanResponseTransfer->setTemplateTransfer($templateTransfer);
        return $templateScanResponseTransfer;
    }

    /**
     * @return TemplateScanResponseTransfer
     */
    public function scanPartials(): TemplateScanResponseTransfer
    {
        $templateTransfer = new TemplateTransfer();
        $templateScanResponseTransfer = new TemplateScanResponseTransfer();

        $searchFor = $this->scanUtils->createSearchPattern('partials');

        $files = Storage::disk('templates')->allFiles();

        $partials = preg_grep($searchFor, $files);
        $filesToScan = preg_grep($this->excludeFromScanFolderPattern, $partials, PREG_GREP_INVERT);

        foreach ($filesToScan as $filePath) {
            $templateFileScanRequestTransfer = new TemplateFileScanRequestTransfer();
            $templateFileScanRequestTransfer->setFilePath($filePath);
            $templateFileScanRequestTransfer->setSearchPartialTag(true);
            $templateFileScanRequestTransfer->setSearchComponentTag(false);
            $templateFileScanRequestTransfer->setSearchSingleUseTag(true);
            $templateFileScanResponseTransfer = $this->scanUtils->scanFile($templateFileScanRequestTransfer);

            if ($templateFileScanResponseTransfer->hasSuccess() === false) {
                $templateScanResponseTransfer->setMessage(
                    $templateFileScanResponseTransfer->getMessage()
                );
                $templateScanResponseTransfer->setSuccessful(false);
                return $templateScanResponseTransfer;
            }

            $templateTransfer->addTemplateFileTransfer(
                $templateFileScanResponseTransfer->getTemplateFileTransfer()
            );
        }

        $templateScanResponseTransfer->setTemplateTransfer($templateTransfer);

        return $templateScanResponseTransfer;
    }
}
