<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate;

use Leonp5\Hybridcms\Template\Transfer\TemplateScanResponseTransfer;

interface ScanTemplateProcessorInterface
{
    /**
     * @return TemplateScanResponseTransfer 
     */
    public function scanPageTemplates(): TemplateScanResponseTransfer;

    /**
     * @return TemplateScanResponseTransfer 
     */
    public function scanPartials(): TemplateScanResponseTransfer;
}
