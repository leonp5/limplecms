<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanUtils;

use Leonp5\Hybridcms\Template\Transfer\TemplateFileTransfer;
use Leonp5\Hybridcms\Template\Transfer\ContentFieldTransfer;
use Leonp5\Hybridcms\Template\Business\Template\TemplateInterface;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;
use Leonp5\Hybridcms\Template\Transfer\TemplateFileScanRequestTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateFileScanResponseTransfer;

final class TemplateScanUtilsProcessor implements TemplateScanUtilsProcessorInterface
{
    /**
     * @var array
     */
    private array $contentFieldNames = [];

    /**
     * @param TemplateFileScanRequestTransfer $templateFileScanRequestTransfer
     * 
     * @return TemplateFileScanResponseTransfer
     * @throws BindingResolutionException
     */
    public function scanFile(
        TemplateFileScanRequestTransfer $templateFileScanRequestTransfer
    ): TemplateFileScanResponseTransfer {
        $templateFileTransfer = new TemplateFileTransfer();
        $templateFileScanResponseTransfer = new TemplateFileScanResponseTransfer();
        $this->resetTemporaryContentFieldNames();

        $templateFileTransfer->setFilePath($templateFileScanRequestTransfer->getFilePath());

        $viewString = $this->createViewString($templateFileScanRequestTransfer->getFilePath());
        $viewStringWithPrefix = TemplateInterface::VIEW_PREFIX . $viewString;

        $templateName = ucfirst(substr(strrchr($viewStringWithPrefix, "."), 1));
        $templateFileTransfer->setTemplateName($templateName);
        $templateFileTransfer->setViewString($viewString);

        $templateFileScanResponseTransfer->setTemplateFileTransfer($templateFileTransfer);
        $htmlFile = view($viewStringWithPrefix)->render();
        $this->searchContentFields(
            $htmlFile,
            $templateFileScanResponseTransfer,
            ContentTableInterface::CONTENT_TYPE_TEXT,
            ContentTableInterface::CONTENT_TEXT_TAG
        );

        $this->searchContentFields(
            $htmlFile,
            $templateFileScanResponseTransfer,
            ContentTableInterface::CONTENT_TYPE_EDITOR,
            ContentTableInterface::CONTENT_EDITOR_TAG
        );

        if ($templateFileScanRequestTransfer->getSearchSingleUseTag() === true) {
            $this->checkBoolValue(
                $htmlFile,
                $templateFileScanResponseTransfer,
                TemplateInterface::SINGLE_USE_TAG
            );
        }
        if ($templateFileScanRequestTransfer->getSearchComponentTag() === true) {
            $this->checkBoolValue(
                $htmlFile,
                $templateFileScanResponseTransfer,
                TemplateInterface::COMPONENT_TAG,
            );
        }
        if ($templateFileScanRequestTransfer->getSearchPartialTag() === true) {
            $this->checkBoolValue(
                $htmlFile,
                $templateFileScanResponseTransfer,
                TemplateInterface::PARTIAL_TAG
            );
        }

        return $templateFileScanResponseTransfer;
    }

    /**
     * @param string $htmlFile
     * @param TemplateFileScanResponseTransfer $templateFileScanResponseTransfer
     * @param string $attributeToSearch
     * 
     * @return TemplateFileScanResponseTransfer
     * @throws BindingResolutionException
     */
    public function checkBoolValue(
        string $htmlFile,
        TemplateFileScanResponseTransfer $templateFileScanResponseTransfer,
        string $attributeToSearch
    ): TemplateFileScanResponseTransfer {
        $searchfor = $attributeToSearch . '="';
        $pattern = $this->createSearchPattern($searchfor);
        preg_match_all($pattern, $htmlFile, $matches);

        if (empty($matches[0])) {
            $templateFileScanResponseTransfer->setSuccessful(false);
            $templateName = $templateFileScanResponseTransfer->getTemplateFileTransfer()->getTemplateName();
            $msg = __(
                'hybridcms::admin.templates.data.attribute.missing.error',
                [
                    'template' => $templateName,
                    'attribute' => $attributeToSearch
                ]
            );
            $templateFileScanResponseTransfer->setMessage($msg);
            return $templateFileScanResponseTransfer;
        }

        $boolAsString = $this->getFieldName($matches[0][0], $searchfor);

        if ($boolAsString === 'true') {
            switch ($attributeToSearch) {
                case TemplateInterface::SINGLE_USE_TAG:
                    $templateFileScanResponseTransfer->getTemplateFileTransfer()->setSingleUse(true);

                    if (count($matches[0]) > 1) {

                        // if more than one single use tag is found, make sure, that is intended by
                        // checking if the INCLUDES_SINGLE_USE_TAGS is set too true
                        $this->checkBoolValue(
                            $htmlFile,
                            $templateFileScanResponseTransfer,
                            TemplateInterface::INCLUDES_SINGLE_USE_TAGS
                        );
                    }
                    break;

                case TemplateInterface::COMPONENT_TAG:
                    $templateFileScanResponseTransfer->getTemplateFileTransfer()->setComponent(true);
                    break;

                case TemplateInterface::PARTIAL_TAG:
                    $templateFileScanResponseTransfer->getTemplateFileTransfer()->setPartial(true);
                    break;
                case TemplateInterface::INCLUDES_SINGLE_USE_TAGS:
                    $templateFileScanResponseTransfer->getTemplateFileTransfer()->setSingleUse(false);
                    break;
            }
        }

        return $templateFileScanResponseTransfer;
    }

    /**
     * @param string $filePath 
     * 
     * @return string 
     */
    public function createViewString(string $filePath): string
    {
        $viewString = substr($filePath, 0, strpos($filePath, ".blade"));
        $viewString = str_replace('/', ".", $viewString);
        $viewString = $viewString;

        return $viewString;
    }

    /**
     * @param string $htmlFile
     * @param TemplateFileScanResponseTransfer $templateFileScanResponseTransfer
     * @param int $contentTypeId
     * @param string $searchfor
     * 
     * @return TemplateFileScanResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function searchContentFields(
        string $htmlFile,
        TemplateFileScanResponseTransfer $templateFileScanResponseTransfer,
        int $contentTypeId,
        string $searchfor,
    ): TemplateFileScanResponseTransfer {

        $searchfor = $searchfor . '="';
        $pattern = $this->createSearchPattern($searchfor);
        preg_match_all($pattern, $htmlFile, $matches);
        foreach ($matches[0] as $index => $contentFieldLine) {
            $contentFieldName = $this->getFieldName($contentFieldLine, $searchfor);

            if (in_array($contentFieldName, $this->contentFieldNames)) {
                $templateFileScanResponseTransfer->setSuccessful(false);
                $msg = __('hybridcms::admin.templates.field.already.exist.error', ['fieldName' => $contentFieldName]);
                $templateFileScanResponseTransfer->setMessage($msg);
                return $templateFileScanResponseTransfer;
            }

            $this->contentFieldNames[] = $contentFieldName;

            $contentFieldTransfer = new ContentFieldTransfer();
            $contentFieldTransfer->setFieldName($contentFieldName);
            $contentFieldTransfer->setContentTypeId($contentTypeId);

            $templateFileScanResponseTransfer
                ->getTemplateFileTransfer()
                ->addContentFieldTransfer($contentFieldTransfer);
        }


        return $templateFileScanResponseTransfer;
    }

    /**
     * @param string $contentFieldLine
     * @param string $searchfor
     * 
     * @return string 
     */
    public function getFieldName(string $contentFieldLine, string $searchfor): string
    {
        $contentFieldName = explode($searchfor, $contentFieldLine)[1];
        $contentFieldName = substr($contentFieldName, 0, strpos($contentFieldName, "\""));

        return $contentFieldName;
    }

    /**
     * @param string $string
     * 
     * @return string
     */
    public function createSearchPattern(string $searchfor): string
    {
        $pattern = preg_quote($searchfor, '/');
        $pattern = "/^.*$pattern.*\$/m";
        return $pattern;
    }

    /**
     * 
     * @return void 
     */
    private function resetTemporaryContentFieldNames()
    {
        $this->contentFieldNames = [];
    }
}
