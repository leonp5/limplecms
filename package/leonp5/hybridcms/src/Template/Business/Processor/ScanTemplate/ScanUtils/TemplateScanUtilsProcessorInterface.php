<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanUtils;

use Leonp5\Hybridcms\Template\Transfer\TemplateFileScanRequestTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateFileScanResponseTransfer;

interface TemplateScanUtilsProcessorInterface
{
    /**
     * @param TemplateFileScanRequestTransfer $templateFileScanRequestTransfer
     * 
     * @return TemplateFileScanResponseTransfer
     */
    public function scanFile(
        TemplateFileScanRequestTransfer $templateFileScanRequestTransfer
    ): TemplateFileScanResponseTransfer;

    /**
     * @param string $htmlFile
     * @param TemplateFileScanResponseTransfer $templateFileScanResponseTransfer
     * @param string $attributeToSearch
     * 
     * @return TemplateFileScanResponseTransfer
     */
    public function checkBoolValue(
        string $htmlFile,
        TemplateFileScanResponseTransfer $templateFileScanResponseTransfer,
        string $attributeToSearch
    ): TemplateFileScanResponseTransfer;

    /**
     * @param string $filePath 
     * 
     * @return string 
     */
    public function createViewString(string $filePath): string;

    /**
     * @param string $htmlFile
     * @param TemplateFileScanResponseTransfer $templateFileScanResponseTransfer
     * @param int $contentTypeId
     * @param string $searchfor
     * 
     * @return TemplateFileScanResponseTransfer 
     */
    public function searchContentFields(
        string $htmlFile,
        TemplateFileScanResponseTransfer $templateFileScanResponseTransfer,
        int $contentTypeId,
        string $searchfor,
    ): TemplateFileScanResponseTransfer;

    /**
     * @param string $contentFieldLine
     * @param string $searchfor
     * 
     * @return string
     */
    public function getFieldName(string $contentFieldLine, string $searchfor): string;

    /**
     * @param string $string
     * 
     * @return string
     */
    public function createSearchPattern(string $searchfor): string;
}
