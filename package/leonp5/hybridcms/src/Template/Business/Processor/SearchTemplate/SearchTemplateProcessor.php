<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\SearchTemplate;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateRequestTransfer;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateResponseTransfer;

final class SearchTemplateProcessor implements SearchTemplateProcessorInterface
{
    /**
     * @param SearchTemplateRequestTransfer $searchTemplateRequestTransfer
     * 
     * @return SearchTemplateResponseTransfer
     */
    public function search(
        SearchTemplateRequestTransfer $searchTemplateRequestTransfer
    ): SearchTemplateResponseTransfer {
        $templates = Template::get(['template.id', 'template_name', 'single_use']);

        if ($searchTemplateRequestTransfer->getAlreadyUsedSingleUseTemplatesIncluded() === false) {
            $templates = $this->excludeUsedSingleUse($templates);
        }

        return (new SearchTemplateResponseTransfer)->setTemplateCollection($templates);
    }

    /**
     * @param Collection $templates
     * 
     * @return Collection
     */
    private function excludeUsedSingleUse(Collection $templates): Collection
    {
        // remove templates from collection which are singleUse and already used
        $pageTemplateIdClasses = DB::table('pages')
            ->get('template_id')->toArray();

        $pageTemplateIds = array_map(function ($pageTemplateIdObject) {
            return $pageTemplateIdObject->template_id;
        }, $pageTemplateIdClasses);

        $singleUseTemplates = $templates->where('single_use', true);

        foreach ($singleUseTemplates as $key => $singleUseTemplate) {
            $templateUsed = array_search($singleUseTemplate->id, $pageTemplateIds);

            if ($templateUsed !== false) {
                $templates = $templates->filter(function ($template) use ($singleUseTemplate) {
                    return $template->id != $singleUseTemplate->id;
                })->values();

                // forget if found for making the array smaller & the iteration therefore faster
                $singleUseTemplates->forget($key);
            }
        }

        return $templates;
    }
}
