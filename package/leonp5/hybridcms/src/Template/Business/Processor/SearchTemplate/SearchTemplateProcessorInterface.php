<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\SearchTemplate;

use Leonp5\Hybridcms\Template\Transfer\SearchTemplateRequestTransfer;
use Leonp5\Hybridcms\Template\Transfer\SearchTemplateResponseTransfer;

interface SearchTemplateProcessorInterface
{
    /**
     * @param SearchTemplateRequestTransfer $searchTemplateRequestTransfer
     * 
     * @return SearchTemplateResponseTransfer
     */
    public function search(
        SearchTemplateRequestTransfer $searchTemplateRequestTransfer
    ): SearchTemplateResponseTransfer;
}
