<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\UnusedTemplates;

use Illuminate\Database\Eloquent\Collection;

interface SearchUnusedTemplatesProcessorInterface
{
    /**
     * @return null|Collection 
     */
    public function search(): ?Collection;
}
