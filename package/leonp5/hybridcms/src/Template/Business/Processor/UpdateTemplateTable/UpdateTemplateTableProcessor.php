<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\UpdateTemplateTable;

use Exception;

use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\App\Models\TemplateField;
use Leonp5\Hybridcms\Template\Transfer\TemplateTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateUpdateResponseTransfer;

final class UpdateTemplateTableProcessor implements UpdateTemplateTableProcessorInterface
{
    /**
     * @param TemplateTransfer $templateTransfer
     * 
     * @return TemplateUpdateResponseTransfer
     */
    public function update(TemplateTransfer $templateTransfer): TemplateUpdateResponseTransfer
    {
        $templateUpdateResponseTransfer = new TemplateUpdateResponseTransfer();
        try {
            foreach ($templateTransfer->getTemplateFileTransfers() as $templateFileTransfer) {
                $template = Template::updateOrCreate(
                    ['template_name' => $templateFileTransfer->getTemplateName()],
                    [
                        'single_use' => $templateFileTransfer->getSingleUse(),
                        'component' => $templateFileTransfer->getComponent(),
                        'partial' => $templateFileTransfer->getPartial(),
                        'view_string' => $templateFileTransfer->getViewString(),
                        'file_path' => $templateFileTransfer->getFilePath()
                    ]
                );

                foreach ($templateFileTransfer->getContentFieldTransfers() as $contentField) {
                    TemplateField::updateOrCreate(
                        ['template_id' => $template->id, 'field_name' => $contentField->getFieldName()],
                        [
                            'field_name' => $contentField->getFieldName(),
                            'content_type_id' => $contentField->getContentTypeId()
                        ]
                    );
                }
            }
        } catch (Exception $exception) {
            $templateUpdateResponseTransfer->setSuccessful(false);
            $templateUpdateResponseTransfer->setMessage($exception->getMessage());
        }

        return $templateUpdateResponseTransfer;
    }
}
