<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business\Processor\UpdateTemplateTable;

use Leonp5\Hybridcms\Template\Transfer\TemplateTransfer;
use Leonp5\Hybridcms\Template\Transfer\TemplateUpdateResponseTransfer;

interface UpdateTemplateTableProcessorInterface
{
    /**
     * @param TemplateTransfer $templateTransfer
     * 
     * @return TemplateUpdateResponseTransfer
     */
    public function update(
        TemplateTransfer $templateTransfer
    ): TemplateUpdateResponseTransfer;
}
