<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Business;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Container\BindingResolutionException;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanTemplateProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\SearchTemplate\SearchTemplateProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\UnusedTemplates\SearchUnusedTemplatesProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\UpdateTemplateTable\UpdateTemplateTableProcessorInterface;

class TemplateFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return UpdateTemplateTableProcessorInterface
     * @throws BindingResolutionException
     */
    public function getUpdateTemplateTableProcessor(): UpdateTemplateTableProcessorInterface
    {
        return $this->app->make(UpdateTemplateTableProcessorInterface::class);
    }

    /**
     * @return ScanTemplateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getScanTemplateProcessor(): ScanTemplateProcessorInterface
    {
        return $this->app->make(ScanTemplateProcessorInterface::class);
    }

    /**
     * @return SearchTemplateProcessorInterface
     * @throws BindingResolutionException
     */
    public function getSearchTemplateProcessor(): SearchTemplateProcessorInterface
    {
        return $this->app->make(SearchTemplateProcessorInterface::class);
    }

    /**
     * @return SearchUnusedTemplatesProcessorInterface
     * @throws BindingResolutionException
     */
    public function getSearchUnusedTemplatesProcessor(): SearchUnusedTemplatesProcessorInterface
    {
        return $this->app->make(SearchUnusedTemplatesProcessorInterface::class);
    }
}
