<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\App\Models\TemplateField;
use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\App\Models\TemplateScanOption;
use Leonp5\Hybridcms\Template\Business\TemplateFacadeInterface;

class AdminTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allTemplates = Template::get(['template_name', 'single_use', 'component', 'partial', 'id', 'disabled']);

        $components = $allTemplates->where('component', true);
        $partials = $allTemplates->where('partial', true);
        $singleUse = $allTemplates->where('single_use', true);

        $scanOptions = TemplateScanOption::all('scan_partials', 'scan_components', 'check_unused')
            ->first();

        $tabs = [
            'allTemplates' => [
                'number' => 1,
                'name' => __('hybridcms::admin.templates.overview.tab.all-templates'),
                'data' => $allTemplates
            ],
            'components' => [
                'number' => 2,
                'name' => __('hybridcms::admin.templates.overview.tab.components'),
                'data' => $components
            ],
            'partials' => [
                'number' => 3,
                'name' => __('hybridcms::admin.templates.overview.tab.partials'),
                'data' => $partials
            ],
            'single_use' => [
                'number' => 4,
                'name' => __('hybridcms::admin.templates.overview.tab.single-use'),
                'data' => $singleUse
            ],
        ];

        return view('hybridcms::backend.admin.templates.index', [
            'tabs' => $tabs,
            'scanOptions' => $scanOptions
        ]);
    }

    /**
     * @return JsonResponse
     * 
     * @throws BindingResolutionException
     */
    public function scan(
        TemplateFacadeInterface $templateFacade
    ): JsonResponse {
        $unusedTemplates = null;
        $scanOptions = TemplateScanOption::all('scan_partials', 'scan_components', 'check_unused')
            ->first();

        $templateScanResponseTransfer = $templateFacade->scanPageTemplates();
        if ($templateScanResponseTransfer->hasSuccess() !== true) {
            return response()->json(['error' => $templateScanResponseTransfer->getMessage()], 500);
        }
        $templateUpdateResponseTransfer = $templateFacade->updateTemplateTables($templateScanResponseTransfer->getTemplateTransfer());
        if ($templateUpdateResponseTransfer->hasSuccess() !== true) {
            return response()->json(['error' => $templateUpdateResponseTransfer->getMessage()], 500);
        }

        if ($scanOptions->scan_partials === true) {

            $partialScanResponseTransfer = $templateFacade->scanPartials();
            if ($partialScanResponseTransfer->hasSuccess() !== true) {
                return response()->json(['error' => $partialScanResponseTransfer->getMessage()], 500);
            }
            $partialUpdateResponseTransfer = $templateFacade->updateTemplateTables($partialScanResponseTransfer->getTemplateTransfer());
            if ($partialUpdateResponseTransfer->hasSuccess() !== true) {
                return response()->json(['error' => $templateUpdateResponseTransfer->getMessage()], 500);
            }
        }

        if ($scanOptions->check_unused === true) {
            $unusedTemplates = $templateFacade->searchUnusedTemplates();
        }

        return response()->json([
            'success' => __('hybridcms::admin.templates.scan.update.success'),
            'unusedTemplates' => $unusedTemplates ? $unusedTemplates : null
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * 
     * @throws BindingResolutionException
     */
    public function toggleStatus(Request $request): JsonResponse
    {
        $templateId = (int) $request->input('template_id');
        $templateStatus = DB::table('template')
            ->where('id', $templateId)
            ->first('disabled')->disabled;

        $response = DB::table('template')
            ->where('id', $templateId)
            ->update(['disabled' => !$templateStatus]);

        if ($response === 0) {
            return response()->json(['error' => __('hybridcms::admin.templates.status.update.error')], 500);
        }
        return response()->json();
    }

    /**
     * @param string $templateId
     * 
     * @return View|Factory
     * @throws BindingResolutionException
     */
    public function getFields(string $templateId)
    {
        $templateFields = DB::table('template_field')
            ->where('template_id', $templateId)
            ->join('content_field_types', 'template_field.content_type_id', '=', 'content_field_types.id')
            ->select(['field_name', 'template_id', 'label'])
            ->get();

        $template = Template::where('id', $templateId)->get('template_name')->first();

        return view(
            'hybridcms::backend.admin.templates.template-fields',
            [
                'templateFields' => $templateFields,
                'template' => $template
            ]
        );
    }

    /**
     * @param Request $request
     * 
     * @return JsonResponse 
     * @throws BindingResolutionException 
     */
    public function toggleScanOptions(Request $request): JsonResponse
    {
        $optionName = $request->input('option_name');
        $scanOptions = TemplateScanOption::first();
        $optionNameValue = $scanOptions->{$optionName};
        $scanOptions->{$optionName} = !$optionNameValue;
        $scanOptions->save();

        return response()->json();
    }

    /**
     * @param Request $request
     * 
     * @return RedirectResponse 
     */
    public function destroy(Request $request)
    {
        $templateToDeleteIds = $request->get('unused_templates');

        if ($templateToDeleteIds !== null) {
            $templatesToDelete = Template::whereIn('id', $templateToDeleteIds)->get(['id', 'file_path']);

            foreach ($templatesToDelete as $template) {
                Storage::disk('templates')->delete($template->file_path);
                $template->delete();
            }

            TemplateField::whereIn('template_id', $templateToDeleteIds)->delete();
            return redirect()->route('templates.index')->with('success', __('hybridcms::admin.template.delete.success'));
        }

        return redirect()->route('templates.index')->with('error', __('hybridcms::admin.template.delete.templates.missing.error'));
    }
}
