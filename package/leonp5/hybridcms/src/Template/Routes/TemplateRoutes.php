<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\Template\Controller\TemplateController;
use Leonp5\Hybridcms\Template\Controller\AdminTemplateController;

class TemplateRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        /**
         * Template scan, preview, editing
         */
        Route::prefix('templates')->group(function () {
            Route::get('/get', [TemplateController::class, 'searchTemplates'])
                ->name('templates.get')->middleware('auth');

            // Admin Routes
            Route::middleware('can:admin')->group(function () {
                Route::get('/', [AdminTemplateController::class, 'index'])
                    ->name('templates.index');
                Route::get('/scan', [AdminTemplateController::class, 'scan'])
                    ->name('templates.scan');
                Route::post('/toggle-status', [AdminTemplateController::class, 'toggleStatus'])
                    ->name('templates.toggleStatus');
                Route::post('/toggle-scan-options', [AdminTemplateController::class, 'toggleScanOptions'])
                    ->name('templates.toggle.scan-options');
                Route::get('/fields/{templateId}', [AdminTemplateController::class, 'getFields'])
                    ->name('templates.fields');
                Route::post('/destroy', [AdminTemplateController::class, 'destroy'])
                    ->name('templates.destroy');
            });
        });
    }
}
