<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Template\Business\TemplateFacade;
use Leonp5\Hybridcms\Template\Business\TemplateFacadeInterface;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\Template\Business\TemplateFacadeBusinessFactory;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanTemplateProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\SearchTemplate\SearchTemplateProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanTemplateProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\UnusedTemplates\SearchUnusedTemplatesProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\SearchTemplate\SearchTemplateProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\UpdateTemplateTable\UpdateTemplateTableProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanUtils\TemplateScanUtilsProcessor;
use Leonp5\Hybridcms\Template\Business\Processor\UnusedTemplates\SearchUnusedTemplatesProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\UpdateTemplateTable\UpdateTemplateTableProcessorInterface;
use Leonp5\Hybridcms\Template\Business\Processor\ScanTemplate\ScanUtils\TemplateScanUtilsProcessorInterface;

final class TemplateDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @param Application $app
     * 
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(TemplateFacadeBusinessFactory::class);

        $app->bind(TemplateFacadeInterface::class, function ($app) {
            return new TemplateFacade(
                $app->make(TemplateFacadeBusinessFactory::class)
            );
        });

        $app->bind(
            UpdateTemplateTableProcessorInterface::class,
            UpdateTemplateTableProcessor::class
        );

        $app->bind(
            ScanTemplateProcessorInterface::class,
            function ($app) {
                return new ScanTemplateProcessor(
                    $app->make(
                        TemplateScanUtilsProcessorInterface::class
                    )
                );
            }
        );

        $app->bind(
            TemplateScanUtilsProcessorInterface::class,
            TemplateScanUtilsProcessor::class
        );

        $app->bind(
            SearchUnusedTemplatesProcessorInterface::class,
            SearchUnusedTemplatesProcessor::class
        );

        $app->bind(
            SearchTemplateProcessorInterface::class,
            SearchTemplateProcessor::class
        );
    }
}
