<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class ContentFieldTransfer
{
    /**
     * @var string
     */
    private string $fieldName;

    /**
     * @var int
     */
    private int $contentTypeId;

    /**
     * @return string
     */
    public function getFieldName(): string
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName 
     *
     * @return ContentFieldTransfer
     */
    public function setFieldName(string $fieldName): ContentFieldTransfer
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * @return int
     */
    public function getContentTypeId(): int
    {
        return $this->contentTypeId;
    }

    /**
     * @param int $contentTypeId 
     *
     * @return ContentFieldTransfer
     */
    public function setContentTypeId(int $contentTypeId): ContentFieldTransfer
    {
        $this->contentTypeId = $contentTypeId;

        return $this;
    }
}
