<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class SearchTemplateRequestTransfer
{
    /**
     * @var bool
     */
    private bool $alreadyUsedSingleUseTemplatesIncluded = true;

    /**
     * @return bool
     */
    public function getAlreadyUsedSingleUseTemplatesIncluded(): bool
    {
        return $this->alreadyUsedSingleUseTemplatesIncluded;
    }

    /**
     * @param bool $alreadyUsedSingleUseTemplatesIncluded 
     *
     * @return self
     */
    public function setAlreadyUsedSingleUseTemplatesIncluded(bool $alreadyUsedSingleUseTemplatesIncluded): self
    {
        $this->alreadyUsedSingleUseTemplatesIncluded = $alreadyUsedSingleUseTemplatesIncluded;

        return $this;
    }
}
