<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

use Illuminate\Database\Eloquent\Collection;

class SearchTemplateResponseTransfer
{
    /**
     * @var Collection
     */
    private Collection $templateCollection;

    /**
     * @return Collection
     */
    public function getTemplateCollection(): Collection
    {
        return $this->templateCollection;
    }

    /**
     * @param Collection $templateCollection 
     *
     * @return self
     */
    public function setTemplateCollection(Collection $templateCollection): self
    {
        $this->templateCollection = $templateCollection;

        return $this;
    }
}
