<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class TemplateFileScanRequestTransfer
{
    /**
     * @var bool
     */
    private bool $searchComponentTag = false;

    /**
     * @var bool
     */
    private bool $searchSingleUseTag = false;

    /**
     * @var bool
     */
    private bool $searchPartialTag = false;

    /**
     * @var string
     */
    private string $filePath;

    /**
     * @return bool
     */
    public function getSearchComponentTag(): bool
    {
        return $this->searchComponentTag;
    }

    /**
     * @param bool $searchComponentTags
     *
     * @return TemplateFileScanRequestTransfer
     */
    public function setSearchComponentTag(bool $searchComponentTag): TemplateFileScanRequestTransfer
    {
        $this->searchComponentTag = $searchComponentTag;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSearchSingleUseTag(): bool
    {
        return $this->searchSingleUseTag;
    }

    /**
     * @param bool $searchSingleUseTag
     *
     * @return TemplateFileScanRequestTransfer
     */
    public function setSearchSingleUseTag(bool $searchSingleUseTag): TemplateFileScanRequestTransfer
    {
        $this->searchSingleUseTag = $searchSingleUseTag;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSearchPartialTag(): bool
    {
        return $this->searchPartialTag;
    }

    /**
     * @param bool $searchPartialTag
     *
     * @return TemplateFileScanRequestTransfer
     */
    public function setSearchPartialTag(bool $searchPartialTag): TemplateFileScanRequestTransfer
    {
        $this->searchPartialTag = $searchPartialTag;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     *
     * @return TemplateFileScanRequestTransfer
     */
    public function setFilePath(string $filePath): TemplateFileScanRequestTransfer
    {
        $this->filePath = $filePath;

        return $this;
    }
}
