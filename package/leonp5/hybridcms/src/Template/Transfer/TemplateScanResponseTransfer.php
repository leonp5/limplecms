<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class TemplateScanResponseTransfer
{
    /**
     * @var bool
     */
    private bool $successful = true;

    /**
     * @var string
     */
    private string $message;

    /**
     * @var TemplateTransfer
     */
    private TemplateTransfer $templateTransfer;

    /**
     * @return  bool
     */
    public function hasSuccess()
    {
        return $this->successful;
    }

    /**
     * @param  bool  $successful
     *
     * @return  TemplateScanResponseTransfer
     */
    public function setSuccessful(bool $successful): TemplateScanResponseTransfer
    {
        $this->successful = $successful;

        return $this;
    }

    /**
     * @return  string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param  string  $message
     *
     * @return  TemplateScanResponseTransfer
     */
    public function setMessage(string $message): TemplateScanResponseTransfer
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return TemplateTransfer
     */
    public function getTemplateTransfer(): TemplateTransfer
    {
        return $this->templateTransfer;
    }

    /**
     * @param TemplateTransfer $templateTransfer
     *
     * @return TemplateScanResponseTransfer
     */
    public function setTemplateTransfer(TemplateTransfer $templateTransfer): TemplateScanResponseTransfer
    {
        $this->templateTransfer = $templateTransfer;

        return $this;
    }
}
