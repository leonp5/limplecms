<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class TemplateTransfer
{
    /**
     * @var TemplateFileTransfer[]
     */
    private array $templateFiles = [];

    /**
     * @return  TemplateFileTransfer[]
     */
    public function getTemplateFileTransfers(): array
    {
        return $this->templateFiles;
    }

    /**
     * @param  TemplateFileTransfer[]  $templateFileTransfer
     *
     * @return  TemplateTransfer
     */
    public function addTemplateFileTransfer(TemplateFileTransfer $templateFileTransfer): TemplateTransfer
    {
        $this->templateFiles[] = $templateFileTransfer;

        return $this;
    }
}
