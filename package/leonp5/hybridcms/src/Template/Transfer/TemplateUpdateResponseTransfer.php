<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Template\Transfer;

class TemplateUpdateResponseTransfer
{
    /**
     * @var bool
     */
    private bool $successful = true;

    /**
     * @var string
     */
    private string $message;


    /**
     * @return  bool
     */
    public function hasSuccess()
    {
        return $this->successful;
    }

    /**
     * @param  bool  $successful
     *
     * @return  TemplateUpdateResponseTransfer
     */
    public function setSuccessful(bool $successful): TemplateUpdateResponseTransfer
    {
        $this->successful = $successful;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param  string  $message
     *
     * @return  TemplateUpdateResponseTransfer
     */
    public function setMessage(string $message): TemplateUpdateResponseTransfer
    {
        $this->message = $message;

        return $this;
    }
}
