<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\AdminAddUser;

use Leonp5\Hybridcms\User\Transfer\UserAddRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserAddResponseTransfer;

interface AdminAddUserProcessorInterface
{
    /**
     * @return UserAddResponseTransfer
     */
    public function add(UserAddRequestTransfer $userAddRequestTransfer): UserAddResponseTransfer;
}
