<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\AdminUpdateUser;

use Leonp5\Hybridcms\User\Transfer\AdminUserUpdateResponseTransfer;
use Leonp5\Hybridcms\User\Transfer\AdminUserUpdateRequestTransfer;

interface AdminUpdateUserProcessorInterface
{
    /**
     * @param AdminUserUpdateRequestTransfer $adminUserUpdateRequestTransfer
     * 
     * @return AdminUserUpdateResponseTransfer 
     */
    public function update(AdminUserUpdateRequestTransfer $adminUserUpdateRequestTransfer): AdminUserUpdateResponseTransfer;
}
