<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\PasswordValidation;

use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

class PasswordValidationProcessor implements
    PasswordValidationProcessorInterface
{

    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer
     * @throws InvalidArgumentException 
     * @throws BindingResolutionException 
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer {
        $userDataValidateResponseTransfer = new UserDataValidateResponseTransfer();
        $password = $userDataValidateRequestTransfer->getPassword();
        $cpassword = $userDataValidateRequestTransfer->getCpassword();

        if ($password === null) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.pw.empty.error');
            $userDataValidateResponseTransfer->setMessage($msg);
            return $userDataValidateResponseTransfer;
        }
        if ($cpassword === null) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.pw.empty.error');
            $userDataValidateResponseTransfer->setMessage($msg);
            return $userDataValidateResponseTransfer;
        }

        $pwRequirements = DB::table('settings_password')
            ->select(['settings_password.min_chars', 'settings_password.number_required', 'settings_password.capital_required'])
            ->first();

        if ($userDataValidateResponseTransfer->hasSuccess() !== true) {
            return $userDataValidateResponseTransfer;
        }

        $this->passwodHasMinLength($password, $pwRequirements->min_chars, $userDataValidateResponseTransfer);
        $this->passwodHasMinLength($cpassword, $pwRequirements->min_chars, $userDataValidateResponseTransfer);

        if ($userDataValidateResponseTransfer->hasSuccess() !== true) {
            return $userDataValidateResponseTransfer;
        }

        if ($pwRequirements->capital_required === 1) {
            $this->passwordHasCapitalLetter($password, $userDataValidateResponseTransfer);
            $this->passwordHasCapitalLetter($cpassword, $userDataValidateResponseTransfer);

            if ($userDataValidateResponseTransfer->hasSuccess() !== true) {
                return $userDataValidateResponseTransfer;
            }
        }

        if ($pwRequirements->number_required === 1) {
            $this->passwordHasNumber($password, $userDataValidateResponseTransfer);
            $this->passwordHasNumber($cpassword, $userDataValidateResponseTransfer);

            if ($userDataValidateResponseTransfer->hasSuccess() !== true) {
                return $userDataValidateResponseTransfer;
            }
        }

        if ($password !== $cpassword) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.pw.not.identic.error');
            $userDataValidateResponseTransfer->setMessage($msg);
            return $userDataValidateResponseTransfer;
        }


        return $userDataValidateResponseTransfer;
    }

    /**
     * @param string $password
     * @param int $minLength
     * @param UserDataValidateResponseTransfer $userDataValidateResponseTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     * @throws BindingResolutionException 
     */
    private function passwodHasMinLength(
        string $password,
        int $minLength,
        UserDataValidateResponseTransfer $userDataValidateResponseTransfer
    ): UserDataValidateResponseTransfer {
        if (strlen($password) < $minLength) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.short.pw.error', ['minLength' => $minLength]);
            $userDataValidateResponseTransfer->setMessage($msg);
        }
        return $userDataValidateResponseTransfer;
    }

    /**
     * @param string $password
     * @param UserDataValidateResponseTransfer $userDataValidateResponseTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     * @throws BindingResolutionException 
     */
    private function passwordHasCapitalLetter(
        string $password,
        UserDataValidateResponseTransfer $userDataValidateResponseTransfer
    ): UserDataValidateResponseTransfer {
        if (preg_match('/[A-Z]/', $password) == false) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.capital.pw.error');
            $userDataValidateResponseTransfer->setMessage($msg);
        }
        return $userDataValidateResponseTransfer;
    }

    /**
     * @param string $password
     * @param UserDataValidateResponseTransfer $userDataValidateResponseTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     * @throws BindingResolutionException 
     */
    private function passwordHasNumber(
        string $password,
        UserDataValidateResponseTransfer $userDataValidateResponseTransfer
    ): UserDataValidateResponseTransfer {
        if (preg_match('/[0-9]/', $password) == false) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.number.pw.error');
            $userDataValidateResponseTransfer->setMessage($msg);
        }
        return $userDataValidateResponseTransfer;
    }
}
