<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\PasswordValidation;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

interface PasswordValidationProcessorInterface
{

    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer;
}
