<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\RoleValidation;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

class RoleValidationProcessor implements RoleValidationProcessorInterface
{
    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer,
    ): UserDataValidateResponseTransfer {
        $userDataValidateResponseTransfer = new UserDataValidateResponseTransfer();

        $requestedUserUuid = $userDataValidateRequestTransfer->getUserUuid();

        $roles = $userDataValidateRequestTransfer->getRoles();

        if (empty($roles)) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.missing.role.error');
            $userDataValidateResponseTransfer->setMessage($msg);
            return $userDataValidateResponseTransfer;
        }

        // check if the current user wants to delete his admin rights
        if ($requestedUserUuid !== null) {
            $currentUserUuid = Auth::User()->uuid;

            if ($currentUserUuid === $requestedUserUuid) {
                $adminRoleId = DB::table('roles')
                    ->where('roles.role', '=', 'admin')
                    ->select(['roles.id'])
                    ->first();

                $exists = in_array($adminRoleId->id, $roles);

                if ($exists == false) {
                    $userDataValidateResponseTransfer->setSuccess(false);
                    $msg = __('hybridcms::user.admin.save.user.admin.role.error');
                    $userDataValidateResponseTransfer->setMessage($msg);
                }
            }
        }

        return $userDataValidateResponseTransfer;
    }
}
