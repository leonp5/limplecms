<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\RoleValidation;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

interface RoleValidationProcessorInterface
{
    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer,
    ): UserDataValidateResponseTransfer;
}
