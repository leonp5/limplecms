<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\UserDataValidation;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;
use Leonp5\Hybridcms\User\Business\Processor\RoleValidation\RoleValidationProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\PasswordValidation\PasswordValidationProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\UserNameValidation\UserNameValidationProcessorInterface;

class UserDataValidationProcessor implements UserDataValidationProcessorInterface
{
    /**
     * @var PasswordValidationProcessorInterface
     */
    private PasswordValidationProcessorInterface $passwordValidator;

    /**
     * @var UserNameValidationProcessorInterface
     */
    private UserNameValidationProcessorInterface $userNameValidator;

    /**
     * @var RoleValidationProcessorInterface
     */
    private RoleValidationProcessorInterface $roleValidator;

    /**
     * @param PasswordValidationProcessorInterface $passwordValidator
     * @param UserNameValidationProcessorInterface $userNameValidator
     * @param RoleValidationProcessorInterface $roleValidator
     * 
     * @return void 
     */
    public function __construct(
        PasswordValidationProcessorInterface $passwordValidator,
        UserNameValidationProcessorInterface $userNameValidator,
        RoleValidationProcessorInterface $roleValidator,
    ) {
        $this->passwordValidator = $passwordValidator;
        $this->roleValidator = $roleValidator;
        $this->userNameValidator = $userNameValidator;
    }

    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer {

        $userDataValidateResponseTransfer = new UserDataValidateResponseTransfer();

        $userNameValidationResponse = $this->userNameValidator->validate(
            $userDataValidateRequestTransfer
        );

        if ($userNameValidationResponse->hasSuccess() !== true) {
            return $userNameValidationResponse;
        }

        $roleValidationResponse = $this->roleValidator->validate(
            $userDataValidateRequestTransfer
        );

        if ($roleValidationResponse->hasSuccess() !== true) {
            return $roleValidationResponse;
        }

        $passwordValidationResponse = $this->passwordValidator->validate(
            $userDataValidateRequestTransfer
        );

        if ($passwordValidationResponse->hasSuccess() !== true) {
            return $passwordValidationResponse;
        }

        return $userDataValidateResponseTransfer;
    }

    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer
     */
    public function validateForUserUpdate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer {
        $userDataValidateResponseTransfer = new UserDataValidateResponseTransfer();

        $userNameValidationResponse = $this->userNameValidator->validate(
            $userDataValidateRequestTransfer
        );

        if ($userNameValidationResponse->hasSuccess() !== true) {
            return $userNameValidationResponse;
        }

        $roleValidationResponse = $this->roleValidator->validate(
            $userDataValidateRequestTransfer
        );

        if ($roleValidationResponse->hasSuccess() !== true) {
            return $roleValidationResponse;
        }

        if ($userDataValidateRequestTransfer->getPassword() || $userDataValidateRequestTransfer->getCpassword() !== null) {
            $passwordValidationResponse = $this->passwordValidator->validate(
                $userDataValidateRequestTransfer
            );

            if ($passwordValidationResponse->hasSuccess() !== true) {
                return $passwordValidationResponse;
            }
        }

        return $userDataValidateResponseTransfer;
    }
}
