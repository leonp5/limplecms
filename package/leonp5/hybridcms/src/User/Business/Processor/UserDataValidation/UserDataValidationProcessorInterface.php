<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\UserDataValidation;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

interface UserDataValidationProcessorInterface
{
    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer;

    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     */
    public function validateForUserUpdate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer;
}
