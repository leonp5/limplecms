<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\UserNameValidation;

use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

class UserNameValidationProcessor implements UserNameValidationProcessorInterface
{
    /**
     * @param UserDataValidateRequestTransfer $userValidateRequestTransfer
     * 
     * @return UserDataValidateResponseTransfer
     * @throws BindingResolutionException
     */
    public function validate(
        UserDataValidateRequestTransfer $userValidateRequestTransfer,
    ): UserDataValidateResponseTransfer {

        $userDataValidateResponseTransfer = new UserDataValidateResponseTransfer();

        $newUsername =  $userValidateRequestTransfer->getUserName();
        $requestedUserUuid = $userValidateRequestTransfer->getUserUuid();
        $minLength = 3;

        if ($newUsername === null) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.short.name.error', ['minLength' => $minLength]);
            $userDataValidateResponseTransfer->setMessage($msg);
            return $userDataValidateResponseTransfer;
        }

        if (strlen($newUsername) < $minLength) {
            $userDataValidateResponseTransfer->setSuccess(false);
            $msg = __('hybridcms::user.admin.save.user.short.name.error', ['minLength' => $minLength]);
            $userDataValidateResponseTransfer->setMessage($msg);
            return $userDataValidateResponseTransfer;
        }

        $userCollection = DB::table('users')
            ->select(['users.username', 'users.uuid'])
            ->get();

        foreach ($userCollection as $user) {
            if ($user->username === $newUsername) {
                if ($requestedUserUuid !== null && $requestedUserUuid === $user->uuid) {
                    return $userDataValidateResponseTransfer;
                }
                $userDataValidateResponseTransfer->setSuccess(false);
                $msg = __('hybridcms::user.admin.save.user.name.exists.error', ['name' => $newUsername]);
                $userDataValidateResponseTransfer->setMessage($msg);
                return $userDataValidateResponseTransfer;
            }
        }
        return $userDataValidateResponseTransfer;
    }
}
