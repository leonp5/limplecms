<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Business\Processor\UserNameValidation;

use Leonp5\Hybridcms\User\Transfer\UserDataValidateRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserDataValidateResponseTransfer;

interface UserNameValidationProcessorInterface
{
    /**
     * @param UserDataValidateRequestTransfer $userDataValidateRequestTransfer
     * @param UserDataValidateResponseTransfer $userDataValidateResponseTransfer
     * 
     * @return UserDataValidateResponseTransfer 
     */
    public function validate(
        UserDataValidateRequestTransfer $userDataValidateRequestTransfer
    ): UserDataValidateResponseTransfer;
}
