<?php

namespace Leonp5\Hybridcms\User\Controller;

use Carbon\Carbon;
use RuntimeException;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\Container\BindingResolutionException;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

use Leonp5\Hybridcms\App\Models\User;
use Leonp5\Hybridcms\App\Controller\Controller;
use Leonp5\Hybridcms\User\Transfer\UserAddRequestTransfer;
use Leonp5\Hybridcms\User\Persistence\UserReadRepositoryInterface;
use Leonp5\Hybridcms\User\Transfer\AdminUserUpdateRequestTransfer;
use Leonp5\Hybridcms\User\Business\Processor\AdminAddUser\AdminAddUserProcessorInterface;
use Leonp5\Hybridcms\User\Business\Processor\AdminUpdateUser\AdminUpdateUserProcessorInterface;

class AdminUsersController extends Controller
{
    /**
     * @return \Illuminate\Http\Response
     */
    public function index(UserReadRepositoryInterface $userReadRepository)
    {
        $allUsers = $userReadRepository->findAllUsers();

        $roleUserLinks = $userReadRepository->findAllUserRoles();

        foreach ($allUsers as $user) {
            $user->last_online_at = Carbon::createFromFormat('Y-m-d H:i:s', $user->last_online_at)->translatedFormat('D, j.m.y, H:i');
            $roles = [];
            foreach ($roleUserLinks as $userRole) {
                if ($user->id === $userRole->user_id) {
                    $roles[] = __('hybridcms::user.role.' . $userRole->role);
                }
            }
            $user->roles = implode(', ', $roles);
        }

        return view('hybridcms::backend.admin.users.index', ['allUsers' => $allUsers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(UserReadRepositoryInterface $userReadRepository)
    {
        $availableRoles = $userReadRepository->findAllRoles();

        return view('hybridcms::backend.admin.users.create', ['roles' => $availableRoles]);
    }

    /**
     * @param Request $request
     * @param AdminAddUserProcessorInterface $adminAddUserProcessor
     * 
     * @return RedirectResponse 
     * @throws BindingResolutionException 
     * @throws RuntimeException 
     * @throws RouteNotFoundException 
     */
    public function store(Request $request, AdminAddUserProcessorInterface $adminAddUserProcessor)
    {
        $userAddRequestTransfer = new UserAddRequestTransfer();
        $userAddRequestTransfer->setUserName($request->input('username'));
        $userAddRequestTransfer->setPassword($request->input('password'));
        $userAddRequestTransfer->setCpassword($request->input('cpassword'));
        $userAddRequestTransfer->setRoles($request->input('roles'));

        $userAddResponse = $adminAddUserProcessor->add($userAddRequestTransfer);


        if ($userAddResponse->hasSuccess() === false) {
            $request->session()->flash("error", $userAddResponse->getMessage());
            return redirect()->back()->withInput(
                $request->except('password', 'cpassword')
            );
        }

        return redirect()->route('users.index')->with('success', $userAddResponse->getMessage());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(
        string $userUuid,
        UserReadRepositoryInterface $userReadRepository
    ) {
        $userTransfer = $userReadRepository->findUserByUuid($userUuid);

        $roles = $userReadRepository->findAllRoles();

        $userRoles = $userReadRepository->findRolesByUserId($userTransfer->getUserId());

        foreach ($userRoles as $userRole) {
            foreach ($roles as $role) {
                if ($role->id === $userRole->role_id) {
                    $role->userHas = true;
                }
            }
        }

        return view('hybridcms::backend.admin.users.edit', ['user' => $userTransfer, 'roles' => $roles]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @param AdminUpdateUserProcessorInterface $adminUpdateUserProcessor
     * 
     * @return RedirectResponse 
     * @throws RuntimeException 
     * @throws BindingResolutionException 
     * @throws InvalidArgumentException 
     * @throws RouteNotFoundException 
     */
    public function update(
        Request $request,
        User $user,
        AdminUpdateUserProcessorInterface $adminUpdateUserProcessor
    ) {
        $adminUserUpdateRequestTransfer = new AdminUserUpdateRequestTransfer();
        $adminUserUpdateRequestTransfer->setUserName($request->input('username'));
        $adminUserUpdateRequestTransfer->setPassword($request->input('password'));
        $adminUserUpdateRequestTransfer->setCpassword($request->input('cpassword'));
        $adminUserUpdateRequestTransfer->setRoles($request->input('roles'));
        $adminUserUpdateRequestTransfer->setUserUuid($user->uuid);
        $adminUserUpdateRequestTransfer->setUser($user);

        $adminUserUpdateResponse = $adminUpdateUserProcessor->update($adminUserUpdateRequestTransfer);

        if ($adminUserUpdateResponse->hasSuccess() === false) {
            $request->session()->flash("error", $adminUserUpdateResponse->getMessage());
            return redirect()->back()->withInput(
                $request->except('password', 'cpassword')
            );
        }

        return redirect()->route('users.index')->with('success', $adminUserUpdateResponse->getMessage());
    }

    /**
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        DB::table('users')->where('id', $user->id)->delete();
        DB::table('role_user')->where('user_id', $user->id)->delete();

        return redirect()->route('users.index')
            ->with('success', __('hybridcms::user.admin.delete.user.success', ['username' => $user->username]));
    }
}
