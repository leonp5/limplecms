<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Persistence;

use Leonp5\Hybridcms\User\Transfer\UserTransfer;
use Leonp5\Hybridcms\User\Transfer\UserAddRequestTransfer;
use Leonp5\Hybridcms\User\Transfer\UserAddResponseTransfer;

interface UserWriteRepositoryInterface
{
    /**
     * @param UserAddRequestTransfer $userAddRequestTransfer
     * 
     * @return UserAddResponseTransfer 
     */
    public function add(
        UserAddRequestTransfer $userAddRequestTransfer
    ): UserAddResponseTransfer;

    /**
     * @param UserTransfer $userTransfer
     * 
     * @return bool 
     */
    public function update(
        UserTransfer $userTransfer
    ): bool;
}
