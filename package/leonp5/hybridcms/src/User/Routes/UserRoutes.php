<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\Hybridcms\App\Interfaces\HcmsRouteInterface;
use Leonp5\Hybridcms\User\Controller\AdminUsersController;

final class UserRoutes implements HcmsRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        /**
         * Add, edit, delete users & their roles
         */
        Route::resource(
            'users',
            AdminUsersController::class,
            ['except' => ['show']]
        )->middleware('can:admin');
    }
}
