<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Transfer;

class UserAddResponseTransfer
{
    /**
     * @var bool
     */
    private bool $success = true;

    /**
     * @var string
     */
    private string $message;

    /**
     * @param bool $success 
     *
     * @return self
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }

    /**
     * @return bool
     */
    public function hasSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message 
     *
     * @return self
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
