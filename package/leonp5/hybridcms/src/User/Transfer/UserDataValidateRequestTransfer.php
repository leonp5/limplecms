<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Transfer;

class UserDataValidateRequestTransfer
{
    /**
     * @var null|string
     */
    private ?string $userName;

    /**
     * @var null|string
     */
    private ?string $password;

    /**
     * @var null|string
     */
    private ?string $cpassword;

    /**
     * @var null|string
     */
    private ?string $userUuid = null;

    /**
     * @var null|string[]
     */
    private ?array $roles;

    /**
     * @return null|string
     */
    public function getCpassword(): null|string
    {
        return $this->cpassword;
    }

    /**
     * @param null|string $cpassword 
     *
     * @return self
     */
    public function setCpassword($cpassword): self
    {
        $this->cpassword = $cpassword;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUserName(): null|string
    {
        return $this->userName;
    }

    /**
     * @param null|string $userName 
     *
     * @return self
     */
    public function setUserName($userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPassword(): null|string
    {
        return $this->password;
    }

    /**
     * @param null|string $password 
     *
     * @return self
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUserUuid(): null|string
    {
        return $this->userUuid;
    }

    /**
     * @param null|string $userUuid 
     *
     * @return self
     */
    public function setUserUuid($userUuid): self
    {
        $this->userUuid = $userUuid;

        return $this;
    }

    /**
     * @return null|string[]
     */
    public function getRoles(): null|array
    {
        return $this->roles;
    }

    /**
     * @param null|string[] $roles 
     *
     * @return self
     */
    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
