<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\User\Transfer;

use Leonp5\Hybridcms\App\Models\User;

class UserTransfer
{
    /**
     * @var null|string
     */
    private ?string $userName;

    /**
     * @var null|string
     */
    private ?string $password;

    /**
     * @var null|string
     */
    private ?string $cpassword;

    /**
     * @var null|string[]
     */
    private ?array $roles;

    /**
     * @var string
     */
    private string $userUuid;

    /**
     * @var int
     */
    private int $userId;

    /**
     * @var User
     */
    private User $user;



    /**
     * @return null|string
     */
    public function getCpassword(): null|string
    {
        return $this->cpassword;
    }

    /**
     * @param null|string $cpassword 
     *
     * @return self
     */
    public function setCpassword($cpassword): self
    {
        $this->cpassword = $cpassword;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUserName(): null|string
    {
        return $this->userName;
    }

    /**
     * @param null|string $userName 
     *
     * @return self
     */
    public function setUserName($userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPassword(): null|string
    {
        return $this->password;
    }

    /**
     * @param null|string $password 
     *
     * @return self
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return null|string[]
     */
    public function getRoles(): null|array
    {
        return $this->roles;
    }

    /**
     * @param null|string[] $roles 
     *
     * @return self
     */
    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserUuid(): string
    {
        return $this->userUuid;
    }

    /**
     * @param string $userUuid 
     *
     * @return self
     */
    public function setUserUuid(string $userUuid): self
    {
        $this->userUuid = $userUuid;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user 
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId 
     *
     * @return self
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }
}
