<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Business\Processor\PageDataGetter;

use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\Utility\Traits\LocaleChangerTrait;
use Leonp5\Hybridcms\Utility\Transfer\PageDataTransfer;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataRequestTransfer;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataResponseTransfer;
use Leonp5\Hybridcms\Content\Business\Content\ContentTableInterface;

class PageDataGetterProcessor implements PageDataGetterProcessorInterface
{
    use LocaleChangerTrait;

    /**
     * @var GetPageDataResponseTransfer
     */
    private GetPageDataResponseTransfer $getPageDataResponseTransfer;

    /**
     * @var LanguageFacadeInterface
     */
    private LanguageFacadeInterface $languageFacade;

    /**
     * @var string|null
     */
    private ?string $locale = null;

    /**
     * @param LanguageFacadeInterface $languageFacade
     * 
     * @return void 
     */
    public function __construct(
        LanguageFacadeInterface $languageFacade
    ) {
        $this->getPageDataResponseTransfer = new GetPageDataResponseTransfer();
        $this->languageFacade = $languageFacade;
    }

    /**
     * @param GetPageDataRequestTransfer $getPageDataRequestTransfer
     * 
     * @return DataGetterResponseTransfer
     */
    public function requestMultilangData(
        GetPageDataRequestTransfer $getPageDataRequestTransfer,
    ): GetPageDataResponseTransfer {
        $url = $getPageDataRequestTransfer->getUrl();
        $pageStatus = $getPageDataRequestTransfer->getPageStatus();

        if ($getPageDataRequestTransfer->getLocale() !== null) {
            $this->locale = $getPageDataRequestTransfer->getLocale();
            $requestedPage = $this->searchPage($url, $pageStatus);
        } else {
            $activatedLanguages = $this->languageFacade->getActivatedLanguages();

            foreach ($activatedLanguages as $language) {

                $this->locale = $language->alpha_2;

                $requestedPage = $this->searchPage($url, $pageStatus);

                if ($requestedPage !== null) {
                    $this->createRedirect(
                        $getPageDataRequestTransfer->getUrl(),
                        303
                    );

                    return $this->getPageDataResponseTransfer;
                }
            }
        }

        if ($requestedPage === null) {
            $this->locale =
                $getPageDataRequestTransfer->getLocale() ?
                $getPageDataRequestTransfer->getLocale() :
                $this->languageFacade->getMainLanguage()->alpha_2;

            $this->set404Page();

            return $this->getPageDataResponseTransfer;
        }

        $this->getPageDataResponseTransfer->setUrl(
            $this->gbToEn(
                $this->locale
            )
                . $getPageDataRequestTransfer->getUrl()
        );

        $this->getPageDataResponseTransfer->setViewString($requestedPage->view_string);
        $this->getPageDataResponseTransfer->setPageDataTransfer(
            $this->createPageDataTransfer($requestedPage)
        );

        return $this->getPageDataResponseTransfer;
    }

    /**
     * @param GetPageDataRequestTransfer $getPageDataRequestTransfer
     * 
     * @return GetPageDataResponseTransfer 
     * @throws InvalidArgumentException 
     */
    public function requestData(
        GetPageDataRequestTransfer $getPageDataRequestTransfer
    ): GetPageDataResponseTransfer {
        $url = $getPageDataRequestTransfer->getUrl();
        $pageStatus = $getPageDataRequestTransfer->getPageStatus();
        $this->locale = $getPageDataRequestTransfer->getLocale();

        $requestedPage = $this->searchPage($url, $pageStatus);

        if ($requestedPage === null) {
            $this->set404Page();

            return $this->getPageDataResponseTransfer;
        }

        $this->getPageDataResponseTransfer->setViewString($requestedPage->view_string);
        $this->getPageDataResponseTransfer->setPageDataTransfer(
            $this->createPageDataTransfer($requestedPage)
        );
        return $this->getPageDataResponseTransfer;
    }

    /**
     * @param int $pageId
     * @param null|string $langKey
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function requestDataByPageId(int $pageId, ?string $langKey = null): GetPageDataResponseTransfer
    {
        if ($langKey === null) {
            $langKey = $this->languageFacade->getMainLanguage()->alpha_2;
        }

        $this->locale = $langKey;

        $requestedPage =  $this->searchPage($pageId);

        $this->getPageDataResponseTransfer->setViewString($requestedPage->view_string);
        $this->getPageDataResponseTransfer->setPageDataTransfer(
            $this->createPageDataTransfer($requestedPage)
        );

        return $this->getPageDataResponseTransfer;
    }

    /**
     * @param Template $template
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function createPreviewData(Template $template): GetPageDataResponseTransfer
    {
        $getPageDataResponseTransfer = new GetPageDataResponseTransfer();
        $pageDataTransfer = new PageDataTransfer();
        $pageData = [];
        $meta = [];

        foreach ($template->templateField as $field) {
            $pageData[$field->field_name] =  __('hybridcms::frontend.page.preview.text');
        }
        $meta['title'] = __('hybridcms::pages.title.preview', ['templateName' => $template->template_name]);
        $meta['description'] = __('hybridcms::pages.description.preview');

        $pageDataTransfer->setPageData($pageData);
        $pageDataTransfer->addPageData('meta', $meta);
        $getPageDataResponseTransfer->setPageDataTransfer($pageDataTransfer);
        $getPageDataResponseTransfer->setViewString($template->view_string);

        return $getPageDataResponseTransfer;
    }

    /**
     * @param string $url
     * @param int $statusCode
     * 
     * @return void 
     */
    private function createRedirect(
        string $url,
        int $statusCode
    ) {
        if (env('FRONTEND_MULTILANG') === true) {

            $this->locale = $this->gbToEn(
                $this->locale
            );

            $this->getPageDataResponseTransfer->setUrl(
                $this->locale . $url
            );
        } else {
            $this->pageDataGetterResponseTransfer->setUrl(
                $url
            );
        }

        $this->getPageDataResponseTransfer->setStatusCode($statusCode);
        $this->getPageDataResponseTransfer->setRedirectNecessary(true);
    }

    /**
     * @param object $requestedPage
     * 
     * @return PageDataTransfer 
     */
    private function createPageDataTransfer(
        object $requestedPage
    ): PageDataTransfer {
        $pageDataTransfer = new PageDataTransfer();

        $pageContentFieldCollection = $this->getContentFieldDataCollection($requestedPage->page_id);

        $pageData = [];
        $fieldIds = [];

        foreach ($pageContentFieldCollection as $contentFieldItem) {
            $pageData[$contentFieldItem->field_name] = $contentFieldItem->content;
            $fieldIds[$contentFieldItem->field_name] = $contentFieldItem->id;
        }
        $pageDataTransfer->setPageData($pageData);

        $meta['title'] = $requestedPage->title;
        $meta['description'] = $requestedPage->description;
        $meta['field_ids'] = $fieldIds;

        $pageDataTransfer->addPageData('meta', $meta);

        return $pageDataTransfer;
    }

    /**
     * @param int $pageId
     * 
     * @return Collection
     * @throws InvalidArgumentException
     */
    private function getContentFieldDataCollection(int $pageId): Collection
    {
        $contentFieldTable = ContentTableInterface::CONTENT_FIELD_TABLE_PREFIX . $this->locale;

        return DB::table($contentFieldTable)
            ->where([$contentFieldTable . '.page_id' => $pageId])
            ->join('template_field', 'template_field.id', '=', $contentFieldTable . '.template_field_id')
            ->select([
                'template_field' . '.field_name',
                $contentFieldTable . '.content', $contentFieldTable . '.id'
            ])
            ->get();
    }

    /**
     * @return void 
     */
    private function set404Page()
    {
        $this->createRedirect(
            '/404',
            404
        );
    }

    /**
     * @param string|int $searchParam
     * @param int|null $pageStatus
     * 
     * @return null|object 
     * @throws InvalidArgumentException 
     */
    private function searchPage(string|int $searchParam, int $pageStatus = null): ?object
    {
        $contentTable = ContentTableInterface::CONTENT_TABLE_PREFIX . $this->locale;

        if (gettype($searchParam) === 'string') {
            $col = '.url';
        }

        if (gettype($searchParam) === 'integer') {
            $col = '.page_id';
        }

        return DB::table($contentTable)
            ->where([$contentTable . $col => $searchParam])
            ->join('pages', function ($join) use ($contentTable, $pageStatus) {
                $join->on($contentTable . '.page_id', '=', 'pages.id')
                    ->where(function ($query) use ($pageStatus) {
                        $query->when($pageStatus, function ($query, $pageStatus) {
                            $query->where('pages.page_status_id', $pageStatus);
                        });
                    });
            })
            ->join('template', 'pages.template_id', '=', 'template.id')
            ->select([
                $contentTable . '.title',
                $contentTable . '.description',
                $contentTable . '.url',
                'pages.id as page_id',
                'template.view_string'
            ])
            ->first();
    }
}
