<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Business;

use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataRequestTransfer;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataResponseTransfer;

class UtilityFacade implements UtilityFacadeInterface
{

    /**
     * @var UtilityFacadeBusinessFactory
     */
    private UtilityFacadeBusinessFactory $factory;

    public function __construct(UtilityFacadeBusinessFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param GetPageDataRequestTransfer $getPageDataRequestTransfer
     * 
     * @return GetPageDataResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function findPageData(GetPageDataRequestTransfer $getPageDataRequestTransfer): GetPageDataResponseTransfer
    {
        return $this->factory
            ->getPageDataGetter()
            ->requestData($getPageDataRequestTransfer);
    }

    /**
     * @param GetPageDataRequestTransfer $getPageDataRequestTransfer
     * 
     * @return GetPageDataResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function findMultiLangPageData(GetPageDataRequestTransfer $getPageDataRequestTransfer): GetPageDataResponseTransfer
    {
        return $this->factory
            ->getPageDataGetter()
            ->requestMultilangData($getPageDataRequestTransfer);
    }

    /**
     * @param Template $template
     * 
     * @return GetPageDataResponseTransfer 
     * @throws BindingResolutionException 
     */
    public function createPreviewData(Template $template): GetPageDataResponseTransfer
    {
        return $this->factory
            ->getPageDataGetter()
            ->createPreviewData($template);
    }

    /**
     * @param int $pageId
     * @param string|null $langKey
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function requestPageDataByPageId(int $pageId, string $langKey = null): GetPageDataResponseTransfer
    {
        return $this->factory
            ->getPageDataGetter()
            ->requestDataByPageId($pageId, $langKey);
    }
}
