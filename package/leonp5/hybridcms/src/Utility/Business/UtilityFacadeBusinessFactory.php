<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Business;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Utility\Business\Processor\PageDataGetter\PageDataGetterProcessorInterface;

class UtilityFacadeBusinessFactory
{
    /**
     * @var Application
     */
    private Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @return PageDataGetterProcessorInterface
     */
    public function getPageDataGetter(): PageDataGetterProcessorInterface
    {
        return $this->app->make(PageDataGetterProcessorInterface::class);
    }
}
