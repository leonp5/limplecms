<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Business;

use Leonp5\Hybridcms\App\Models\Template;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataRequestTransfer;
use Leonp5\Hybridcms\Utility\Transfer\GetPageDataResponseTransfer;

interface UtilityFacadeInterface
{
    /**
     * @param GetPageDataRequestTransfer $getPageDataRequestTransfer
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function findPageData(
        GetPageDataRequestTransfer $getPageDataRequestTransfer
    ): GetPageDataResponseTransfer;

    /**
     * @param GetPageDataRequestTransfer $getPageDataRequestTransfer
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function findMultiLangPageData(
        GetPageDataRequestTransfer $getPageDataRequestTransfer
    ): GetPageDataResponseTransfer;

    /**
     * @param Template $template
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function createPreviewData(Template $template): GetPageDataResponseTransfer;

    /**
     * @param int $pageId
     * @param string|null $langKey
     * 
     * @return GetPageDataResponseTransfer 
     */
    public function requestPageDataByPageId(int $pageId, string $langKey = null): GetPageDataResponseTransfer;
}
