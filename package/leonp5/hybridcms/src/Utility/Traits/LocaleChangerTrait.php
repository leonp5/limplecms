<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Traits;

trait LocaleChangerTrait
{
    /**
     * @param string $locale
     * 
     * @return string 
     */
    public function enToGb(string $locale): string
    {
        if ($locale === 'en') {
            return 'gb';
        }

        return $locale;
    }

    /**
     * @param string $locale
     * 
     * @return string 
     */
    public function gbToEn(string $locale): string
    {
        if ($locale === 'gb') {
            return 'en';
        }

        return $locale;
    }
}
