<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility\Transfer;

class GetPageDataRequestTransfer
{
    /**
     * @var null|string
     */
    private ?string $url = null;

    /**
     * @var string|null
     */
    private ?string $locale = null;

    /**
     * @var int|null
     */
    private ?int $pageStatus = null;

    /**
     * @return bool 
     */
    public function hasUrl(): bool
    {
        return $this->url !== null;
    }

    /**
     * @return null|string
     */
    public function getUrl(): null|string
    {
        return $this->url;
    }

    /**
     * @param null|string $url 
     *
     * @return self
     */
    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): string|null
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale 
     *
     * @return self
     */
    public function setLocale($locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPageStatus(): int|null
    {
        return $this->pageStatus;
    }

    /**
     * @param int|null $pageStatus 
     *
     * @return self
     */
    public function setPageStatus($pageStatus): self
    {
        $this->pageStatus = $pageStatus;

        return $this;
    }
}
