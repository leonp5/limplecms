<?php

namespace Leonp5\Hybridcms\Utility\Transfer;

class PageDataTransfer
{
    /**
     * @var array
     */
    private array $pageData;

    /**
     * @return array
     */
    public function getPageData(): array
    {
        return $this->pageData;
    }

    /**
     * @param array $pageData 
     *
     * @return PageDataTransfer
     */
    public function setPageData(array $pageData): PageDataTransfer
    {
        $this->pageData = $pageData;

        return $this;
    }

    /**
     * @param string $key
     * @param array $pageData
     * 
     * @return PageDataTransfer 
     */
    public function addPageData(string $key, array $pageData): PageDataTransfer
    {
        $this->pageData[$key] = $pageData;

        return $this;
    }
}
