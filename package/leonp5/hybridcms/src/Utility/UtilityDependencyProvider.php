<?php

declare(strict_types=1);

namespace Leonp5\Hybridcms\Utility;

use Illuminate\Contracts\Foundation\Application;

use Leonp5\Hybridcms\Utility\Business\UtilityFacade;
use Leonp5\Hybridcms\Utility\Business\UtilityFacadeInterface;
use Leonp5\Hybridcms\Language\Business\LanguageFacadeInterface;
use Leonp5\Hybridcms\Utility\Business\UtilityFacadeBusinessFactory;
use Leonp5\Hybridcms\App\Interfaces\HcmsDependencyProviderInterface;
use Leonp5\Hybridcms\Utility\Business\Processor\PageDataGetter\PageDataGetterProcessor;
use Leonp5\Hybridcms\Utility\Business\Processor\PageDataGetter\PageDataGetterProcessorInterface;

final class UtilityDependencyProvider implements HcmsDependencyProviderInterface
{
    /**
     * @return void
     */
    public function provide(Application $app): void
    {
        $app->bind(UtilityFacadeBusinessFactory::class);

        $app->bind(UtilityFacadeInterface::class, function ($app) {
            return new UtilityFacade(
                $app->make(UtilityFacadeBusinessFactory::class)
            );
        });

        $app->bind(PageDataGetterProcessorInterface::class, function ($app) {
            return new PageDataGetterProcessor(
                $app->make(LanguageFacadeInterface::class)
            );
        });
    }
}
