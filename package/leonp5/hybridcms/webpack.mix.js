const mix = require('laravel-mix')
require('dotenv').config({ path: '../../../.env' })

let pathBackend
let pathFrontend
let pathFlagSvgs
// let tinyMCEfiles
let suneditorStyles
let pathBackendTranslations

// const quillIconsRegex = /node_modules\/quill\/assets\/icons\/(.*)\.svg$/
// // Do not use Laravel mix's built-in SVG loader for Quill SVG's
// mix.extend('quill', (webpackConfig) => {
//     const { rules } = webpackConfig.module

//     // 1. Exclude quill's SVG icons from existing rules
//     rules
//         .filter((rule) => /svg/.test(rule.test.toString()))
//         .forEach((rule) => (rule.exclude = /quill/))

//     // 2. Instead, use html-loader for quill's SVG icons
//     rules.push({
//         test: quillIconsRegex,
//         use: [{ loader: 'html-loader', options: { minimize: true } }],
//     })

//     // 3. Don't exclude quill from babel-loader rule
//     rules
//         .filter(
//             (rule) =>
//                 rule.exclude &&
//                 rule.exclude.toString() === '/(node_modules|bower_components)/'
//         )
//         .forEach((rule) => (rule.exclude = /node_modules\/(?!(quill)\/).*/))
// })

if (process.env.NODE_ENV === 'development') {
    pathPublicFolder = '../../../public/assets'
    mix.setPublicPath(pathPublicFolder)
    pathBackend = 'backend'
    pathFrontend = 'frontend'
    pathFlagSvgs = pathPublicFolder + '/' + pathBackend + '/svgs/flags'
    // tinyMCEfiles = pathPublicFolder + '/' + pathBackend + '/tiny'
    suneditorStyles = pathPublicFolder + '/' + pathBackend + '/suneditor'
    pathBackendTranslations =
        pathPublicFolder + '/' + pathBackend + '/translations'
}

if (process.env.NODE_ENV === 'prepare-assets') {
    pathBackend = 'dist/backend'
    pathFrontend = 'dist/frontend'
    pathFlagSvgs = pathBackend + '/svgs/flags'
    // tinyMCEfiles = pathBackend + '/tiny'
    suneditorStyles = pathBackend + '/suneditor'
    pathBackendTranslations = pathBackend + '/translations'
}

const projectName = process.env.FRONTEND_NAME

// Flags css
mix.copyDirectory('node_modules/flag-icon-css/flags', pathFlagSvgs)
// mix.copyDirectory('node_modules/tinymce/skins', tinyMCEfiles)
// mix.copyDirectory(
//     'node_modules/tinymce/plugins/emoticons/js/emojis.min.js',
//     tinyMCEfiles
//     )

// Suneditor styles
mix.copyDirectory(
    'node_modules/suneditor/dist/css/suneditor.min.css',
    suneditorStyles
)
mix.copyDirectory(
    'node_modules/suneditor/src/assets/css/suneditor-contents.css',
    suneditorStyles
)

// Backend translation files
mix.copyDirectory(
    'resources/assets/backend/js/translations',
    pathBackendTranslations
)

// Backend files
mix.js('resources/assets/backend/js/hcms.js', pathBackend)
    .js('resources/assets/backend/js/hcms-vendor.js', pathBackend)
    .sass('resources/assets/backend/sass/hcms.scss', pathBackend)
    .sass('resources/assets/backend/sass/vendor/flags.scss', pathBackend)
    .options({
        processCssUrls: false,
    })

// Frontend files
mix.js(
    'resources/assets/frontend/js/ExampleProject.js',
    pathFrontend + '/' + projectName + '.js'
)
    .js(
        'resources/assets/frontend/js/ExampleProject-vendor.js',
        pathFrontend + '/' + projectName + '-vendor.js'
    )
    .sass(
        'resources/assets/frontend/sass/ExampleProject.scss',
        pathFrontend + '/' + projectName + '.css'
    )

mix.browserSync({
    proxy: 'http://127.0.0.1:8000',

    files: [
        'src/*.php',
        'src/**/*.php',
        'routes/*.php',
        'resources/**/*.php',
        'resources/assets/backend/js/*.js',
        'resources/assets/backend/js/**/*.js',
        'resources/assets/backend/sass/*.scss',
    ],
    // => true = the browser opens a new browser window with every npm run watch startup
    open: false,
    notify: false,
    ui: false,
})
mix.disableNotifications()
