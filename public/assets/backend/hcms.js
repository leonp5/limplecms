/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./resources/assets/backend/js/components/DescriptionPreview.js":
/*!**********************************************************************!*\
  !*** ./resources/assets/backend/js/components/DescriptionPreview.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ DescriptionPreview)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var DescriptionPreview = /*#__PURE__*/function () {
  function DescriptionPreview() {
    _classCallCheck(this, DescriptionPreview);

    this.descriptionPreviewElements = null;
  }

  _createClass(DescriptionPreview, [{
    key: "init",
    value: function init(htmlEl) {
      this.descriptionPreviewElements = Array.from(document.querySelectorAll('[data-hcms-preview]'));
      this.attachEventlistener(this.descriptionPreviewElements, 'data-hcms-preview');
    }
  }, {
    key: "attachEventlistener",
    value: function attachEventlistener(elements, dataAttribute) {
      var _this = this;

      elements.forEach(function (el) {
        var id = el.getAttribute(dataAttribute);
        var inputField = document.getElementById(id);
        inputField.addEventListener('keyup', function (e) {
          _this.insertText(id, e);
        }); // refill the preview in case of a validation error redirect back from the backend

        if (inputField.value.length > 0) {
          inputField.dispatchEvent(new KeyboardEvent('keyup', {
            keyCode: 37
          }));
        }
      });
    }
  }, {
    key: "insertText",
    value: function insertText(elementId, e) {
      var previewEl = document.querySelector("[data-hcms-preview=\"".concat(elementId, "\"]"));

      if (elementId.includes('url') === true) {
        previewEl.innerHTML = window.location.origin + e.target.value;
        return;
      }

      if (elementId.includes('description') === true) {
        this.handleSignCount(elementId, e.target.value.length);
      }

      previewEl.innerHTML = e.target.value;
    }
  }, {
    key: "handleSignCount",
    value: function handleSignCount(elementId, amountSigns) {
      var countEl = document.querySelector("[data-hcms-preview-word-count=\"".concat(elementId, "\"]"));
      countEl.innerHTML = amountSigns;

      if (amountSigns >= 170) {
        countEl.classList.add('text-danger');
      }

      if (amountSigns < 170) {
        countEl.classList.remove('text-danger');
      }
    }
  }]);

  return DescriptionPreview;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/HandleLanguageCheckboxes.js":
/*!****************************************************************************!*\
  !*** ./resources/assets/backend/js/components/HandleLanguageCheckboxes.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ HandleLanguageCheckboxes)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var HandleLanguageCheckboxes = /*#__PURE__*/function () {
  function HandleLanguageCheckboxes() {
    _classCallCheck(this, HandleLanguageCheckboxes);

    this.activeLangContainer = null;
    this.allLangContainer = null;
    this.activeLangsDisablingCheckboxes = null;
    this.allLangDeleteButtons = null;
    this.mainLanguageCheckboxes = null;
    this.allLangCheckboxes = null;
    this.updateButtonEnabled = false;
    this.mainLangSelected = true;
    this.updateButton = null;
    this.activeLangRowPatternElement = null;

    this.deleteLangButtonClicked = function (e) {
      var _this = this;

      var tableRow = e.target.closest('tr');
      var langId = tableRow.getAttribute('data-hcms-language-id');
      var allLangCheckbox = this.allLangCheckboxes.find(function (inputEl) {
        return inputEl.getAttribute('data-hcms-language-id') === langId;
      });
      allLangCheckbox.checked = false;
      tableRow.classList.add('fade');
      window.setTimeout(function () {
        tableRow.style.display = 'none';
        tableRow.remove();

        var activeLangCount = _this.activeLangContainer.querySelectorAll('tr[data-hcms-language-id]').length;

        document.getElementById('hcms_active_lang_count').innerHTML = activeLangCount;
      }, 300);
      this.enableUpdateButton();
    };

    this.handleLanguageDelete = this.deleteLangButtonClicked.bind(this);
  }

  _createClass(HandleLanguageCheckboxes, [{
    key: "init",
    value: function init(htmlElement) {
      var _this2 = this;

      this.activeLangContainer = htmlElement;
      this.allLangContainer = document.getElementById('hcms_all_languages');
      this.activeLangsDisablingCheckboxes = this.getActiveLangsDisablingCheckboxes();
      this.allLangDeleteButtons = this.getDeleteButtons();
      this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes();
      this.allLangCheckboxes = this.getAllLanguagesCheckboxes();
      this.updateButton = document.getElementById('hmcs-update-languages');
      this.activeLangRowPatternElement = this.createLanguageRowPatternElement();
      this.activeLangsDisablingCheckboxes.forEach(function (checkBoxEl) {
        checkBoxEl.addEventListener('click', function (e) {
          _this2.handleLanguageDisabling(e);
        });
      });
      this.mainLanguageCheckboxes.forEach(function (checkBoxEl) {
        checkBoxEl.addEventListener('click', function (e) {
          _this2.handleMainLanguageSelection(e);
        });
      });
      document.getElementById('all_lang_search').addEventListener('keyup', function (e) {
        _this2.handleAllLangSearchFilter(e);
      });
      this.allLangDeleteButtons.forEach(function (buttonEl) {
        if (!buttonEl.classList.contains('hcms-cursor-forbidden')) {
          buttonEl.addEventListener('click', _this2.handleLanguageDelete);
        }
      });
      this.allLangCheckboxes.forEach(function (checkBoxEl) {
        checkBoxEl.addEventListener('click', function (e) {
          _this2.handleLanguageAddClick(e);
        });
      });
    }
  }, {
    key: "createLanguageRowPatternElement",
    value: function createLanguageRowPatternElement() {
      var tableRow = this.activeLangContainer.querySelector('tr[data-hcms-language-id]').cloneNode(true);
      var deleteButton = tableRow.querySelector('svg[class^="svg-delete"]');

      if (deleteButton.classList.contains('hcms-cursor-forbidden') === true) {
        deleteButton.classList.remove('hcms-cursor-forbidden');
      }

      return tableRow;
    }
  }, {
    key: "handleMainLanguageSelection",
    value: function handleMainLanguageSelection(e) {
      var deleteButton = e.target.closest('tr').querySelector('svg[class^="svg-delete"]');

      if (parseInt(e.target.value) === 1) {
        e.target.value = 0;
        e.target.checked = false;
        this.mainLangSelected = false;
        deleteButton.classList.remove('hcms-cursor-forbidden');
        deleteButton.addEventListener('click', this.handleLanguageDelete);
        this.mainLanguageCheckboxes.forEach(function (checkbox) {
          var activeLangDisablingCheckbox = checkbox.closest('tr').querySelector('input[data-hcms-lang-disabled=""]');

          if (activeLangDisablingCheckbox.checked === true) {
            return;
          }

          activeLangDisablingCheckbox.disabled = false;
          checkbox.disabled = false;
        });
        this.disableUpdateButton();
      } else {
        e.target.value = 1;
        e.target.checked = true;
        this.mainLangSelected = true;
        deleteButton.classList.add('hcms-cursor-forbidden');
        deleteButton.removeEventListener('click', this.handleLanguageDelete);
        this.mainLanguageCheckboxes.forEach(function (checkbox) {
          if (checkbox === e.target) {
            var activeLangDisablingCheckbox = checkbox.closest('tr').querySelector('input[data-hcms-lang-disabled=""]');
            activeLangDisablingCheckbox.disabled = true;
            return;
          }

          checkbox.disabled = true;
        });
        this.enableUpdateButton();
      }

      this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes();
    }
  }, {
    key: "handleLanguageAddClick",
    value: function handleLanguageAddClick(e) {
      var _this3 = this;

      var langId = e.target.getAttribute('data-hcms-language-id');
      var alreadyAdded = this.activeLangContainer.querySelector("tr[data-hcms-language-id=\"".concat(langId, "\"]"));

      if (alreadyAdded === null) {
        var langAlpha2Code = e.target.value;
        var langName = e.target.name;
        var clonedRow = this.activeLangRowPatternElement.cloneNode(true);
        clonedRow.setAttribute('data-hcms-language-id', langId);
        clonedRow.setAttribute('data-hcms-language-alpha_2', langAlpha2Code); // set the data from the activated lanuage to the cloned row
        // set flag icon

        clonedRow.getElementsByClassName('flag-icon')[0].className = "flag-icon flag-icon-".concat(langAlpha2Code); // set language name

        clonedRow.querySelector('p[id^="lang_name"]').innerHTML = langName; // give main lang checkbox correct id value & add eventlistener

        var mainLangCheckbox = clonedRow.querySelector('input[id^="main"]');
        mainLangCheckbox.setAttribute('id', "main_".concat(langId));

        if (this.mainLangSelected === true) {
          mainLangCheckbox.disabled = true;
        }

        mainLangCheckbox.value = 0;
        mainLangCheckbox.checked = false;
        mainLangCheckbox.addEventListener('click', function (e) {
          _this3.handleMainLanguageSelection(e);
        }); // update main lang checkbox label

        clonedRow.querySelector('label[for^="main"]').setAttribute('for', "main_".concat(langId)); // give lang disabling checkbox correct id, value & add eventlistener

        var activeLangDisablingLangCheckbox = clonedRow.querySelector('input[id^="disabled"]');
        activeLangDisablingLangCheckbox.setAttribute('id', "disabled_".concat(langId));
        activeLangDisablingLangCheckbox.value = 0;
        activeLangDisablingLangCheckbox.disabled = false;
        activeLangDisablingLangCheckbox.addEventListener('click', function (e) {
          _this3.handleLanguageDisabling(e);
        }); // update disable lang checkbox label

        clonedRow.querySelector('label[for^="disabled"]').setAttribute('for', "disabled_".concat(langId)); // add delete function to delete button

        clonedRow.querySelector('svg[name="delete"]').addEventListener('click', function (e) {
          _this3.handleLanguageDelete(e);
        }); // update hidden input field

        var hiddenInput = clonedRow.querySelector('input[name^="languages"]');
        hiddenInput.setAttribute('name', "languages[".concat(langId, "][alpha_2]"));
        hiddenInput.value = langAlpha2Code;
        this.activeLangContainer.querySelector('tbody').append(clonedRow);
      } else {
        this.activeLangContainer.querySelector("tr[data-hcms-language-id=\"".concat(langId, "\"]")).remove();
      }

      var activeLangCount = this.activeLangContainer.querySelectorAll('tr[data-hcms-language-id]').length;
      document.getElementById('hcms_active_lang_count').innerHTML = activeLangCount;
      this.enableUpdateButton();
      this.mainLanguageCheckboxes = this.getMainLanguageCheckboxes();
    }
  }, {
    key: "handleLanguageDisabling",
    value: function handleLanguageDisabling(e) {
      var tableRow = e.target.closest('tr');
      var mainLangCheckbox = tableRow.querySelector('input[data-hcms-main-language=""]');
      var mainLanguageChecked = this.isMainLangCheckboxChecked();

      if (parseInt(e.target.value) === 1) {
        e.target.value = 0;
        e.target.checked = false;
        tableRow.classList.remove('bg-light');
        tableRow.classList.remove('text-dark');

        if (mainLanguageChecked === false) {
          mainLangCheckbox.disabled = false;
        }

        if (parseInt(mainLangCheckbox.value) === 1) {
          this.activeLangContainer.classList.remove('border-warning');
        }
      } else {
        e.target.value = 1;
        e.target.checked = true;
        tableRow.classList.add('bg-light');
        tableRow.classList.add('text-dark');

        if (mainLanguageChecked === false) {
          mainLangCheckbox.disabled = true;
        }

        if (parseInt(mainLangCheckbox.value) === 1) {
          this.activeLangContainer.classList.add('border-warning');
        }
      }

      this.activeLangsDisablingCheckboxes = this.getActiveLangsDisablingCheckboxes();
      var leftLanguageInputs = [];
      this.activeLangsDisablingCheckboxes.forEach(function (element) {
        if (parseInt(element.value) === 0) {
          leftLanguageInputs.push(element);
        }
      });

      if (1 > leftLanguageInputs.length) {
        this.activeLangContainer.classList.add('border-danger');
        this.disableUpdateButton();
      }

      if (leftLanguageInputs.length >= 1) {
        this.activeLangContainer.classList.remove('border-danger');
        this.enableUpdateButton();
      }
    }
  }, {
    key: "getActiveLangsDisablingCheckboxes",
    value: function getActiveLangsDisablingCheckboxes() {
      return Array.from(this.activeLangContainer.querySelectorAll('input[data-hcms-lang-disabled=""]'));
    }
  }, {
    key: "getMainLanguageCheckboxes",
    value: function getMainLanguageCheckboxes() {
      return Array.from(this.activeLangContainer.querySelectorAll('input[data-hcms-main-language=""]'));
    }
  }, {
    key: "getAllLanguagesCheckboxes",
    value: function getAllLanguagesCheckboxes() {
      return Array.from(this.allLangContainer.querySelectorAll('input:not([id="all_lang_search"])'));
    }
  }, {
    key: "getDeleteButtons",
    value: function getDeleteButtons() {
      return Array.from(this.activeLangContainer.querySelectorAll('svg[name=delete]'));
    }
  }, {
    key: "handleAllLangSearchFilter",
    value: function handleAllLangSearchFilter(e) {
      var searchString = e.target.value.toUpperCase();

      for (var i = 0; i < this.allLangCheckboxes.length; i++) {
        var langName = this.allLangCheckboxes[i].name;

        if (langName.toUpperCase().indexOf(searchString) > -1) {
          this.allLangCheckboxes[i].parentNode.parentNode.style.display = '';
        } else {
          this.allLangCheckboxes[i].parentNode.parentNode.style.display = 'none';
        }
      }

      var visibleLangsCount = this.allLangContainer.querySelectorAll('div[name="hcms_lang_container"]:not([style="display: none;"])').length;
      var allLangCountElement = document.getElementById('hcms_all_lang_count');
      allLangCountElement.innerHTML = visibleLangsCount;
    }
  }, {
    key: "enableUpdateButton",
    value: function enableUpdateButton() {
      if (this.updateButtonEnabled === false && this.mainLangSelected === true) {
        this.updateButton.removeAttribute('disabled');
        this.updateButton.classList.remove('hcms-cursor-forbidden');
        this.updateButtonEnabled = true;
      }
    }
  }, {
    key: "disableUpdateButton",
    value: function disableUpdateButton() {
      if (this.updateButtonEnabled === true) {
        this.updateButton.setAttribute('disabled', 'disabled');
        this.updateButton.classList.add('hcms-cursor-forbidden');
        this.updateButtonEnabled = false;
      }
    }
  }, {
    key: "isMainLangCheckboxChecked",
    value: function isMainLangCheckboxChecked() {
      var checkedBox = [];
      this.mainLanguageCheckboxes.forEach(function (checkbox) {
        if (checkbox.checked === true) {
          checkedBox.push(checkbox);
        }
      });

      if (checkedBox.length > 0) {
        return true;
      }

      return false;
    }
  }]);

  return HandleLanguageCheckboxes;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/Tooltip.js":
/*!***********************************************************!*\
  !*** ./resources/assets/backend/js/components/Tooltip.js ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Tooltip)
/* harmony export */ });
/* harmony import */ var _utils_StringWordCounter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/StringWordCounter */ "./resources/assets/backend/js/components/utils/StringWordCounter.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var Tooltip = /*#__PURE__*/function () {
  function Tooltip() {
    var _this = this;

    _classCallCheck(this, Tooltip);

    this.alreadyInitialized = false;

    this.hideTooltipKeyOnKeyUp = function (e) {
      // 27 = 'ESC'-key
      if (e.keyCode === 27 || e.code === 'Escape') {
        _this.hideTooltip();
      }

      document.body.removeEventListener('keyup', _this.hideTooltipKeyOnKeyUp, false);
    };
  }

  _createClass(Tooltip, [{
    key: "init",
    value: function init(htmlElement) {
      var _this2 = this;

      if (this.alreadyInitialized === true) {
        return;
      }

      this.targetElements = Array.from(document.querySelectorAll('[data-hcms-tooltip]'));
      this.targetElements.forEach(function (targetEl) {
        targetEl.addEventListener('mouseover', function (e) {
          _this2.showTooltipHover(e);
        });
        targetEl.addEventListener('click', function (e) {
          _this2.showTooltipClick(e);
        });
        targetEl.style.cursor = 'pointer';
      });
      this.alreadyInitialized = true;
    }
  }, {
    key: "create",
    value: function create(e) {
      var _this3 = this;

      var targetElement = e.target;
      var tooltip = document.createElement('div');
      tooltip.className = 'hcms-tooltip';
      tooltip.innerHTML = targetElement.getAttribute('data-hcms-tooltip');
      document.body.appendChild(tooltip);
      var position = this.createPosition(targetElement);
      this.posHorizontal = position.split(' ')[0];
      this.posVertical = position.split(' ')[1];
      this.position(targetElement, tooltip, position); // remove tooltip

      document.body.addEventListener('keyup', this.hideTooltipKeyOnKeyUp, false);
      document.addEventListener('click', function (e) {
        _this3.hideTooltipClick(e);
      });
      document.body.addEventListener('mouseout', function (e) {
        _this3.hideTooltipHover(e);
      });
    }
  }, {
    key: "position",
    value: function position(targetElement, tooltip, _position) {
      var targetElementCoords = targetElement.getBoundingClientRect();
      var left = targetElementCoords.left;
      var top = targetElementCoords.top;
      var dist = 10; // position the tooltip based on HTML data attribute

      switch (_position) {
        case 'left':
          left = parseInt(targetElementCoords.left) - dist - tooltip.offsetWidth;

          if (parseInt(targetElementCoords.left) - tooltip.offsetWidth < 0) {
            left = dist;
          }

          top = (parseInt(targetElementCoords.top) + parseInt(targetElementCoords.bottom)) / 2 - tooltip.offsetHeight / 2;
          tooltip.classList.add('tooltip-left');
          break;

        case 'right':
          left = targetElementCoords.right + dist;

          if (parseInt(targetElementCoords.right) + tooltip.offsetWidth > document.documentElement.clientWidth) {
            left = document.documentElement.clientWidth - tooltip.offsetWidth - dist;
          }

          top = (parseInt(targetElementCoords.top) + parseInt(targetElementCoords.bottom)) / 2 - tooltip.offsetHeight / 2;
          tooltip.classList.add('tooltip-right');
          break;

        case 'bottom':
          top = parseInt(targetElementCoords.bottom) + dist;
          left = parseInt(targetElementCoords.left) + (targetElement.offsetWidth - tooltip.offsetWidth) / 2;
          tooltip.classList.add('tooltip-bottom');
          break;

        default:
        case 'top':
          top = parseInt(targetElementCoords.top) - tooltip.offsetHeight - dist;
          left = parseInt(targetElementCoords.left) + (targetElement.offsetWidth - tooltip.offsetWidth) / 2;
          tooltip.classList.add('tooltip-top');
      }

      left = left < 0 ? parseInt(targetElementCoords.left) : left;
      top = top < 0 ? parseInt(targetElementCoords.bottom) + dist : top;
      tooltip.style.left = left + 'px';
      tooltip.style.top = top + pageYOffset + 'px'; // Correct the position, if necessary
      // Position tooltip on the left side of the targetElement if there is no space on the right

      if (window.innerWidth - targetElementCoords.right < tooltip.offsetWidth && targetElementCoords.width < 50 && _position === 'right') {
        tooltip.style.left = targetElementCoords.left - (tooltip.offsetWidth + targetElementCoords.width) + 'px';
        tooltip.className = 'hcms-tooltip tooltip-left'; // Position tooltip on the right side of the targetElement if there is no space on the left
      } else if (targetElementCoords.left < tooltip.offsetWidth && targetElementCoords.width < 50 && _position === 'left') {
        tooltip.style.left = targetElementCoords.left + targetElementCoords.width + 20 + 'px';
        tooltip.className = 'hcms-tooltip tooltip-right'; // Position tooltip on the bottom if the targetElement is too close to the top of the page
      } else if (targetElementCoords.top < tooltip.offsetHeight) {
        tooltip.style.top = top = parseInt(targetElementCoords.bottom) + dist;
        tooltip.className = 'hcms-tooltip tooltip-bottom'; // Position tooltip on the top, if the targetElement is too close to the bottom of the page
      } else if (window.innerHeight - parseInt(targetElementCoords.bottom) < tooltip.offsetHeight) {
        tooltip.style.top = parseInt(targetElementCoords.top) - tooltip.offsetHeight - dist;
        tooltip.className = 'hcms-tooltip tooltip-top';
      }
    }
  }, {
    key: "createPosition",
    value: function createPosition(targetElement) {
      var position = targetElement.getAttribute('data-tooltip-position');

      if (position === null) {
        return 'top';
      }

      var stringWordCounter = new _utils_StringWordCounter__WEBPACK_IMPORTED_MODULE_0__.default(position);
      var hasOneWord = stringWordCounter.hasMax(1);

      if (hasOneWord === false) {
        return 'top';
      }

      var allowedPositions = ['top', 'bottom', 'left', 'right'];
      var positionAllowed = allowedPositions.indexOf(position) > -1;

      if (positionAllowed === false) {
        return 'top';
      }

      return position;
    }
  }, {
    key: "showTooltipHover",
    value: function showTooltipHover(e) {
      this.create(e);
    }
  }, {
    key: "showTooltipClick",
    value: function showTooltipClick(e) {
      this.create(e);
    }
  }, {
    key: "hideTooltipHover",
    value: function hideTooltipHover(e) {
      if (e.target.hasAttribute('data-hcms-tooltip')) {
        return;
      } else {
        this.hideTooltip();
      }
    }
  }, {
    key: "hideTooltipClick",
    value: function hideTooltipClick(e) {
      if (!e.target.hasAttribute('data-hcms-tooltip')) {
        this.hideTooltip();
      }
    }
  }, {
    key: "hideTooltip",
    value: function hideTooltip() {
      if (document.querySelector('.hcms-tooltip') !== null) {
        document.body.removeChild(document.querySelector('.hcms-tooltip'));
      }
    }
  }]);

  return Tooltip;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/fetch/Fetch.js":
/*!***************************************************************!*\
  !*** ./resources/assets/backend/js/components/fetch/Fetch.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Fetch)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Fetch = /*#__PURE__*/function () {
  function Fetch() {
    _classCallCheck(this, Fetch);

    this.fetchOptions = null;
    this.token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  }

  _createClass(Fetch, [{
    key: "doFetch",
    value: function () {
      var _doFetch = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var fetchOptions,
            response,
            _args = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                fetchOptions = _args.length > 0 && _args[0] !== undefined ? _args[0] : this.fetchOptions;
                _context.next = 3;
                return fetch(fetchOptions.url, {
                  method: fetchOptions.method,
                  credentials: 'same-origin',
                  headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json, text-plain, */*',
                    'X-Requested-With': 'XMLHttpRequest',
                    'X-CSRF-TOKEN': fetchOptions.token ? fetchOptions.token : this.token
                  },
                  body: fetchOptions.data
                });

              case 3:
                response = _context.sent;
                return _context.abrupt("return", response);

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function doFetch() {
        return _doFetch.apply(this, arguments);
      }

      return doFetch;
    }()
  }, {
    key: "createFetchOptions",
    value: function createFetchOptions(form) {
      var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      this.fetchOptions = {
        method: form.method,
        url: form.action,
        data: data ? JSON.stringify(data) : null,
        token: this.token
      };
      return this.fetchOptions;
    }
  }]);

  return Fetch;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/initializer/Initializer.js":
/*!***************************************************************************!*\
  !*** ./resources/assets/backend/js/components/initializer/Initializer.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ Initializer)
/* harmony export */ });
/* harmony import */ var _classes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes */ "./resources/assets/backend/js/components/initializer/classes.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var Initializer = /*#__PURE__*/function () {
  function Initializer() {
    _classCallCheck(this, Initializer);

    this.elementsWithAttributes = Array.from(document.querySelectorAll('[data-hcms-function]'));
    this.classes = _classes__WEBPACK_IMPORTED_MODULE_0__.classes;
  }

  _createClass(Initializer, [{
    key: "init",
    value: function init() {
      var _this = this;

      if (this.elementsWithAttributes.length > 0) {
        this.elementsWithAttributes.forEach(function (htmlElement) {
          var className = htmlElement.getAttribute('data-hcms-function');
          var classToExecute = _this.classes[className];
          classToExecute.init(htmlElement);
        });
      }
    }
  }]);

  return Initializer;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/initializer/classes.js":
/*!***********************************************************************!*\
  !*** ./resources/assets/backend/js/components/initializer/classes.js ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "classes": () => (/* binding */ classes)
/* harmony export */ });
/* harmony import */ var _HandleLanguageCheckboxes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../HandleLanguageCheckboxes */ "./resources/assets/backend/js/components/HandleLanguageCheckboxes.js");
/* harmony import */ var _template_TemplateStatusToggle__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../template/TemplateStatusToggle */ "./resources/assets/backend/js/components/template/TemplateStatusToggle.js");
/* harmony import */ var _Tooltip__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Tooltip */ "./resources/assets/backend/js/components/Tooltip.js");
/* harmony import */ var _template_TemplateSelectModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../template/TemplateSelectModal */ "./resources/assets/backend/js/components/template/TemplateSelectModal.js");
/* harmony import */ var _template_TemplateScan__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../template/TemplateScan */ "./resources/assets/backend/js/components/template/TemplateScan.js");
/* harmony import */ var _DescriptionPreview__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../DescriptionPreview */ "./resources/assets/backend/js/components/DescriptionPreview.js");
/* harmony import */ var _page_PageDeletableStatusToggle__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../page/PageDeletableStatusToggle */ "./resources/assets/backend/js/components/page/PageDeletableStatusToggle.js");
/* harmony import */ var _page_PagePreview__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../page/PagePreview */ "./resources/assets/backend/js/components/page/PagePreview.js");








var classes = {
  HandleLanuageCheckboxes: new _HandleLanguageCheckboxes__WEBPACK_IMPORTED_MODULE_0__.default(),
  TemplateScan: new _template_TemplateScan__WEBPACK_IMPORTED_MODULE_4__.default(),
  TemplateStatusToggle: new _template_TemplateStatusToggle__WEBPACK_IMPORTED_MODULE_1__.default(),
  Tooltip: new _Tooltip__WEBPACK_IMPORTED_MODULE_2__.default(),
  TemplateSelectModal: new _template_TemplateSelectModal__WEBPACK_IMPORTED_MODULE_3__.default(),
  DescriptionPreview: new _DescriptionPreview__WEBPACK_IMPORTED_MODULE_5__.default(),
  PageDeletableStatusToggle: new _page_PageDeletableStatusToggle__WEBPACK_IMPORTED_MODULE_6__.default(),
  PagePreview: new _page_PagePreview__WEBPACK_IMPORTED_MODULE_7__.default()
};

/***/ }),

/***/ "./resources/assets/backend/js/components/page/PageDeletableStatusToggle.js":
/*!**********************************************************************************!*\
  !*** ./resources/assets/backend/js/components/page/PageDeletableStatusToggle.js ***!
  \**********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ PageDeletableStatusToggle)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/AlertCreator */ "./resources/assets/backend/js/components/utils/AlertCreator.js");
/* harmony import */ var _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fetch/Fetch */ "./resources/assets/backend/js/components/fetch/Fetch.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var PageDeletableStatusToggle = /*#__PURE__*/function () {
  function PageDeletableStatusToggle() {
    _classCallCheck(this, PageDeletableStatusToggle);

    this["switch"] = null;
  }

  _createClass(PageDeletableStatusToggle, [{
    key: "init",
    value: function init(htmlElement) {
      var _this = this;

      this["switch"] = htmlElement;
      this["switch"].addEventListener('change', function (e) {
        _this.updateStatus(e);
      });
    }
  }, {
    key: "updateStatus",
    value: function () {
      var _updateStatus = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(e) {
        var pageId, fetch, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                pageId = e.target.getAttribute('data-hcms-page-id');
                fetch = new _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__.default();
                fetch.createFetchOptions(this["switch"].form, {
                  page_id: pageId
                });
                _context.next = 5;
                return fetch.doFetch();

              case 5:
                response = _context.sent;

                if (!(response.status !== 200)) {
                  _context.next = 14;
                  break;
                }

                _context.t0 = this;
                _context.next = 10;
                return response.json();

              case 10:
                _context.t1 = _context.sent;

                _context.t0.handleResponseError.call(_context.t0, _context.t1);

                _context.next = 15;
                break;

              case 14:
                setTimeout(function () {
                  location.reload();
                }, 500);

              case 15:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function updateStatus(_x) {
        return _updateStatus.apply(this, arguments);
      }

      return updateStatus;
    }()
  }, {
    key: "handleResponseError",
    value: function handleResponseError(response) {
      new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__.default(response.error, 'error').createAlertElement();
    }
  }]);

  return PageDeletableStatusToggle;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/page/PageEdit.js":
/*!*****************************************************************!*\
  !*** ./resources/assets/backend/js/components/page/PageEdit.js ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ PageEdit)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fetch_Fetch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../fetch/Fetch */ "./resources/assets/backend/js/components/fetch/Fetch.js");
/* harmony import */ var _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils/AlertCreator */ "./resources/assets/backend/js/components/utils/AlertCreator.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var PageEdit = /*#__PURE__*/function () {
  function PageEdit() {
    var _this = this;

    var iFrameContainer = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

    _classCallCheck(this, PageEdit);

    this.headerHeight = document.getElementsByTagName('header')[0].offsetHeight;
    this.iFrameContainer = iFrameContainer;
    this.modalId = 'hcms-field-edit-modal';
    this.dataAttributeTextFields = 'data-hcms-content-text';
    this.dataAttributeEditorFields = 'data-hcms-content-editor';
    this.additionalPixels = 20;
    this.fieldIdsContainer = document.getElementById('hcms-field-ids');
    this.langKey = document.documentElement.lang;
    this.modal = null;
    this.closeButtons = null;
    this.jsonResponse = null;
    this.fetch = null;

    this.closeModal = function () {
      _this.modal.classList.remove('show');

      _this.modal.style.display = '';
      document.body.className = '';

      _this.background.remove();
    };
  }

  _createClass(PageEdit, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      var textFields = this.searchTextFields(this.dataAttributeTextFields);
      textFields.forEach(function (textField) {
        textField.addEventListener('mouseover', function (e) {
          _this2.addHoverAnimation(e, textField.getAttribute(_this2.dataAttributeTextFields));
        });
      });
      var editorFields = this.searchTextFields(this.dataAttributeEditorFields);
      editorFields.forEach(function (editorField) {
        editorField.addEventListener('mouseover', function (e) {
          _this2.addHoverAnimation(e, editorField.getAttribute(_this2.dataAttributeEditorFields));
        });
      });
      this.closeButtons = Array.from(document.querySelectorAll('[data-hcms-field-edit-modal-dismiss]'));
    }
  }, {
    key: "searchTextFields",
    value: function searchTextFields(dataAttribute) {
      return Array.from(this.iFrameContainer.contentWindow.document.body.querySelectorAll("[".concat(dataAttribute, "]")));
    }
  }, {
    key: "getFieldId",
    value: function getFieldId(key) {
      return this.fieldIdsContainer.querySelector("input[name=".concat(key)).value;
    }
  }, {
    key: "addHoverAnimation",
    value: function addHoverAnimation(e, dataAttribute) {
      var _this3 = this;

      var coleredBackgroundElement = document.createElement('div');
      coleredBackgroundElement.id = dataAttribute;
      coleredBackgroundElement.addEventListener('mouseleave', function () {
        _this3.removeHoverAnimation(coleredBackgroundElement);
      });
      coleredBackgroundElement.addEventListener('click', function () {
        _this3.doEditRequest(coleredBackgroundElement);
      });
      var css = {
        position: 'absolute',
        background: '#e55353',
        display: 'flex',
        borderRadius: '3px',
        border: '1px solid red',
        opacity: 0,
        transition: 'all 0.1s ease-in-out',
        minHeight: e.target.offsetHeight + this.additionalPixels + 'px',
        minWidth: e.target.offsetWidth + this.additionalPixels + 'px'
      };
      var position = this.setPosition(e.target);
      Object.assign(coleredBackgroundElement.style, css, position);
      document.body.append(coleredBackgroundElement);
      document.body.style.cursor = 'pointer'; // Giving the browser time to process makes animation work

      setTimeout(function () {
        Object.assign(coleredBackgroundElement.style, {
          opacity: 0.8
        });
      }, 5);
    }
  }, {
    key: "setPosition",
    value: function setPosition(element) {
      var top = this.headerHeight + element.getBoundingClientRect().top - this.additionalPixels / 2 + 'px';
      var leftNumber;

      if (this.iFrameContainer.getBoundingClientRect().left > 0) {
        leftNumber = this.iFrameContainer.getBoundingClientRect().left + element.getBoundingClientRect().left - this.additionalPixels / 2;
      } else {
        leftNumber = element.getBoundingClientRect().left - this.additionalPixels / 2;
      }

      var left = leftNumber + 'px';
      var position = {
        top: top,
        left: left
      };
      return position;
    }
  }, {
    key: "removeHoverAnimation",
    value: function removeHoverAnimation(coleredBackgroundElement) {
      this.iFrameContainer.contentWindow.document.body.style.cursor = 'auto';
      coleredBackgroundElement.remove();
    }
  }, {
    key: "doEditRequest",
    value: function () {
      var _doEditRequest = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(coleredBackgroundElement) {
        var fieldId, url, form, fetchOptions, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                fieldId = this.getFieldId(coleredBackgroundElement.id);
                url = '/cms/fields/' + this.iFrameContainer.id + '/' + coleredBackgroundElement.id + '/' + fieldId + '/' + this.langKey;
                form = document.createElement('form');
                form.setAttribute('action', url);
                form.setAttribute('method', 'GET');
                this.fetch = new _fetch_Fetch__WEBPACK_IMPORTED_MODULE_1__.default();
                fetchOptions = this.fetch.createFetchOptions(form);
                _context.next = 9;
                return this.fetch.doFetch(fetchOptions);

              case 9:
                response = _context.sent;
                this.handleEditResponse(response);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function doEditRequest(_x) {
        return _doEditRequest.apply(this, arguments);
      }

      return doEditRequest;
    }()
  }, {
    key: "handleEditResponse",
    value: function () {
      var _handleEditResponse = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2(response) {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return response.json();

              case 2:
                this.jsonResponse = _context2.sent;

                if (response.status !== 200) {
                  this.handleResponseError(this.jsonResponse);
                }

                if (this.jsonResponse.hasOwnProperty('redirect') === true) {
                  window.location = this.jsonResponse.redirect.url;
                }

                if (this.jsonResponse.hasOwnProperty('data') === true) {
                  this.openModal();
                }

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function handleEditResponse(_x2) {
        return _handleEditResponse.apply(this, arguments);
      }

      return handleEditResponse;
    }()
  }, {
    key: "openModal",
    value: function openModal() {
      var _this4 = this;

      this.modal = document.getElementById(this.modalId);
      this.addDataToModal();
      this.background = document.createElement('div');
      this.background.className = 'modal-backdrop fade show';
      document.body.append(this.background);
      this.modal.classList.add('show');
      this.modal.style.display = 'block';
      document.body.className = 'modal-open';
      this.closeButtons.forEach(function (buttonEl) {
        buttonEl.addEventListener('click', _this4.closeModal);
      });
      window.addEventListener('click', function (e) {
        if (e.target.getAttribute('id') === _this4.modalId) {
          _this4.closeModal(e);
        }
      });
    }
  }, {
    key: "addDataToModal",
    value: function addDataToModal() {
      var _this5 = this;

      var formGroup = this.modal.getElementsByClassName('form-group')[0];
      formGroup.getElementsByTagName('label')[0].innerHTML = "".concat(this.jsonResponse.data.field_name);
      var input = formGroup.getElementsByTagName('input')[0];
      input.value = this.jsonResponse.data.content;
      input.name = this.jsonResponse.data.field_name;
      var submitButton = this.modal.querySelector('button[type="submit"]');
      submitButton.addEventListener('click', function (e) {
        _this5.fetchData(e, input, _this5.jsonResponse.data.field_id, _this5.jsonResponse.data.page_id, _this5.jsonResponse.data.field_name, _this5.langKey, submitButton.form);
      });
    }
  }, {
    key: "fetchData",
    value: function () {
      var _fetchData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee3(e, inputEl, fieldId, pageId, fieldName, langKey, form) {
        var fetchOptions, response, jsonResponse;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                e.preventDefault();
                fetchOptions = this.fetch.createFetchOptions(form, {
                  fieldId: fieldId,
                  pageId: pageId,
                  content: inputEl.value,
                  fieldName: fieldName,
                  langKey: langKey
                });
                _context3.next = 4;
                return this.fetch.doFetch(fetchOptions);

              case 4:
                response = _context3.sent;

                if (!(response.status !== 200)) {
                  _context3.next = 11;
                  break;
                }

                _context3.t0 = this;
                _context3.next = 9;
                return response.json();

              case 9:
                _context3.t1 = _context3.sent;

                _context3.t0.handleResponseError.call(_context3.t0, _context3.t1);

              case 11:
                if (!(response.status === 200)) {
                  _context3.next = 18;
                  break;
                }

                this.closeModal();
                _context3.next = 15;
                return response.json();

              case 15:
                jsonResponse = _context3.sent;
                new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_2__.default(jsonResponse.success).createAlertElement();
                setTimeout(function () {
                  location.reload();
                }, 1000);

              case 18:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function fetchData(_x3, _x4, _x5, _x6, _x7, _x8, _x9) {
        return _fetchData.apply(this, arguments);
      }

      return fetchData;
    }()
  }, {
    key: "handleResponseError",
    value: function handleResponseError(response) {
      new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_2__.default(response.error, 'error').createAlertElement();
    }
  }]);

  return PageEdit;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/page/PagePreview.js":
/*!********************************************************************!*\
  !*** ./resources/assets/backend/js/components/page/PagePreview.js ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ PagePreview)
/* harmony export */ });
/* harmony import */ var _PageEdit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PageEdit */ "./resources/assets/backend/js/components/page/PageEdit.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var PagePreview = /*#__PURE__*/function () {
  function PagePreview() {
    _classCallCheck(this, PagePreview);

    this.iFrame = null;
    this.mainContainer = null;
  }

  _createClass(PagePreview, [{
    key: "init",
    value: function init(iFrame) {
      var _this = this;

      this.iFrame = iFrame;
      this.mainContainer = document.querySelector('main[class="c-main"]');
      this.mainContainer.style.paddingTop = '0';
      this.setFrameHeight();
      window.addEventListener('resize', function () {
        _this.setFrameHeight();
      });
      new _PageEdit__WEBPACK_IMPORTED_MODULE_0__.default(iFrame).init();
    }
  }, {
    key: "setFrameHeight",
    value: function setFrameHeight() {
      this.iFrame.height = 0;
      this.iFrame.height = this.mainContainer.clientHeight - 5;
    }
  }]);

  return PagePreview;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/template/TemplateScan.js":
/*!*************************************************************************!*\
  !*** ./resources/assets/backend/js/components/template/TemplateScan.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ TemplateScan)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/AlertCreator */ "./resources/assets/backend/js/components/utils/AlertCreator.js");
/* harmony import */ var _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fetch/Fetch */ "./resources/assets/backend/js/components/fetch/Fetch.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var TemplateScan = /*#__PURE__*/function () {
  function TemplateScan() {
    _classCallCheck(this, TemplateScan);

    this.startScanButton = null;
    this.unusedTemplatesModalId = 'deleteConfirmModal';
    this.unusedTemplatesData = null;
    this.scanOptionsForm = null;
    this.scanOptionSwitches = null;
  }

  _createClass(TemplateScan, [{
    key: "init",
    value: function init(htmlElement) {
      var _this = this;

      this.startScanButton = htmlElement;
      this.startScanButton.addEventListener('click', function (e) {
        _this.doTemplateScan(e);
      });
      this.scanOptionsForm = document.getElementById('hcms-scan-options-form');
      this.scanOptionSwitches = Array.from(this.scanOptionsForm.querySelectorAll('input'));
      this.scanOptionSwitches.forEach(function (optionSwitch) {
        optionSwitch.addEventListener('change', function (e) {
          _this.toggleScanOption(e);
        });
      });
    }
  }, {
    key: "doTemplateScan",
    value: function () {
      var _doTemplateScan = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(e) {
        var fetch, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                e.preventDefault();
                this.addSpinner();
                fetch = new _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__.default();
                fetch.createFetchOptions(this.startScanButton.form);
                _context.next = 6;
                return fetch.doFetch();

              case 6:
                response = _context.sent;

                if (!(response.status !== 200)) {
                  _context.next = 15;
                  break;
                }

                _context.t0 = this;
                _context.next = 11;
                return response.json();

              case 11:
                _context.t1 = _context.sent;

                _context.t0.handleResponseError.call(_context.t0, _context.t1);

                _context.next = 20;
                break;

              case 15:
                _context.t2 = this;
                _context.next = 18;
                return response.json();

              case 18:
                _context.t3 = _context.sent;

                _context.t2.handleResponseSuccess.call(_context.t2, _context.t3);

              case 20:
                this.removeSpinner();

              case 21:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function doTemplateScan(_x) {
        return _doTemplateScan.apply(this, arguments);
      }

      return doTemplateScan;
    }()
  }, {
    key: "addSpinner",
    value: function addSpinner() {
      this.startScanButton.firstElementChild.classList = 'spinner-border spinner-border-sm';
    }
  }, {
    key: "removeSpinner",
    value: function removeSpinner() {
      var _this2 = this;

      setTimeout(function () {
        _this2.startScanButton.firstElementChild.classList = '';
      }, 500);
    }
  }, {
    key: "handleResponseSuccess",
    value: function handleResponseSuccess(response) {
      if (response.unusedTemplates != null) {
        this.unusedTemplates = response.unusedTemplates;
        this.showUnusedTemplatesModal();
        return;
      }

      new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__.default(response.success, 'success').createAlertElement();
      setTimeout(function () {
        location.reload();
      }, 1000);
    }
  }, {
    key: "handleResponseError",
    value: function handleResponseError(response) {
      new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__.default(response.error, 'error').createAlertElement();
    }
  }, {
    key: "showUnusedTemplatesModal",
    value: function showUnusedTemplatesModal() {
      var button = document.createElement('button');
      button.classList.add('d-none');
      button.setAttribute('data-toggle', 'modal');
      button.setAttribute('data-target', '#' + this.unusedTemplatesModalId);
      document.body.append(button);
      this.insertUnusedTemplatesIntoModalBody();
      button.click();
      button.remove();
      var closeButtons = Array.from(document.getElementById(this.unusedTemplatesModalId).querySelectorAll('button[type="button"]'));
      closeButtons.forEach(function (closeButton) {
        closeButton.addEventListener('click', function () {
          setTimeout(function () {
            location.reload();
          }, 500);
        });
      });
    }
  }, {
    key: "insertUnusedTemplatesIntoModalBody",
    value: function insertUnusedTemplatesIntoModalBody() {
      var _this3 = this;

      var row = document.getElementById(this.unusedTemplatesModalId).querySelector('div[class="custom-control custom-checkbox"]');
      var modalBody = document.getElementById(this.unusedTemplatesModalId).querySelector('div[class="modal-body"]');
      this.unusedTemplates.forEach(function (template) {
        var clonedRow = row.cloneNode(true);

        var rowWithData = _this3.createRow(clonedRow, template);

        modalBody.append(rowWithData);
      });
      row.remove();
    }
  }, {
    key: "createRow",
    value: function createRow(row, template) {
      var label = row.querySelector('label[class="custom-control-label"]');
      label.innerHTML = template.template_name;
      label.setAttribute('for', 'unused_template_' + template.id);
      var checkbox = row.querySelector('input[type="checkbox"]');
      checkbox.value = template.id;
      checkbox.id = 'unused_template_' + template.id;
      return row;
    }
  }, {
    key: "toggleScanOption",
    value: function () {
      var _toggleScanOption = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee2(e) {
        var fetch, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                fetch = new _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__.default();
                fetch.createFetchOptions(e.target.form, {
                  option_name: e.target.name
                });
                _context2.next = 4;
                return fetch.doFetch();

              case 4:
                response = _context2.sent;

                if (!(response.status !== 200)) {
                  _context2.next = 11;
                  break;
                }

                _context2.t0 = this;
                _context2.next = 9;
                return response.json();

              case 9:
                _context2.t1 = _context2.sent;

                _context2.t0.handleResponseError.call(_context2.t0, _context2.t1);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function toggleScanOption(_x2) {
        return _toggleScanOption.apply(this, arguments);
      }

      return toggleScanOption;
    }()
  }]);

  return TemplateScan;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/template/TemplateSelectModal.js":
/*!********************************************************************************!*\
  !*** ./resources/assets/backend/js/components/template/TemplateSelectModal.js ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ TemplateSelectModal)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/AlertCreator */ "./resources/assets/backend/js/components/utils/AlertCreator.js");
/* harmony import */ var _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fetch/Fetch */ "./resources/assets/backend/js/components/fetch/Fetch.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var TemplateSelectModal = /*#__PURE__*/function () {
  function TemplateSelectModal() {
    var _this = this;

    _classCallCheck(this, TemplateSelectModal);

    this.alreadyInitialized = false;
    this.optionsCreated = false;
    this.openButtons = null;
    this.closeButtons = null;
    this.modalId = null;
    this.modal = null;
    this.background = null;
    this.select = null;
    this.linkElement = null;
    this.previewLink = null;
    this.pageCreateForm = null;
    this.pageCreateLink = null;
    this.createButton = null;

    this.closeModal = function () {
      _this.modal.classList.remove('show');

      _this.modal.style.display = '';
      document.body.className = '';

      _this.background.remove();
    };
  }

  _createClass(TemplateSelectModal, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      if (this.alreadyInitialized === true) {
        return;
      }

      this.openButtons = Array.from(document.querySelectorAll('[data-hcms-create-page-modal-open]'));
      this.closeButtons = Array.from(document.querySelectorAll('[data-hcms-create-page-modal-dismiss]'));
      this.openButtons.forEach(function (buttonEl) {
        buttonEl.addEventListener('click', function (e) {
          _this2.showModal(e);
        });
      });
      this.select = document.getElementById('hcms-template-select');
      this.alreadyInitialized = true;
    }
  }, {
    key: "showModal",
    value: function showModal(e) {
      var _this3 = this;

      if (this.optionsCreated === false) {
        this.createSelectOptions();
      }

      this.background = document.createElement('div');
      this.background.className = 'modal-backdrop fade show';
      document.body.append(this.background);
      this.modalId = e.target.getAttribute('data-hcms-target');
      this.modal = document.getElementById(this.modalId);
      this.modal.classList.add('show');
      this.modal.style.display = 'block';
      document.body.className = 'modal-open';
      this.closeButtons.forEach(function (buttonEl) {
        buttonEl.addEventListener('click', _this3.closeModal);
      });
      window.addEventListener('click', function (e) {
        if (e.target.getAttribute('id') === _this3.modalId) {
          _this3.closeModal(e);
        }
      });
    }
  }, {
    key: "createSelectOptions",
    value: function () {
      var _createSelectOptions = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee() {
        var _this4 = this;

        var form, fetch, response, templates, previewLinkElement;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                form = document.getElementById('hcms-template-select-get');
                fetch = new _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__.default();
                fetch.createFetchOptions(form);
                _context.next = 5;
                return fetch.doFetch();

              case 5:
                response = _context.sent;

                if (!(response.status !== 200)) {
                  _context.next = 9;
                  break;
                }

                new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__.default(response.status, 'error').createAlertElement();
                return _context.abrupt("return");

              case 9:
                _context.next = 11;
                return response.json();

              case 11:
                templates = _context.sent;
                templates.templates.forEach(function (template) {
                  var option = document.createElement('option');
                  option.innerHTML = template.template_name;
                  option.value = template.id;
                  option.setAttribute('name', template.template_name);

                  _this4.select.append(option);
                });
                previewLinkElement = document.getElementById('hcms-template-preview-link');
                this.linkElement = previewLinkElement.getElementsByTagName('a')[0];
                this.previewLink = this.linkElement.getAttribute('href');
                this.pageCreateForm = document.getElementById('hcms-page-create');
                this.createButton = this.pageCreateForm.querySelector('button[type="submit"]');
                this.pageCreateLink = this.pageCreateForm.getAttribute('action');
                Array.from(this.select.options).forEach(function (optionEl) {
                  optionEl.addEventListener('click', function (e) {
                    _this4.handleClick(e);
                  });
                });
                this.optionsCreated = true;

              case 21:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function createSelectOptions() {
        return _createSelectOptions.apply(this, arguments);
      }

      return createSelectOptions;
    }()
  }, {
    key: "handleClick",
    value: function handleClick(e) {
      if (e.target.getAttribute('value')) {
        var previewUrl = this.previewLink + '/' + e.target.value;
        var pageCreateUrl = this.pageCreateLink + '/' + e.target.value;
        this.pageCreateForm.setAttribute('action', pageCreateUrl);
        this.linkElement.setAttribute('href', previewUrl);
        this.linkElement.classList.remove('d-none');
        this.createButton.classList.remove('hcms-cursor-forbidden');
        this.createButton.removeAttribute('disabled');
      } else {
        this.linkElement.className = 'd-none';
        this.pageCreateForm.setAttribute('action', this.pageCreateLink);
        this.linkElement.setAttribute('href', this.previewLink);
        this.createButton.classList.add('hcms-cursor-forbidden');
        this.createButton.setAttribute('disabled', '');
      }
    }
  }]);

  return TemplateSelectModal;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/template/TemplateStatusToggle.js":
/*!*********************************************************************************!*\
  !*** ./resources/assets/backend/js/components/template/TemplateStatusToggle.js ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ TemplateStatusToggle)
/* harmony export */ });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../utils/AlertCreator */ "./resources/assets/backend/js/components/utils/AlertCreator.js");
/* harmony import */ var _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fetch/Fetch */ "./resources/assets/backend/js/components/fetch/Fetch.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }




var TemplateStatusToggle = /*#__PURE__*/function () {
  function TemplateStatusToggle() {
    _classCallCheck(this, TemplateStatusToggle);

    this["switch"] = null;
  }

  _createClass(TemplateStatusToggle, [{
    key: "init",
    value: function init(htmlElement) {
      var _this = this;

      this["switch"] = htmlElement;
      this["switch"].addEventListener('change', function (e) {
        _this.updateStatus(e);
      });
    }
  }, {
    key: "updateStatus",
    value: function () {
      var _updateStatus = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().mark(function _callee(e) {
        var templateId, fetch, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default().wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                templateId = e.target.getAttribute('data-hcms-template-id');
                fetch = new _fetch_Fetch__WEBPACK_IMPORTED_MODULE_2__.default();
                fetch.createFetchOptions(this["switch"].form, {
                  template_id: templateId
                });
                _context.next = 5;
                return fetch.doFetch();

              case 5:
                response = _context.sent;

                if (!(response.status !== 200)) {
                  _context.next = 12;
                  break;
                }

                _context.t0 = this;
                _context.next = 10;
                return response.json();

              case 10:
                _context.t1 = _context.sent;

                _context.t0.handleResponseError.call(_context.t0, _context.t1);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function updateStatus(_x) {
        return _updateStatus.apply(this, arguments);
      }

      return updateStatus;
    }()
  }, {
    key: "handleResponseError",
    value: function handleResponseError(response) {
      new _utils_AlertCreator__WEBPACK_IMPORTED_MODULE_1__.default(response.error, 'error').createAlertElement();
    }
  }]);

  return TemplateStatusToggle;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/utils/AlertCreator.js":
/*!**********************************************************************!*\
  !*** ./resources/assets/backend/js/components/utils/AlertCreator.js ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ AlertCreator)
/* harmony export */ });
/* harmony import */ var _RemoveAlert__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RemoveAlert */ "./resources/assets/backend/js/components/utils/RemoveAlert.js");
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }



var AlertCreator = /*#__PURE__*/function () {
  function AlertCreator(message, type) {
    _classCallCheck(this, AlertCreator);

    this.message = message;
    this.type = type;
  }

  _createClass(AlertCreator, [{
    key: "createAlertElement",
    value: function createAlertElement() {
      var alertElement = document.createElement('div');
      alertElement.addEventListener('click', function () {
        (0,_RemoveAlert__WEBPACK_IMPORTED_MODULE_0__.default)();
      });

      if (this.type === 'error') {
        alertElement.className = 'warning-modal';
      } else {
        alertElement.className = 'alert-modal';
      }

      alertElement.innerHTML = this.message;
      var mainElement = document.getElementsByTagName('main').item(0);
      mainElement.append(alertElement);
    }
  }]);

  return AlertCreator;
}();



/***/ }),

/***/ "./resources/assets/backend/js/components/utils/RemoveAlert.js":
/*!*********************************************************************!*\
  !*** ./resources/assets/backend/js/components/utils/RemoveAlert.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ RemoveAlert)
/* harmony export */ });
function RemoveAlert() {
  var alert = document.querySelector("div[class=alert-modal]");
  var warning = document.querySelector("div[class=warning-modal]");

  if (alert) {
    alert.style.display = "none";
  }

  if (warning) {
    warning.style.display = "none";
  }
}

/***/ }),

/***/ "./resources/assets/backend/js/components/utils/StringWordCounter.js":
/*!***************************************************************************!*\
  !*** ./resources/assets/backend/js/components/utils/StringWordCounter.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (/* binding */ StringWordCounter)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var StringWordCounter = /*#__PURE__*/function () {
  function StringWordCounter(string) {
    _classCallCheck(this, StringWordCounter);

    this.string = string;
    this.prepareString();
  }

  _createClass(StringWordCounter, [{
    key: "hasMax",
    value: function hasMax(maxNumber) {
      var stringLength = this.string.split(' ').length;

      if (maxNumber >= stringLength) {
        return true;
      }

      if (maxNumber < stringLength) {
        return false;
      }
    }
  }, {
    key: "hasMin",
    value: function hasMin(minNumber) {
      var stringLength = this.string.split(' ').length;

      if (stringLength >= minNumber) {
        return true;
      }

      if (minNumber > stringLength) {
        return false;
      }
    }
  }, {
    key: "prepareString",
    value: function prepareString() {
      this.string = this.string.replace(/(^\s*)|(\s*$)/gi, '');
      this.string = this.string.replace(/[ ]{2,}/gi, ' ');
      this.string = this.string.replace(/\n /, '\n');
    }
  }]);

  return StringWordCounter;
}();



/***/ }),

/***/ "./resources/assets/backend/js/hcms.js":
/*!*********************************************!*\
  !*** ./resources/assets/backend/js/hcms.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_utils_RemoveAlert__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components/utils/RemoveAlert */ "./resources/assets/backend/js/components/utils/RemoveAlert.js");
/* harmony import */ var _components_initializer_Initializer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/initializer/Initializer */ "./resources/assets/backend/js/components/initializer/Initializer.js");


document.RemoveAlert = _components_utils_RemoveAlert__WEBPACK_IMPORTED_MODULE_0__.default;
var initializer = new _components_initializer_Initializer__WEBPACK_IMPORTED_MODULE_1__.default();
initializer.init();

/***/ }),

/***/ "./resources/assets/backend/sass/hcms.scss":
/*!*************************************************!*\
  !*** ./resources/assets/backend/sass/hcms.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/assets/backend/sass/vendor/flags.scss":
/*!*********************************************************!*\
  !*** ./resources/assets/backend/sass/vendor/flags.scss ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/assets/frontend/sass/ExampleProject.scss":
/*!************************************************************!*\
  !*** ./resources/assets/frontend/sass/ExampleProject.scss ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./node_modules/regenerator-runtime/runtime.js":
/*!*****************************************************!*\
  !*** ./node_modules/regenerator-runtime/runtime.js ***!
  \*****************************************************/
/***/ ((module) => {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : 0
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					result = fn();
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/backend/hcms": 0,
/******/ 			"frontend/exampleFrontend": 0,
/******/ 			"backend/flags": 0,
/******/ 			"backend/hcms": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			for(moduleId in moreModules) {
/******/ 				if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 					__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 				}
/******/ 			}
/******/ 			if(runtime) var result = runtime(__webpack_require__);
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkhybridcms"] = self["webpackChunkhybridcms"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["frontend/exampleFrontend","backend/flags","backend/hcms"], () => (__webpack_require__("./resources/assets/backend/js/hcms.js")))
/******/ 	__webpack_require__.O(undefined, ["frontend/exampleFrontend","backend/flags","backend/hcms"], () => (__webpack_require__("./resources/assets/backend/sass/hcms.scss")))
/******/ 	__webpack_require__.O(undefined, ["frontend/exampleFrontend","backend/flags","backend/hcms"], () => (__webpack_require__("./resources/assets/backend/sass/vendor/flags.scss")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["frontend/exampleFrontend","backend/flags","backend/hcms"], () => (__webpack_require__("./resources/assets/frontend/sass/ExampleProject.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;