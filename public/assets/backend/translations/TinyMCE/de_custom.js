function provideTranslations() {
    globalThis.tinyMCECustomTranslations = {
        test1: 'test de',
        test2: 'test d1',
        test3: 'test d2',
        test4: 'test d2',
        test5: 'test d3',
        test6: 'test d4',
        test7: 'test d4',
        test8: 'test d4',
        test9: 'test d4',
        test10: 'test d4',
        test11: 'test d4',
        test12: 'test d4',
        test13: 'test d4',
    }
}

provideTranslations()
