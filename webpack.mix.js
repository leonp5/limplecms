const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js(
//     "package/leonp5/hybridcms/resources/assets/js/hcms.js",
//     "public/hybridcms"
// )
//     .js(
//         "package/leonp5/hybridcms/resources/assets/js/hcms-vendor.js",
//         "public/hybridcms"
//     )
//     .sass(
//         "package/leonp5/hybridcms/resources/assets/sass/hcms.scss",
//         "public/hybridcms",
//         [
//             //
//         ]
//     );

// mix.browserSync({
//     proxy: "http://127.0.0.1:8000",

//     files: [
//         "package/leonp5/hybridcms/**/*.php",
//         "package/leonp5/hybridcms/resources/assets/*.js",
//         "package/leonp5/hybridcms/resources/assets/*.scss"
//     ],
//     // => true = the browse opens a new browser window with every npm run watch startup/reload

//     open: false,
//     notify: false,
//     ui: false
// });
